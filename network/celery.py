# pylint: disable=C0415
"""SONIKS Network celery task workers"""

import os

from celery import Celery
from celery.schedules import crontab
from django.conf import settings  # noqa

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "network.settings")

RUN_DAILY = 60 * 60 * 24
RUN_EVERY_TWO_HOURS = 2 * 60 * 60
RUN_HOURLY = 60 * 60
RUN_EVERY_15_MINUTES = 60 * 15
RUN_TWICE_HOURLY = 60 * 30
RUN_EVERY_FOUR_HOURS = 60 * 60 * 4

APP = Celery("network")

APP.config_from_object("django.conf:settings", namespace="CELERY")
APP.autodiscover_tasks()


# Wrapper tasks as workaround for registering shared tasks to beat scheduler
# See https://github.com/celery/celery/issues/5059
# and https://github.com/celery/celery/issues/3797#issuecomment-422656038
@APP.task
def update_future_observations_with_new_tle_sets():
    """
    Wrapper task for 'update_future_observations_with_new_tle_sets' shared task
    """
    from network.base.tasks import (
        update_future_observations_with_new_tle_sets as periodic_task,
    )

    periodic_task()


@APP.task
def fetch_data():
    """
    Wrapper task for 'fetch_data' shared task
    """
    from network.base.tasks import fetch_data as periodic_task

    periodic_task()


@APP.task
def clean_observations():
    """
    Wrapper task for 'clean_observations' shared task
    """
    from network.base.tasks import clean_observations as periodic_task

    periodic_task()


@APP.task
def station_status_update():
    """
    Wrapper task for 'station_status_update' shared task
    """
    from network.base.tasks import station_status_update as periodic_task

    periodic_task()


@APP.task
def notify_for_stations_without_results():
    """
    Wrapper task for 'notify_for_stations_without_results' shared task
    """
    from network.base.tasks import notify_for_stations_without_results as periodic_task

    periodic_task()


@APP.task
def zip_audio_files():
    """
    Wrapper task for 'zip_audio_files' shared task
    """
    from network.base.tasks import zip_audio_files as periodic_task

    periodic_task()


@APP.task
def archive_audio_zip_files():
    """
    Wrapper task for 'archive_audio_zip_files' shared task
    """
    from network.base.tasks import archive_audio_zip_files as periodic_task

    periodic_task()


@APP.task
def find_and_rate_failed_observations():
    """
    Wrapper task for 'find_and_rate_failed_observations' shared task
    """
    from network.base.rating_tasks import (
        find_and_rate_failed_observations as periodic_task,
    )

    periodic_task()


@APP.task
def decode_recent_data():
    """
    Wrapper task for 'decode_recent_data' shared task
    """
    from network.base.tasks import decode_recent_data as periodic_task

    periodic_task()


@APP.task
def update_transmitters_stats():
    """
    Wrapper task for 'update_transmitters_stats' shared task
    """
    from network.base.tasks import update_transmitters_stats as periodic_task

    periodic_task()


@APP.task
def remove_old_exported_framesets():
    """
    Wrapper task for 'remove_old_exported_framesets' shared task
    """
    from network.base.tasks import remove_old_exported_framesets as periodic_task

    periodic_task()


@APP.task
def parse_data_from_satnogs():
    """
    Wrapper task for 'parse_data_from_satnogs' shared task
    """
    from network.base.tasks import parse_data_from_satnogs as periodic_task

    periodic_task()


@APP.task
def auto_schedule_network(hours_for_schedule):
    """
    Wrapper task for 'auto_schedule_network' shared task
    """
    from network.base.tasks import auto_schedule_network as periodic_task

    periodic_task(hours_for_schedule)


@APP.task
def check_unknown_satellites(check_launch=False):
    """
    Wrapper task for 'check_unknown_satellites' shared task
    """
    from network.base.tasks import check_unknown_satellites as periodic_task

    periodic_task(check_launch)


@APP.task
def calculate_all_satellite_orbital():
    """Wrapper task for 'calculate_satellite_orbital' shared task"""
    from network.base.tasks import calculate_all_satellite_orbital as periodic_task

    periodic_task()


@APP.task
def calculate_all_station_stats():
    """Wrapper task for 'calculate_station_stats' shared task"""
    from network.base.tasks import calculate_all_station_stat as periodic_task

    periodic_task()


@APP.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):  # pylint: disable=W0613
    """
    Initializes celery tasks that need to run on a scheduled basis
    """
    sender.add_periodic_task(
        RUN_TWICE_HOURLY,
        update_future_observations_with_new_tle_sets.s(),
        name="update_future_observations_with_new_tle_sets",
    )

    sender.add_periodic_task(RUN_HOURLY, fetch_data.s(), name="fetch_data")

    sender.add_periodic_task(
        RUN_HOURLY, station_status_update.s(), name="station_status_update"
    )

    sender.add_periodic_task(
        settings.OBS_NO_RESULTS_CHECK_PERIOD,
        notify_for_stations_without_results.s(),
        name="notify_for_stations_without_results",
    )

    if settings.ARCHIVE_ZIP_FILES and not settings.USE_S3_STORAGE_FOR_AUDIO:
        sender.add_periodic_task(
            RUN_HOURLY, archive_audio_zip_files.s(), name="archive_audio_zip_files"
        )

    if settings.ZIP_AUDIO_FILES and not settings.USE_S3_STORAGE_FOR_AUDIO:
        sender.add_periodic_task(
            RUN_EVERY_15_MINUTES, zip_audio_files.s(), name="zip_audio_files"
        )

    sender.add_periodic_task(
        RUN_EVERY_15_MINUTES,
        find_and_rate_failed_observations.s(),
        name="find_and_rate_failed_observations",
    )

    sender.add_periodic_task(
        crontab(minute="*/15"),
        update_transmitters_stats.s(),
        name="update_transmitters_stats",
    )

    sender.add_periodic_task(
        RUN_DAILY, decode_recent_data.s(), name="decode_recent_data"
    )

    sender.add_periodic_task(
        RUN_HOURLY,
        remove_old_exported_framesets.s(),
        name="remove_old_exported_frameset",
    )

    if settings.ENVIRONMENT == "production":
        sender.add_periodic_task(
            crontab(minute=0, hour=1),
            parse_data_from_satnogs.s(),
            name="parse_data_from_satnogs",
        )

    sender.add_periodic_task(
        crontab(minute=0, hour="*/12"),
        auto_schedule_network.s(36),
        name="auto_schedule_network",
    )

    sender.add_periodic_task(
        RUN_DAILY, check_unknown_satellites.s(), name="check_unknown_satellites"
    )

    sender.add_periodic_task(
        RUN_EVERY_15_MINUTES,
        check_unknown_satellites.s(True),
        name="check_unknown_satellites_launch",
    )

    sender.add_periodic_task(
        crontab(minute=0, hour=5),
        calculate_all_satellite_orbital.s(),
        name="calculate_satellite_orbital",
    )
    sender.add_periodic_task(
        crontab(minute=0, hour=3),
        calculate_all_station_stats.s(),
        name="calculate_station_stats",
    )
