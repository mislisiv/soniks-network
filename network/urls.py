"""Base Django URL mapping for SONIKS Network"""

from allauth import urls as allauth_urls
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import include, path, re_path
from django.utils.version import get_version
from django.views.decorators.cache import cache_page
from django.views.i18n import JavaScriptCatalog
from django.views.static import serve

from network.api.urls import API_URLPATTERNS
from network.base.urls import BASE_URLPATTERNS
from network.users.urls import USERS_URLPATTERNS

urlpatterns = [
    # language switch
    path("i18n/", include("django.conf.urls.i18n")),
    # js lang,caching for speed-up site
    path("jsi18n/", JavaScriptCatalog.as_view(), name="javascript-catalog"),
    # Base urls
    path("", include(BASE_URLPATTERNS)),
    path("admin/", admin.site.urls),
    path("users/", include(USERS_URLPATTERNS)),
    path("accounts/", include(allauth_urls)),
    path("api/", include(API_URLPATTERNS)),
    path("oidc/", include("mozilla_django_oidc.urls")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
    urlpatterns += [
        re_path(r"^media/(?P<path>.*)$", serve, {"document_root": settings.MEDIA_ROOT}),
    ]
