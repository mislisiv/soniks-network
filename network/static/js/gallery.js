/* global gettext */

$(document).ready(function () {
    $('.loading-spinner').show();
    var satellites = $('.image-card .title');
    satellites.each(function() {
        var sat_id = $(this).data('satellite');
        var gallery_card = $(this).parent();
        $.ajax({
            url: '/db/gallery/load/' + sat_id
        }).done(function (data) {
            var card = create_gallery_card(data.cover, data.images, sat_id);
            gallery_card.append(card);
            gallery_card.find('.loading-spinner').hide();
            gallery_card.css('background-color', 'transparent');
        });
        $(this).click(function() {
            $(this).next()[0].click();
        });
    });

});

function create_gallery_card(cover, images, sat_id) {
    var alt_photo_text=gettext('Фотография со спутника');
    var card = `
    <a href="/db/gallery/${sat_id}" class="images">
      <img src="${cover}" loading="lazy" alt="${alt_photo_text}" class="cover img-1">
      <img src="${images[0]}" loading="lazy"  alt="${alt_photo_text}" class="cover img-2">
      <img src="${images[1]}" loading="lazy" alt="${alt_photo_text}" class="cover img-3">
    </a>`;
    return card;
}
