/* global gettext */

$(document).ready(demoddata_json_script);
document.addEventListener('obs_changed', demoddata_json_script, false);

function demoddata_json_script(){
    'use strict';
    $('#DemoddataJsonModal').on('show.bs.modal', function (event) {
        var datalink = $(event.relatedTarget);
        var modalBody = $(this).find('.modal-body');
        modalBody.empty();
        var url=`/api/demoddata/?format=json&id=${datalink.data('demoddata-id')}&observation_id=${datalink.data('obs-id')}`;
        $.ajax({
            url: url
        }).done(function (data) {
            
            var json=data.results[0].payload_json[0];
            var json_fields=json.fields;
            for(const key in json_fields){
                modalBody.append(`
                    <div class="row"><code>${key}</code><code>${json_fields[key]}</code></div>
                    `);
            }   
        }).fail(function() {
            modalBody.append(`<div class="row">${gettext('Для просмотра декодированных данных необходимо выполнить вход в систему.')}</div>`);
        });
    });
}
