/* global moment */

$(document).ready(function () {
    'use strict';
    utc_info();
});

document.addEventListener('obs_changed', utc_info, false);

function utc_info() {
    var local = moment().format('HH:mm');
    var utc = moment().utc().format('HH:mm');
    $('#timezone-info').prop('title', '  UTC Time: ' + utc + '\nLocal Time: ' + local);
}
