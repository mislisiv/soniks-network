/*global mapboxgl*/
/*global satellite */

$(document).ready(function () {
    'use strict';

    var whiteStyle = 'mapbox://styles/kurnosovaiv/clmhjm9nr01mv01r74civ01a3';
    var darkStyle = 'mapbox://styles/kurnosovaiv/clmhl04sh01kj01r40rkg9o66';
    var mapStyle = localStorage.getItem('darkmode') == 'false' ? darkStyle : whiteStyle;

    var online_points = {};
    var location_data = {};
    var orbit_data = {};
    var footprint_data = {};


    var sin = Math.sin;
    var cos = Math.cos;
    var fabs = Math.abs;
    var acos = Math.acos;
    var asin = Math.asin;

    var pi = Math.pi;
    var earthRadius = 6.378135E3;  /* Earth radius km */
    var de2ra = 1.74532925E-2;  /* Degrees to Radians */
    var pio2 = 1.5707963267949;  /* Pi/2 */

    var tle1 = $('div#sat-map').data('tle1');
    var tle2 = $('div#sat-map').data('tle2');

    var sat_id = $('div#sat-map').data('satid');

    var mapboxtoken = $('div#sat-map').data('mapboxtoken');
    var stations = $('div#sat-map').data('stations');

    mapboxgl.accessToken = mapboxtoken;

    /* Returns angle in radians from arg id degrees */
    function radians(arg) {
        return (arg * de2ra);
    }

    /* Returns angle in degrees from arg in rads */
    function degrees(arg) {
        return (arg / de2ra);
    }

    function arccos(x, y) {
        if (x && y) {
            if (y > 0.0) {
                return Math.acos(x / y);
            }
            else if (y < 0.0) {
                return pi + Math.acos(x / y);
            }
        }

        return 0.0;
    }


    function get_orbits(satrec, t) {
        /**
         * Calculate 300 orbital points with 60 second increments, starting at epoch t
         *
         * @return {Array} Array that contains Arrays of geodetic (lon, lat) pairs in degrees
         */

        // Number of positions to compute
        const COUNT = 3000;

        // Interval in ms between positions to compute
        const STEP = 6 * 1000;

        // Create satellite orbit
        var current_orbit = [];
        var all_orbits = [];

        var previous = 0;
        for (var i = 0; i < COUNT; i++) {
            const coords = calc_geodetic_coords(satrec, t);

            if (Math.abs(coords['lon'] - previous) > 180) {
                // Satellite passed the 180th meridian.
                // Save last orbital point and
                // start new Array for the next orbit
                var end_lat = (coords['lat'] + current_orbit[current_orbit.length - 1][1]) / 2;

                if (coords['lon'] < 0.0) {
                    current_orbit.push([180.0, end_lat]);
                    all_orbits.push(current_orbit);
                    current_orbit = [];
                    current_orbit.push([-180.0, end_lat]);
                }
                else {
                    current_orbit.push([-180.0, end_lat]);
                    all_orbits.push(current_orbit);
                    current_orbit = [];
                    current_orbit.push([180.0, end_lat]);
                }
            }
            else {
                current_orbit.push([coords['lon'], coords['lat']]);
            }

            previous = coords['lon'];

            // Increase time for next point
            t.setTime(t.getTime() + STEP);
        }
        return all_orbits;
    }

    function calc_geodetic_coords(satrec, t) {
        const positionAndVelocity = satellite.propagate(satrec, t);
        const gmst = satellite.gstime(t);
        const positionEci = positionAndVelocity.position;
        const positionGd = satellite.eciToGeodetic(positionEci, gmst);
        const longitude = satellite.degreesLong(positionGd.longitude);
        const latitude = satellite.degreesLat(positionGd.latitude);

        return {
            'lon': longitude,
            'lat': latitude,
            'height': positionGd.height
        };
    }

    function get_range_circle(current_coords) {
        // first we have to calculate the footprint
        var footprint = 12756.33 * acos(earthRadius / (earthRadius + current_coords.height));

        var azi,
            ssplat,
            ssplon,
            beta,
            azimuth,
            num,
            dem;

        /* Range circle calculations.
         * Borrowed from gsat 0.9.0 by Xavier Crehueras, EB3CZS
         * who borrowed from John Magliacane, KD2BD.
         * Optimized by Alexandru Csete and William J Beksi.
         */
        ssplat = radians(current_coords.lat);
        ssplon = radians(current_coords.lon);
        beta = (0.5 * footprint) / earthRadius;

        var points = [];
        var lat = 0.0;
        var lon = 0.0;

        for (azi = 0; azi < 360; azi += 5) {
            azimuth = de2ra * azi;
            lat = asin(sin(ssplat) * cos(beta) + cos(azimuth) * sin(beta)
                * cos(ssplat));
            num = cos(beta) - (sin(ssplat) * sin(lat));
            dem = cos(ssplat) * cos(lat);

            if (azi == 0 && (beta > pio2 - ssplat)) {
                lon = ssplon + pi;
            }
            else if (azi == 180 && (beta > pio2 + ssplat)) {
                lon = ssplon + pi;
            }
            else if (fabs(num / dem) > 1.0) {
                lon = ssplon;
            } else {
                if ((180 - azi) >= 0) {
                    lon = ssplon - arccos(num, dem);
                } else {
                    lon = ssplon + arccos(num, dem);
                }
            }
            points.push([degrees(lon), degrees(lat)]);
        }
        return points;
    }

    // Load satellite orbit data from TLE
    var satrec = satellite.twoline2satrec(tle1, tle2);


    // Calculate orbits and current satellite location
    const now = new Date();
    var current_coords = calc_geodetic_coords(satrec, now);
    var sat_location = [current_coords.lon, current_coords.lat];
    var footprint = get_range_circle(current_coords);
    var all_orbits = get_orbits(satrec, now);

    var map = new mapboxgl.Map({
        container: 'sat-map',
        style: mapStyle,
        zoom: 2,
        minZoom: 0,
        center: sat_location
    });

    map.touchZoomRotate.disableRotation();
    map.dragRotate.disable();
    if (!('ontouchstart' in window)) {
        map.addControl(new mapboxgl.NavigationControl());
    }

    map.on('load', function () {
        refreshMap();
        
        function update_map() {
            // Recalculate current satellite location
            current_coords = calc_geodetic_coords(satrec, new Date());
            sat_location = [current_coords.lon, current_coords.lat];
            footprint = get_range_circle(current_coords);
            location_data.features[0].geometry.coordinates = sat_location;
            footprint_data.features[0].geometry.coordinates = [footprint];
            map.getSource('sat_location').setData(location_data);
            map.getSource('sat_footprint').setData(footprint_data);
        }
        setInterval(update_map, 5000);

        map.loadImage('/static/img/online.png', function (error, image) {
            map.addImage('online', image);
        });

        online_points = {
            'id': 'online-points',
            'type': 'symbol',
            'source': {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': []
                }
            },
            'layout': {
                'icon-image': 'online',
                'icon-size': 0.25,
                'icon-allow-overlap': true
            }
        };

        $.ajax({
            url: stations
        }).done(function (data) {
            data.forEach(function (m) {
                if(m.status==2)
                {create_station_point(online_points, m, sat_id);}
            });

            // Add layers to map
            map.addLayer(online_points);

            // Register keys for toggling visibility of layers
            $(document).bind('keyup', function (event) {
                if (event.which == 78) {
                    toggle_layer(map, online_points);
                }
            });

        });
    });

    function refreshMap() {
        location_data = {
            'type': 'FeatureCollection',
            'features': [{
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': sat_location
                }
            }]
        };

        if (!map.getSource('sat_location')) {
            map.addSource('sat_location', {
                'type': 'geojson',
                'data': location_data
            });
        }
        
        if (!map.getLayer('sat_location')) {
            var theme = localStorage.getItem('darkmode');
            var img = '/static/img/satellite-marker.png';
            if (theme == 'false') {
                img = '/static/img/satellite-marker-white.png';
            }
            map.loadImage(img, function (error, image) {
                map.addImage('sat_icon', image);
            });
            map.addLayer({
                'id': 'sat_location',
                'type': 'symbol',
                'source': 'sat_location',
                'layout': {
                    'icon-image': 'sat_icon',
                    'icon-size': 0.4
                }
            });
        }

        orbit_data = {
            'type': 'FeatureCollection',
            'features': []
        };

        all_orbits.forEach(function (orbit) {
            orbit_data.features.push({
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates': orbit
                }
            });
        });

        if (!map.getSource('sat_orbit')) {
            map.addSource('sat_orbit', {
                'type': 'geojson',
                'data': orbit_data
            });
        }

        if (!map.getLayer('sat_orbit')) {
            map.addLayer({
                'id': 'sat_orbit',
                'type': 'line',
                'source': 'sat_orbit',
                'paint': {
                    'line-color': '#fe5e22',
                    'line-width': 2
                }
            });
        }

        footprint_data = {
            'type': 'FeatureCollection',
            'features': [{
                'type': 'Feature',
                'geometry': {
                    'type': 'Polygon',
                    'coordinates': [footprint]
                }
            }]
        };

        if (!map.getSource('sat_footprint')) {
            map.addSource('sat_footprint', {
                'type': 'geojson',
                'data': footprint_data
            });
        }

        if (!map.getLayer('sat_footprint')) {
            map.addLayer({
                'id': 'sat_footprint',
                'type': 'fill',
                'source': 'sat_footprint',
                'paint': {
                    'fill-opacity': 0.2
                }
            });
        }
    }

    // Toggle map layer
    function toggle_layer(map, layer) {
        var visibility = map.getLayoutProperty(layer.id, 'visibility');

        //Check if layer is already visible
        if (visibility === 'visible') {
            map.setLayoutProperty(layer.id, 'visibility', 'none');
            layer.className = '';
        } else {
            layer.className = 'active';
            map.setLayoutProperty(layer.id, 'visibility', 'visible');
        }
    }

    // Create a popup, but don't add it to the map yet.
    var popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: true
    });

    map.on('mouseenter', 'online-points', function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(e.features[0].geometry.coordinates)
            .setHTML(e.features[0].properties.description)
            .addTo(map);
    });

    map.on('click', 'online-points', function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(e.features[0].geometry.coordinates)
            .setHTML(e.features[0].properties.description)
            .addTo(map);
    });

    map.on('mouseenter', 'testing-points', function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(e.features[0].geometry.coordinates)
            .setHTML(e.features[0].properties.description)
            .addTo(map);
    });

    map.on('click', 'testing-points', function (e) {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(e.features[0].geometry.coordinates)
            .setHTML(e.features[0].properties.description)
            .addTo(map);
    });

    // Resize map for Stations modal
    $('#MapModal').on('shown.bs.modal', function () {
        map.resize();
    });

    $('button.darkmode-toggle').click(function() {
        var l = localStorage.getItem('darkmode');
        if (l == 'false') {
            map.setStyle(darkStyle);
        }
        else {
            map.setStyle(whiteStyle);
        }
        map.on('style.load', function() {
            refreshMap();
            if (!map.getLayer(online_points.id)) {
                map.loadImage('/static/img/online.png', function (error, image) {
                    map.addImage('online', image);
                });
                map.addLayer(online_points);
            }
        });
    });

    $('div.gotocard').click(function() {
        create_big_map(map);
    });

    $('div.gotoback').click(function() {
        hide_big_map(map, sat_location);
    });
});

function create_big_map(map) {
    $('header.darkmode-ignore').addClass('d-none');
    var blockMap = $('#sat-map');
    blockMap.addClass('prev-map');
    $('.gotoback').addClass('show-arrow').removeClass('d-none');
    map.resize();
}

function hide_big_map(map, center) {
    $('header.darkmode-ignore').removeClass('d-none');
    var blockMap = $('#sat-map');
    blockMap.removeClass('prev-map');
    $('.gotoback').removeClass('show-arrow').addClass('d-none');
    $('.mapboxgl-popup').hide();
    map.resize();
    map.flyTo({center: center, zoom: 2});
}

function create_station_point(array, station, satellite_id) {
    var key = `${station.lng}${station.lat}`;
    var index = array.source.data.features.findIndex(e => e.key === key);
    if (index > -1){
        array.source.data.features[index].properties.description += `<br><a href="/satellites/${satellite_id}/#" data-target="#StationModal" data-toggle="modal" data-stationid="${station.id}" data-satid="${satellite_id}">${station.id} - ${station.name}</a>`;
    }
    array.source.data.features.push({
        'key': key,
        'type': 'Feature',
        'geometry': {
            'type': 'Point',
            'coordinates': [
                parseFloat(station.lng),
                parseFloat(station.lat)]
        },
        'properties': {
            'description': `<a href="/satellites/${satellite_id}/#" data-target="#StationModal" data-toggle="modal" data-stationid="${station.id}" data-satid="${satellite_id}">${station.id} - ${station.name}</a>`,
        }
    });
}
