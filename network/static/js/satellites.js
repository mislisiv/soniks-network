/* global moment tempusDominus,gettext */

$(document).ready(function () {
    'use strict';

    $('#show-all-filters').click(function() {
        $(this).toggleClass('show');
        $('.hidden-filters').slideToggle();

        if($(this)[0].innerText == gettext('Больше фильтров')){
            $(this).text(gettext('Меньше фильтров'));
        }
        else {
            $(this).text(gettext('Больше фильтров'));
        }
    });

    $('.checkbox-group input').click(function () {
        var input = $(this);

        if (input.prop('checked')) {
            input.parent().addClass('inactive');
        } else {
            input.parent().removeClass('inactive');
        }
    });

    $('.filter-section #status-selector input').click(function () {
        var input = $(this);

        if (input.prop('checked')) {
            input.parent().addClass('btn-inactive');
        } else {
            input.parent().removeClass('btn-inactive');
        }
    });

    const filter_button = $('#filter-button');
    const norad_formgroup = $('#norad-id-filter');
    const norad_input = $('#search-norad');
    const norad_format = $('#norad-id-format');

    norad_input.on('input', function (e) {
        e.stopPropagation();
        e.stopImmediatePropagation();
        var has_error = 0;
        var val = parseInt($(this).val());
        const norad_errors = { '1': gettext('Значение должно быть числом'), '2':gettext('Значение должно быть больше 0') };

        if (isNaN(val)) {
            has_error = 1;
        }
        else if (val < 0) {
            has_error = 2;
        }

        if (!val && $(this)[0].validity.valid) {
            norad_formgroup.removeClass('has-error');
            filter_button.prop('disabled', false);
            norad_format.removeClass('alert-error');
            norad_format.html('');
        }
        else if (!has_error) {
            norad_formgroup.removeClass('has-error');
            filter_button.prop('disabled', false);
            norad_format.removeClass('alert-error');
            norad_format.html('');
        } else {
            norad_formgroup.addClass('has-error');
            filter_button.prop('disabled', true);
            norad_format.addClass('alert-error');
            norad_format.html(norad_errors[has_error]);
        }
    });


    function date_to_format_string(date) {
        var month = date.getMonth().toString();
        var day = date.getDate().toString();
        var hours = date.getHours().toString();
        var minutes = date.getMinutes().toString();
        if (month.length == 1) {
            month = '0' + month;
        }
        if (day.length == 1) {
            day = '0' + day;
        }
        if (hours.length == 1) {
            hours = '0' + hours;
        }
        if (minutes.length == 1) {
            minutes = '0' + minutes;
        }

        return date.getFullYear() + '-' + month + '-' +
            day + ' ' + hours + ':' + minutes;
    }

    $('#data-period-selection').on('show.bs.select', function() {

        $('.period').click(function () {
            var period = $(this).text();
            var start_time_input = $('#datetimepicker-start input');
            var end_time_input = $('#datetimepicker-end input');
            var now = new Date();
            now.setMonth(now.getMonth() + 1);
            if (period.includes('время') || period.includes('time')) {
                start_time_input.val('');
                end_time_input.val('');
            }
            else if (period.includes('час') || period.includes('hour')) {
                end_time_input.val(date_to_format_string(now));
                now.setHours(now.getHours() - 1);
                start_time_input.val(date_to_format_string(now));
            }
            else if (period.includes('день') || period.includes('day')) {
                end_time_input.val(date_to_format_string(now));
                now.setDate(now.getDate() - 1);
                start_time_input.val(date_to_format_string(now));
            }
            else if (period.includes('неделю') || period.includes('week')) {
                end_time_input.val(date_to_format_string(now));
                now.setDate(now.getDate() - 7);
                start_time_input.val(date_to_format_string(now));
            }
            else if (period.includes('месяц') || period.includes('month')) {
                end_time_input.val(date_to_format_string(now));
                now.setMonth(now.getMonth() - 1);
                start_time_input.val(date_to_format_string(now));
            }
            else if (period.includes('год') || period.includes('year')) {
                end_time_input.val(date_to_format_string(now));
                now.setFullYear(now.getFullYear() - 1);
                start_time_input.val(date_to_format_string(now));
            }
        });
    });

    var dateStart = $('#observations-date-start');
    var dateEnd = $('#observations-date-end');

    dateStart.on('change', function () {
        var data = dateStart.val().replace('T', 'T');
        dateStart.val(data);
    });

    dateEnd.on('change', function () {
        var data = dateEnd.val().replace('T', 'T');
        dateEnd.val(data);
    });


    tempusDominus.extend(window.tempusDominus.plugins.customDateFormat);
    var locale=document.documentElement.lang;
    var dateConfiguration = {
        useCurrent: false,
        display: {
            icons: {
                type: 'icons',
                time: 'bi bi-clock',
                date: 'bi bi-calendar3',
                up: 'bi bi-arrow-up',
                down: 'bi bi-arrow-down',
                previous: 'bi bi-chevron-left',
                next: 'bi bi-chevron-right',
                today: 'bi bi-calendar-check',
                clear: 'bi bi-trash',
                close: 'bi bi-x-lg'
            },
            sideBySide: true,
            components: {
                useTwentyfourHour: true
            },
            buttons: {
                close: true
            }
        },
        localization: {
            startOfTheWeek: 1,
            format: 'yyyy-MM-dd HH:mm',
            hourCycle: 'h23',
            locale: locale
        }
    };
    var start = new tempusDominus.TempusDominus(document.getElementById('datetimepicker-start'), dateConfiguration);
    var end = new tempusDominus.TempusDominus(document.getElementById('datetimepicker-end'), dateConfiguration);

    const otherValidFormats = [
        'YYYY-MM-DD H:mm', 'YYYY-MM-DD HH', 'YYYY-MM-DD H', 'YYYY-MM-DD HH:m',
        'YYYY-MM-DD H:m', 'YYYY-MM-D HH:mm', 'YYYY-MM-D H:mm', 'YYYY-MM-D HH',
        'YYYY-MM-D H', 'YYYY-MM-D HH:m', 'YYYY-MM-D H:m', 'YYYY-M-D HH:mm', 'YYYY-M-D H:mm',
        'YYYY-M-D HH', 'YYYY-M-D H', 'YYYY-M-D HH:m', 'YYYY-M-D H:m', 'YYYY-M-DD HH:mm',
        'YYYY-M-DD H:mm', 'YYYY-M-DD HH', 'YYYY-M-DD H', 'YYYY-M-DD HH:m', 'YYYY-M-DD H:m'
    ];
    $('#datetimepicker-start input').on('blur', function () {
        if (!moment(this.value, 'YYYY-MM-DD HH:mm', true).isValid()) {
            var date = moment(this.value, otherValidFormats, true);
            if (date.isValid()) {
                var newDate = date.format('YYYY-MM-DD HH:mm');
                start.dates.setFromInput(newDate);
            } else {
                start.clear();
            }
        }
    });
    $('#datetimepicker-end input').on('blur', function () {
        if (!moment(this.value, 'YYYY-MM-DD HH:mm', true).isValid()) {
            var date = moment(this.value, otherValidFormats, true);
            if (date.isValid()) {
                var newDate = date.format('YYYY-MM-DD HH:mm');
                end.dates.setFromInput(newDate);
            } else {
                end.clear();
            }
        }
    });
    start.subscribe(tempusDominus.Namespace.events.error, (e) => {
        if (e.oldDate) {
            var oldDateFormatted = moment(e.oldDate).format('YYYY-MM-DD HH:mm');
            start.dates.setFromInput(oldDateFormatted);
        } else {
            start.clear();
        }
    });
    end.subscribe(tempusDominus.Namespace.events.error, (e) => {
        if (e.oldDate) {
            var oldDateFormatted = moment(e.oldDate).format('YYYY-MM-DD HH:mm');
            end.dates.setFromInput(oldDateFormatted);
        } else {
            end.clear();
        }
    });
    start.subscribe(tempusDominus.Namespace.events.change, (e) => {
        if (e.date) {
            var newMinEndDate = moment(e.date);
            var newMinEndDateFormatted = newMinEndDate.format('YYYY-MM-DD HH:mm');
            if (end.dates.lastPicked && moment(end.dates.lastPicked) < newMinEndDate) {
                end.dates.setFromInput(newMinEndDateFormatted);
            }
            end.updateOptions({
                restrictions: { minDate: newMinEndDateFormatted },
                localization: { format: 'yyyy-MM-dd HH:mm' }
            });
        } else {
            end.updateOptions(dateConfiguration, true);
        }
    });
    end.subscribe(tempusDominus.Namespace.events.change, (e) => {
        if (e.date) {
            var newMaxStartDate = moment(e.date);
            var newMaxStartDateFormatted = newMaxStartDate.format('YYYY-MM-DD HH:mm');
            if (start.dates.lastPicked && moment(start.dates.lastPicked) > newMaxStartDate) {
                start.dates.setFromInput(newMaxStartDateFormatted);
            }
            start.updateOptions({
                restrictions: { maxDate: newMaxStartDateFormatted },
                localization: { format: 'yyyy-MM-dd HH:mm' }
            });
        } else {
            start.updateOptions(dateConfiguration, true);
        }
    });

    var start_value = $('#datetimepicker-start').data('value');
    var end_value = $('#datetimepicker-end').data('value');
    if (start_value) {
        start.dates.setFromInput(moment(start_value).format('YYYY-MM-DD HH:mm'));
    }
    if (end_value) {
        end.dates.setFromInput(moment(end_value).format('YYYY-MM-DD HH:mm'));
    }


    var column_name = gettext('Столбцы');

    $('#satellitesTable').DataTable({
        // the dom field controls the layout and visibility of datatable items
        // and is not intuitive at all. Without layout we have dom: 'Bftrilp'
        // https://datatables.net/reference/option/dom
        dom: '<"row actions"Bp>' +
            '<"responsive-table"tr>' +
            '<"row under-table"ilp>',
        buttons: [{
            extend: 'colvis',
            columnText: function (dt, idx, title) {
                if (idx == 12 && title === '') {
                    return gettext('Страна');
                }
                return title;
            }
        }],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: 'column'
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0
            },
            {
                orderable:true,
                targets:7,
                type:'call_sign'
            }
        ],
        language: {
            info: `${gettext('Показаны от')}  _START_  ${gettext('до')}  _END_  ${gettext('из')}  _TOTAL_  ${gettext('элементов')}`,
            lengthMenu:`${gettext('Показать')} _MENU_ ${gettext('элементов')}`,
            search: gettext('Фильтр:'),
            buttons: {
                colvis: column_name,
            },
            paginate: {
                'first': gettext('Первая'),
                'previous': '«',
                'next': '»',
                'last': gettext('Последняя')
            }
        },
        order: [2, 'asc'],
        pageLength: 25
    });

    const compareNames = (a, b) =>{
        switch (true) {

        case a == '-' && b != a:
            return 1;

        case b == '-' && a != b:
            return -1;

        case b == a:
            return -1;

        case a.match('^[a-z,A-Z]*') > b.match('^[a-z,A-Z]*'):
            return 1;

        case a.match('^[a-z,A-Z]*') < b.match('^[a-z,A-Z]*'):
            return -1;

        case parseInt(a.match(/\d+/)[0]) > parseInt(b.match(/\d+/)[0]):
            return 1;

        case parseInt(a.match(/\d+/)[0]) < parseInt(b.match(/\d+/)[0]):
            return -1;

        case a.match('[a-z,A-Z]*$') > b.match('[a-z,A-Z]*$'):
            return 1;

        case a.match('[a-z,A-Z]*$') < b.match('[a-z,A-Z]*$'):
            return -1;
        }
    };


    $.fn.dataTable.ext.type.order['call_sign-asc'] = (a, b) =>{
        return compareNames(a, b);
    };
    $.fn.dataTable.ext.type.order['call_sign-desc'] = (a, b) =>{
        return compareNames(a, b) == 1 ? -1 : 1;

    };

    function createSatelliteModalForm() {
        $('.create-satellite-link').each(function () {
            $(this).modalForm({
                formURL: $(this).data('form-url'),
                modalID: '#create-satellite-modal'
            });
        });
    }

    $('#satellitesTable_wrapper button').click(function () {
        var list = $('#satellitesTable_wrapper .dropdown-menu a');
        var item;
        var name;
        for (item of list) {
            if (item.text) {
                name = item.text.replace(' ', '_');
                if (item.classList.contains('dt-button-active')) {
                    document.cookie = name + '=true';
                }
                else {
                    document.cookie = name + '=false';
                }
            }
            else {
                item.style.display = 'none';
            }
        }
        $('.dropdown-menu a').click(function () {
            var el = $(this)[0];
            if (el.text) {
                if (el.classList.contains('dt-button-active')) {
                    document.cookie = el.text.replace(' ', '_') + '=true';
                }
                else {
                    document.cookie = el.text.replace(' ', '_') + '=false';
                }
            }
        });
    });

    $('.buttons-colvis').removeClass('btn-secondary').addClass('btn-orange btn-small');

    createSatelliteModalForm();

    $('#create-satellite-modal').on('show.bs.modal', function () {
        var el = $('#id_countries');
        el.hide();
        el.multiselect({
            maxHeight: 200
        });
        el.parent().addClass('multiselect-flex-row');
        el.next().addClass('multiselect-btn-row');
        el.next().children().first().addClass('multiselect-btn-span');
        el.next().children().first().removeClass('text-center');
        el.next().children().first().removeClass('dropdown-toggle');
        el.next().children().last().addClass('multiselect-btn-row');
    });

});
