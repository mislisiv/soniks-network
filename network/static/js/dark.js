const darkmodeOptions = {
    bottom: '64px', // default: '32px'
    right: '32px', // default: '32px'
    left: 'unset', // default: 'unset'
    time: '0.5s', // default: '0.3s'
    mixColor: '#FAFAFA', // default: '#fff'
    backgroundColor: '#121212',  // default: '#fff'
    buttonColorDark: '#000000',  // default: '#100f2c'
    buttonColorLight: '#fff', // default: '#fff'
    saveInCookies: true, // default: true,
    label: '<div class="darkmode-moon-icon"></div>', // default: ''
    autoMatchOsTheme: false // default: true
};
function addDarkmodeWidget() {
    if ((localStorage.getItem('darkmode') == null))
    {
        window.localStorage.setItem('darkmode', 'true');
    }
    // eslint-disable-next-line no-undef
    new Darkmode(darkmodeOptions).showWidget();
}

addDarkmodeWidget();
