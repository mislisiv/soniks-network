/* global gettext */
$(document).ready(satellite_script);
document.addEventListener('obs_changed', satellite_script, false);

var schedule_btn = `<a class="btn btn-small btn-blue" href="/observations/new/" id="new-obs-link">${gettext('Новое наблюдение')}</a>`;
var info_btn = `<a class="btn btn-small btn-orange" href="/" target="_blank" id="satellitePageLink">${gettext('Подробнее')}</a>`;

function satellite_script() {
    'use strict';
    $('#SatelliteModal').on('show.bs.modal', function (event) {
        var satlink = $(event.relatedTarget);
        var modal = $(this);

        $.ajax({
            url: '/satellites-popup/' + satlink.data('id') + '/'
        }).done(function (data) {
            if (data.error) {
                modal.find('.modal-body .card').hide();
                modal.find('.modal-body .error-text').text(data.error);
                modal.find('.modal-body .error-text').show();
                return;
            }
            modal.find('.modal-body .card').show();
            modal.find('.modal-body .error-text').hide();
            modal.find('#modal-buttons').empty().append(info_btn);
            if (data.can_schedule) {
                modal.find('#modal-buttons').append(schedule_btn);
            }
            modal.find('.card-title').text(data.name);
            modal.find('.satellite-names').text(data.names);
            modal.find('#SatelliteModalTitle').text(data.name);
            modal.find('.satellite-id').text(satlink.data('id'));
            modal.find('#satellitePageLink').attr('href', '/satellites/' + satlink.data('sat-id'));
            modal.find('#new-obs-link').attr('href', '/observations/new/?norad=' + satlink.data('id'));
            modal.find('#all-obs-link').attr('href', '/observations/?norad=' + satlink.data('id'));
            modal.find('#good-sat-obs').attr('href', '/observations/?future=0&good=1&bad=0&unknown=0&failed=0&norad=' + satlink.data('id'));
            modal.find('#unknown-sat-obs').attr('href', '/observations/?future=0&good=0&bad=0&unknown=1&failed=0&norad=' + satlink.data('id'));
            modal.find('#bad-sat-obs').attr('href', '/observations/?future=0&good=0&bad=1&unknown=0&failed=0&norad=' + satlink.data('id'));
            modal.find('#future-sat-obs').attr('href', '/observations/?future=1&good=0&bad=0&unknown=0&failed=0&norad=' + satlink.data('id'));
            modal.find('#failed-sat-obs').attr('href', '/observations/?future=0&good=0&bad=0&unknown=0&failed=1&norad=' + satlink.data('id'));
            modal.find('.satellite-success-rate').text(data.success_rate + '%');
            modal.find('.satellite-total-obs').text(data.total_count);
            modal.find('.satellite-good').text(data.good_count);
            modal.find('.satellite-unknown').text(data.unknown_count);
            modal.find('.satellite-bad').text(data.bad_count);
            modal.find('.satellite-future').text(data.future_count);
            modal.find('#transmitters').empty();

            var status = {
                'alive': gettext('активный'),
                'dead': gettext('неактивный'),
                'future': gettext('будущий'),
                're-entered': gettext('сошедший'),
            };
            if (data.status == 'alive' && data.unknown) {
                var status_name = gettext('(неизвестный)');
                modal.find('.satellite-status').text(status[data.status] + status_name);
            }
            else {
                modal.find('.satellite-status').text(status[data.status]);
            }

            var has_status_text = 0;
            var has_unknown_text = 0;
            var has_alive_text = 0;

            var total_text = gettext('Всего наблюдений:');
            var good_text = gettext('Успешные');
            var unknown_text = gettext('Неизвестные');
            var bad_text = gettext('Плохие');
            var future_text = gettext('Будущие');

            if (data.transmitters.length == 0) {
                modal.find('#transmitters').append(`<p class="status-text status-line">${gettext('У спутника не найдено передатчиков')}</p>`);
            }

            $.each(data.transmitters, function (i, transmitter) {
                var status_text = '';
                var inactive_trans_text=gettext('Неактивные передатчики');
                var active_unknown_trans_text=gettext('Активные (неизвестные) передатчики');
                var active_trans_text=gettext('Активные передатчики');
                if (transmitter.status != 'active' && !has_status_text){
                    status_text = `<p class="status-text status-line">${inactive_trans_text}</p>`;
                    has_status_text = 1;
                }
                if (transmitter.status == 'active' && transmitter.unknown && !has_unknown_text){

                    status_text = `<p class="status-text status-line">${active_unknown_trans_text}</p>`;
                    has_unknown_text = 1;
                }
                if (transmitter.status == 'active' && !transmitter.unknown && !has_alive_text){
                    status_text = `<p class="status-text status-line">${active_trans_text}</p>`;
                    has_alive_text = 1;
                }
                var total_count = transmitter.stat.total_count - transmitter.stat.failed_count;
                var success_rate = Math.round(100 * transmitter.stat.good_count / total_count) || 0;
                var unknown_rate = Math.round(100 * (transmitter.stat.unknown_count - transmitter.stat.future_count) / total_count) || 0;
                var bad_rate = Math.round(100 * transmitter.stat.bad_count / total_count) || 0;
                var future_rate = Math.round(100 * transmitter.stat.future_count / total_count) || 0;

                modal.find('#transmitters').append(status_text+`
                    <div class="transmitter card">
                        <div class="card-header row">
                        <div class="transmitter-desc">${transmitter.description}</div>
                        <div class="transmitter-freq">${format_frequency(transmitter.downlink_low)}</div>
                        <div class="obs-all"><a href='/observations/?transmitter_uuid=${transmitter.uuid}'>${total_text} ${total_count} </a></div>
                        </div>
                        <div class="card-body">
                        <div class="progress">
                            <div class="progress-bar pb-green transmitter-good"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="${success_rate}%(${transmitter.stat.good_count} ${good_text})"
                                        role="progressbar" style="width:${success_rate}%"
                                        aria-valuenow="${success_rate}" aria-valuemin="0" aria-valuemax="100"></div>
                            <div class="progress-bar pb-orange transmitter-unknown"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="${unknown_rate}% (${(transmitter.stat.unknown_count - transmitter.stat.future_count)} ${unknown_text})"
                                        role="progressbar" style="width:${unknown_rate}%"
                                        aria-valuenow="${unknown_rate}" aria-valuemin="0" aria-valuemax="100"></div>
                            <div class="progress-bar pb-red transmitter-bad"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="${bad_rate}% (${transmitter.stat.bad_count} ${bad_text})"
                                        role="progressbar" style="width:${bad_rate}%"
                                        aria-valuenow="${bad_rate}" aria-valuemin="0" aria-valuemax="100"></div>
                            <div class="progress-bar pb-blue transmitter-info"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="${future_rate}% (${transmitter.stat.future_count} ${future_text})"
                                        role="progressbar" style="width:${future_rate}%"
                                        aria-valuenow="${future_rate}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        </div>
                    </div>`
                );
            });

            if (data.image) {
                var image_url = data.image;
                modal.find('.modal-img-full').attr('src','/media/'+image_url);
            } else {
                modal.find('.modal-img-full').attr('src', '/static/img/sat.png');
            }
        });
    });
}

function format_frequency(val) {
    if (!Number.isInteger(val)) {
        return gettext('Ошибка');
    }

    if (val === 0) {
        return '0';
    }

    const unit_table = [gettext('Гц'), gettext('КГц'), gettext('МГц'), gettext('ГГц')];
    const div = Math.floor(Math.log10(val) / 3);
    const unit = unit_table[(div > 3) ? 3 : div];

    return (val / (Math.pow(1000, (div > 3) ? 3 : div))).toFixed(3) + ' ' + unit;
}
