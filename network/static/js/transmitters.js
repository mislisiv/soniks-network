/* global gettext */
$(document).ready(function () {
    'use strict';

    $('.checkbox-group input').click(function () {
        var input = $(this);

        if (input.prop('checked')) {
            input.parent().addClass('inactive');
        } else {
            input.parent().removeClass('inactive');
        }
    });
    
    $('#show-all-filters').click(function() {
        $(this).toggleClass('show');
        $('.hidden-filters').slideToggle();

        if($(this)[0].innerText == gettext('Больше фильтров')){
            $(this).text(gettext('Меньше фильтров'));
        }
        else {
            $(this).text(gettext('Больше фильтров'));
        }
    });

    $('.selectpicker').selectpicker();
    $('.filter-section #status-selector input').click(function () {
        var input = $(this);

        if (input.prop('checked')) {
            input.parent().addClass('btn-inactive');
        } else {
            input.parent().removeClass('btn-inactive');
        }
    });

    $('.frequency').each(function () {
        var to_format = $(this).html();
        $(this).html(format_freq(to_format));
    });

    // Baud slider
    const baud_min = 0;
    const baud_max = 375000000;
    var cur_val1 = baud_min;
    var cur_val2 = baud_max;
    if ($('#baud-filter-1').val() && $('#baud-filter-1').val() != baud_min) {
        cur_val1 = $('#baud-filter-1').val();
    }
    if ($('#baud-filter-2').val() && $('#baud-filter-2').val() != baud_max) {
        cur_val2 = $('#baud-filter-2').val();
    }
    $('#slider-range-baud').slider({
        range: true,
        min: baud_min,
        max: baud_max,
        values: [cur_val1, cur_val2],
        step: 1000000,
        slide: function (event, ui) {
            $('#baud-filter-1').val(ui.values[0]);
            $('#baud-filter-2').val(ui.values[1]);
        }
    });
    $('#baud-filter-1').on('input', function () {
        $('#slider-range-baud').slider('values', [$(this).val(), $('#baud-filter-2').val()]);
    });

    $('#baud-filter-2').on('input', function () {
        $('#slider-range-baud').slider('values', [$('#baud-filter-1').val(), $(this).val()]);
    });

    // Downlink slider
    const downlink_min = 0;
    const downlink_max = 40000000000;
    cur_val1 = downlink_min;
    cur_val2 = downlink_max;
    if ($('#downlink-filter-1').val() && $('#downlink-filter-1').val() != downlink_min) {
        cur_val1 = $('#downlink-filter-1').val();
    }
    if ($('#downlink-filter-2').val() && $('#downlink-filter-2').val() != downlink_max) {
        cur_val2 = $('#downlink-filter-2').val();
    }
    $('#slider-range-downlink').slider({
        range: true,
        min: downlink_min,
        max: downlink_max,
        values: [cur_val1, cur_val2],
        step: 100000000,
        slide: function (event, ui) {
            $('#downlink-filter-1').val(ui.values[0]);
            $('#downlink-filter-2').val(ui.values[1]);
        }
    });
    $('#downlink-filter-1').on('input', function () {
        $('#slider-range-downlink').slider('values', [$(this).val(), $('#downlink-filter-2').val()]);
    });

    $('#downlink-filter-2').on('input', function () {
        $('#slider-range-downlink').slider('values', [$('#downlink-filter-1').val(), $(this).val()]);
    });

    function createTransmitterModalForm() {
        $('.create-transmitter-link').each(function () {
            $(this).modalForm({
                formURL: $(this).data('form-url'),
                modalID: '#create-transmitter-modal'
            });
        });
    }

    function editTransmitterModalForm() {
        $('.edit-transmitter-link').each(function () {
            $(this).modalForm({
                formURL: $(this).data('form-url'),
                modalID: '#edit-transmitter-modal'
            });
        });
    }


    createTransmitterModalForm();
    editTransmitterModalForm();

    // Check if filters should be displayed

    if (window.location.hash == '#collapseFilters') {
        $('#collapseFilters').hide();
    } else if ($('#collapseFilters').data('filtered') == 'True') {
        $('.transmitters-actions a').first().click();
    }

    $('.edit-transmitter-link').click(function (e) {
        $('#edit-transmitter-modal').show(function () {
            var uuid = e.currentTarget.dataset.uuid;
            var modal = $(this);
            $.ajax({
                url: '/api/transmitters/?uuid=' + uuid
            }).done(function (data) {
                var transmitter = data[0];
                modal.find('#id_uuid_by_changes').attr('value', uuid);
                modal.find('#id_uuid_by_changes').attr('readonly', true);
                modal.find('#id_description').attr('value', transmitter['description']);
                modal.find('#id_downlink_low').attr('value', transmitter['downlink_low']);
                modal.find('#id_baud').attr('value', transmitter['baud']);
                var i;
                var options = ['type', 'mode', 'status'];
                for (var option of options) {
                    modal.find('#id_' + option).addClass('selectpicker');
                    if (transmitter[option]) {
                        modal.find('#id_' + option).attr('value', transmitter[option]);
                        var values = modal.find('#id_' + option + ' option');
                        for (i = 0; i < values.length; i++) {
                            if (values[i].innerHTML == transmitter[option]) {
                                values[i].setAttribute('selected', true);
                                break;
                            }
                        }
                    }
                }
                modal.find('#id_mode').attr('data-live-search', true);
                $('.selectpicker').selectpicker();

            });
        });
    });

    var column_name = gettext('Столбцы');

    $('#transmittersTable').DataTable({
        // the dom field controls the layout and visibility of datatable items
        // and is not intuitive at all. Without layout we have dom: 'Bftrilp'
        // https://datatables.net/reference/option/dom
        dom: '<"row actions"Bp>' +
            '<"responsive-table"tr>' +
            '<"row under-table"ilp>',
        buttons: [{
            extend: 'colvis',
            columnText: function (dt, idx, title) {
                if (idx == 12 && title === '') {
                    return gettext('Страна');
                }
                return title;
            }
        }],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: 'column'
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0
            },
            {
                type: 'natural',
                targets: [9]
            },
            {
                type: 'num-fmt',
                targets: [5]
            }
        ],
        language: {
            info: `${gettext('Показаны от')}  _START_  ${gettext('до')}  _END_  ${gettext('из')}  _TOTAL_  ${gettext('элементов')}`,
            lengthMenu:`${gettext('Показать')} _MENU_ ${gettext('элементов')}`,
            search: gettext('Фильтр:'),
            buttons: {
                colvis: column_name,
            },
            paginate: {
                'first': gettext('Первая'),
                'previous': '«',
                'next': '»',
                'last': gettext('Последняя')
            }
        },
        order: [1, 'asc'],
        pageLength: 25
    });

    $('#transmittersTable_wrapper button').click(function () {
        var list = $('#transmittersTable_wrapper .dropdown-menu a');
        var item;
        var name;
        for (item of list) {
            if (item.text) {
                name = item.text.replace(' ', '_');
                if (item.classList.contains('dt-button-active')) {
                    document.cookie = 'T_' + name + '=true';
                }
                else {
                    document.cookie = 'T_' + name + '=false';
                }
            }
            else {
                item.style.display = 'none';
            }
        }
        $('.dropdown-menu a').click(function () {
            var el = $(this)[0];
            if (el.text) {
                if (el.classList.contains('dt-button-active')) {
                    document.cookie = 'T_' + el.text.replace(' ', '_') + '=true';
                }
                else {
                    document.cookie = 'T_' + el.text.replace(' ', '_') + '=false';
                }
            }

        });
    });

    $('.buttons-colvis').removeClass('btn-secondary').addClass('btn-orange btn-small');
});



function format_freq(frequency_string) {
    var frequency = Number(frequency_string);
    if (isNaN(frequency) || frequency == '') {
        return 'None';
    } else if (frequency < 1000) {
        // Frequency is in Hz range
        return frequency.toFixed(3) + ' Hz';
    } else if (frequency < 1000000) {
        return (frequency / 1000).toFixed(3) + ' kHz';
    } else {
        return (frequency / 1000000).toFixed(3) + ' MHz';
    }
}
