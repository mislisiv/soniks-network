/* global Swiper,gettext,Fancybox*/

// About page scripts

// block-sattelite-gallery__slider
window.addEventListener('load', () => {
    const satteliteSlider = new Swiper('.block-sattelite-gallery__slider', {
        // Optional parameters
        direction: 'horizontal',
        spaceBetween: 15,
        grabCursor: true,
        slidesPerView: 1.6,
        loop: false,
        // Navigation arrows
        navigation: {
            nextEl: '.block-sattelite-gallery__slider-swiper-navigation-next',
            prevEl: '.block-sattelite-gallery__slider-swiper-navigation-prev',
        },
        breakpoints: {
            992: {
                slidesPerView: 2.6,
            },
        },
    });
    if (satteliteSlider) {
        //pass
    }
});

Fancybox.bind('[data-fancybox]', {
    Toolbar: {
        display: {
            right: ['close'],
        },
    },
    l10n: {
        CLOSE: gettext('Закрыть'),
        NEXT: gettext('Следующий'),
        PREV: gettext('Предыдущий'),
        MODAL: gettext('Вы можете закрыть окно нажатием ESC'),
        ERROR: gettext('Ошибка, попробуйте повторить попытку'),
        IMAGE_ERROR: gettext('Изображение не найдено'),
        ELEMENT_NOT_FOUND: gettext('HTML элемент не найден'),
        AJAX_NOT_FOUND: gettext('Ошибка загрузки AJAX: Не найдено'),
        AJAX_FORBIDDEN: gettext('Ошибка загрузки AJAX: Запрещено'),
        IFRAME_ERROR: gettext('Ошибка при загрузке фрейма'),
        TOGGLE_ZOOM: gettext('Приблизить'),
        TOGGLE_THUMBS: gettext('Миниатюры'),
        TOGGLE_SLIDESHOW: gettext('Слайдшоу'),
        TOGGLE_FULLSCREEN: gettext('Полноэкранный режим'),
        DOWNLOAD: gettext('Загрузить')
    }
});
