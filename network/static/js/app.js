$(document).ready(function () {
    'use strict';

    // Add current copyright year
    var current_year = '-' + new Date().getFullYear();
    $('#copy').text(current_year);

    // Initialize tooltips
    $('[data-toggle="tooltip"]').tooltip();


    // Update suggestion count
    var item = $('a#suggestion-item');

    function update_suggestion() {
        var old_count = parseInt(item.find('sup').text());
        $.ajax({
            url: '/suggestion_count/'
        }).done(function (data) {
            var count = data['count_suggestions'];
            item.find('sup').remove();
            if (count > 0) {
                item.append(`
                <sup class="counter">${count}</sup>
                `);
                if (old_count > 0 && count > old_count && location.pathname == '/suggestion/') {
                    location.reload();
                }
            }
        });
    }

    if (item.length) {
        update_suggestion();
        setInterval(update_suggestion, 60 * 1000);
    }
});
