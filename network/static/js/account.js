$(document).ready(function () {
    'use strict';
    
    $('#userForm input').addClass('w-100');
    
    $('#id_first_name').val($('#userForm').data('firstname'));
    $('#id_last_name').val($('#userForm').data('lastname'));
    $('#id_email').val($('#userForm').data('email')).attr('readonly', true);

    $('#showAPI').click(function() {
        $('.block-copy').slideToggle(200);
    });

    var btnCopy = document.getElementsByClassName('btn-copy');
    for (var i = 0; i < btnCopy.length; i++){
        btnCopy[i].addEventListener('click', event => {
            var copyTarget = event.target.getAttribute('data-target');
            var copyText = document.getElementById(copyTarget).innerHTML;
            copyText = copyText.replace('<br>', '\n');
            copyFunction(copyText);
        });
    }
    

});

function copyFunction(string) {
    const copyText = string;
    const textArea = document.createElement('textarea');
    textArea.textContent = copyText;
    document.body.append(textArea);
    textArea.select();
    document.execCommand('copy');
    textArea.remove();
}