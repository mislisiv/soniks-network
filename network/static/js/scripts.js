// eslint-disable-next-line no-unused-vars
/* global Darkmode, MicroModal */

$(document).ready(function () {
    // All pages scripts
    var body = $('body');
    header_sticky();
    window.onscroll = header_sticky;

    // header navbar toggler
    var headerNavbar = $('#headerNavbar');
    var toggler = $('#headerToggler');
    toggler.click(function() {
        body.toggleClass('navbar-show');
        toggler.toggleClass('collapsed');
        headerNavbar.toggleClass('show');
    });

    // close modal when click outside modal areal
    window.onclick = function (event) {
        if (body.hasClass('navbar-show') && event.target['tagName'] == 'BODY') {
            body.toggleClass('navbar-show');
            toggler.toggleClass('collapsed');
            headerNavbar.toggleClass('show');
        }
    };

    // get actual year
    $('#actual-year').text(new Date().getFullYear());

    // add link to clickable row
    $('.clickable-row').click(function (e) {
        if (e.target.nodeName != 'A') {
            var href = $(this).find('a').attr('href');
            e.preventDefault();
            if (e.ctrlKey) {
                window.open(href, '_blank');
            } else {
                window.location = href;
            }
        }
    });

    MicroModal.init({
        awaitCloseAnimation: true,
    });
    var btnsModal = document.querySelectorAll('[data-target-type="modal"]');
    for (let btn of btnsModal){
        btn.addEventListener('click', () => {
            var target = btn.getAttribute('data-target');
            MicroModal.show(target);
        });
    }

    var navTabsBtn = document.querySelectorAll('[data-toggle="tab"]');
    for (let btn of navTabsBtn){
        btn.addEventListener('click', () => {
            if (!btn.parentElement.classList.contains('active')){
                for (let btn of navTabsBtn){
                    if(btn.parentElement.classList.contains('active')){
                        btn.parentElement.classList.remove('active');
                    }
                }
                btn.parentElement.classList.add('active');
            }
            var targetTab = btn.getAttribute('data-parent-tab');
            var target = btn.getAttribute('data-target-tab');
            var tabpane = document.querySelectorAll('#'+targetTab + ' .tab-pane');
            var tabpaneActive = document.querySelector('#'+targetTab + ' .tab-pane[id="'+target+'"]');
            for (let tab of tabpane){
                if (tab.classList.contains('active')){
                    tab.classList.remove('active');
                }
            }
            tabpaneActive.classList.add('active');
            
        });
    }

    $('.btn-back').click(function () {
        history.back();
    });

    $('img.btn-copy').click(function() {
        $(this).attr('data-original-title', 'TLE скопировано!');
    });
});

function header_sticky() {
    var header = $('header');
    if (window.innerWidth > 767) {
        let scroll = window.scrollY;
        if (scroll > 0) {
            header.addClass('header-sticky');
        } else {
            header.removeClass('header-sticky');
        }
    } else if (header.hasClass('header-sticky')) {
        header.removeClass('header-sticky');
    }
}
