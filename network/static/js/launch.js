/* global Fancybox gettext */

$(document).ready(function () {
    'use strict';

    $('#delete-obs-btn').click(function() {
        var launch_id = $('#info').data('id');
        var auto = $('#is-auto-select option:selected').val();
        var period = $('#is-period-select option:selected').val();
        var data = {
            'period': period,
            'auto': auto
        };
        var confirm_text = gettext('Вы правда хотите удалить будущие наблюдения на ') + period + gettext(' часов?');
        if(!confirm(confirm_text)) {return;} 
        $(this).prop('disabled', true);
        $('#delete-obs-spinner').show();
        $.ajax({
            type: 'POST',
            url: `/launches/${launch_id}/delete_observations/`,
            dataType: 'json',
            data: data,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
            }
        }).done(function(results) {
            var success_msg = results.success;
            var error_msg = results.error;
            if (success_msg) {
                show_alert('success', success_msg);
            }
            else {
                show_alert('success', 'Наблюдений за выбранный период не найдено');
            }
            if (error_msg) {
                show_alert('danger', error_msg);
            }
        }).fail(function(error) {
            var error_msg = error.responseJSON.error;
            show_alert('danger', error_msg);
        }).always(function() {
            $('#delete-obs-spinner').hide();
        });
    });

    $('#sats').DataTable({
        // the dom field controls the layout and visibility of datatable items
        // and is not intuitive at all. Without layout we have dom: 'Bftrilp'
        // https://datatables.net/reference/option/dom
        dom: '<"responsive-table"tr>',
        paging: false
    });
});

function show_alert(type, msg) {
    var alert_container = $('#alert-messages');
    if (!alert_container.find('.row').length) {
        alert_container.append('<div class="row"></div>');
    }
    $('#alert-messages .row').append(
        `<div class="alert alert-${type} alert-dismissible" role="alert">
          ${msg}
          <button type="button" class="btn btm-sm btn-without-border-red" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" class="bi bi-x"></span>
          </button>
        </div>`);
}

Fancybox.bind('[data-fancybox]', {
    Toolbar: {
        display: {
            right: ['close'],
        },
    },
});