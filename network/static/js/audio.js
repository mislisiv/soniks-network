/* eslint-disable no-unused-vars */
/* global WaveSurfer   */


$(document).ready(function () {
    
    $('a[href="#audio-tab"]').on('shown.bs.tab', function () {
        load_audio_tab();
        $('a[href="#audio-tab"]').off('shown.bs.tab');
    });

    function load_audio_tab(){
        $('.wave').each(function () {
            var $this = $(this);
            var wid = $this.data('id');
            var data_audio_url = $this.data('audio');
            var container_el = '#data-' + wid;
            var ws = WaveSurfer.create({
                container: container_el,
                normalize:true,
                waveColor: '#fe5e2f',
                progressColor: '#0097fb',
                mediaControls:true,
                dragToSeek: true,
                height:200,
                plugins:[
                    WaveSurfer.Spectrogram.create({
                        labels: true,
                        height:200
                    }),
                ]
            });
            ws.load(data_audio_url);
            $('#wave-zoom').on('change',function(e) 
            {
                if ($('#wave-zoom').val()=='map'){

                    const minPxPerSec = 1;
                    ws.zoom(minPxPerSec);
                }
                else{
                    const minPxPerSec = 50;
                    ws.zoom(minPxPerSec);
                }
            });
        });
    }
});
