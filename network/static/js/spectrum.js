/* global d3,gettext */

/* eslint-disable no-unused-vars */

$(document).ready(function () {
    var transmitters;
    var satellites;
    var transmittersChart;
    var transmitter_filters = { 'status': 'active' };
    var satellite_filters = { 'status': 'alive' };
    var active_transmitter_text=gettext('Активные');
    var unknown_transmitter_text=gettext('Неизвестные');
    var inactive_transmitter_text=gettext('Неактивные');
    var launch_filter = '';
    var transmitter_launch_filter = '';
    var launch_id = $('#info').data('id');

    if (launch_id) {
        launch_filter = `?launch__id=${launch_id}`;
        transmitter_launch_filter = `?satellite__launch__id=${launch_id}`;
        delete satellite_filters['status'];
        delete transmitter_filters['status'];
    }

    var transmiter_status = {
        [active_transmitter_text]: 'active',
        [unknown_transmitter_text]: 'unknown',
        [inactive_transmitter_text]: 'inactive',
    };
    $('#spectrum-spinner').show();

    $('#transmitter-status-filter').on('change', function (e) {
        e.stopPropagation();
        e.stopImmediatePropagation();

        var items = $(this).next().next().find('li.selected');
        var status = [];
        var transmitter_text = '';

        if (items.length == 3 || items.length == 0) {
            delete transmitter_filters['status'];
            transmitter_text = gettext('Передатчики: все');
            $('#transmitter-status-dropdown .filter-option-inner-inner').text(transmitter_text);
        }
        else {
            transmitter_text = gettext('Передатчики: ');

            for (var item of items) {
                status.push(transmiter_status[item.innerText]);
                transmitter_text += item.innerText + ', ';
            }
            if (transmitter_text.endsWith(', ')) {
                transmitter_text = transmitter_text.slice(0, -2);
            }
            transmitter_filters = {'status': status};
            $('#transmitter-status-dropdown .filter-option-inner-inner').text(transmitter_text);
        }

        $('#transmitters-chart-container').empty();

        transmittersChart = renderTransmittersChart({
            el: document.querySelector('#transmitters-chart-container'),
            transmitters: JSON.parse(JSON.stringify(transmitters)),
            satellites_list: JSON.parse(JSON.stringify(satellites)),
            transmitter_filters: transmitter_filters,
            satellite_filters: satellite_filters,
        });
        transmittersChart.zoom(
            434.5,
            438.5
        );

        $(this).parent().dropdown('toggle');
    });

    var active_satellite_text=gettext('Активные');
    var unknown_satellite_text=gettext('Неизвестные');
    var reentered_satellite_text=gettext('Сошедшие');
    var satellite_status = {
        [active_satellite_text]: 'alive',
        [unknown_satellite_text]: 'unknown',
        [reentered_satellite_text]: 're-entered',
    };


    $('#satellite-status-filter').on('change', function (e) {
        e.stopPropagation();
        e.stopImmediatePropagation();

        var items = $(this).next().next().find('li.selected');
        var status = [];
        var satellite_text = '';

        if (items.length == 3 || items.length == 0) {
            delete satellite_filters['status'];
            satellite_text = gettext('Спутники: все');
            $('#satellite-status-dropdown .filter-option-inner-inner').text(satellite_text);
        }
        else {
            satellite_text = gettext('Спутники: ');
            for (var item of items) {
                status.push(satellite_status[item.innerText]);
                satellite_text += item.innerText + ', ';
            }
            if (satellite_text.endsWith(', ')) {
                satellite_text = satellite_text.slice(0, -2);
            }
            satellite_filters = {'status': status};
            $('#satellite-status-dropdown .filter-option-inner-inner').text(satellite_text);
        }

        $('#transmitters-chart-container').empty();
        transmittersChart = renderTransmittersChart({
            el: document.querySelector('#transmitters-chart-container'),
            transmitters: JSON.parse(JSON.stringify(transmitters)),
            satellites_list: JSON.parse(JSON.stringify(satellites)),
            transmitter_filters: transmitter_filters,
            satellite_filters: satellite_filters,
        });

        transmittersChart.zoom(
            434.5,
            438.5
        );

        $(this).parent().dropdown('toggle');
    });

    $('#spectrum-filters').hide();

    function load_spectrum_tab() {
        $.ajax({
            url: '/api/transmitters/' + transmitter_launch_filter,
            dataType: 'json',
        }).done(function(response_transmitters) {
            $.ajax({
                url: '/api/satellites/' + launch_filter,
                dataType: 'json',
            }).done(function(response_satellites) {
                $('#spectrum-spinner').hide();
                transmitters = response_transmitters;
                satellites = response_satellites;
                transmittersChart = renderTransmittersChart({
                    el: document.querySelector('#transmitters-chart-container'),
                    transmitters: JSON.parse(JSON.stringify(transmitters)),
                    satellites_list: JSON.parse(JSON.stringify(satellites)),
                    transmitter_filters: transmitter_filters,
                    satellite_filters: satellite_filters
                });

                transmittersChart.zoom(
                    434.5,
                    438.5
                );

                var transmitters_active = gettext('Передатчики: Активные');
                var satellites_active = gettext('Спутники: Активные');

                $('#transmitter-status-dropdown .filter-option-inner-inner').text(transmitters_active);
                $('#satellite-status-dropdown .filter-option-inner-inner').text(satellites_active);
                $('#spectrum-filters').show();
            });
        });
    }
    load_spectrum_tab();
});

function renderTransmittersChart({ el, transmitters, satellites_list, transmitter_filters, satellite_filters }) {
    var transmitter_color=gettext('Приемник');
    var transceiver_color=gettext('Передатчик');
    var transponder_color=gettext('Приемопередатчик');
    var type_translate={'Приемник':transmitter_color,'Передатчик':transceiver_color,'Приемопередатчик':transponder_color};
    var colorType = {[transmitter_color]: 'Transmitter', [transceiver_color]: 'Transceiver', [transponder_color]: 'Transponder'};
    var violator_text = gettext('Нарушитель частот');

    d3.formatDefaultLocale({
        decimal: '.',
        thousands: '',
        grouping: [3],
    });

    const data = processData(transmitters, satellites_list, transmitter_filters, satellite_filters);
    const container = d3.select(el).classed('transmitters-chart', true);
    const tooltip = renderTooltip(container);

    var legend_items = [];
    data.types.forEach(entry => {
        legend_items.push({
            'name': entry,
            'color': colorType[entry]
        });
    });

    legend_items.push({
        'name': violator_text,
        'color': 'violator'
    });

    renderLegend(container, legend_items);
    const { zoom } = renderChart(container, data, tooltip);

    function processData(transmitters, satellites_list, transmitter_filters, satellite_filters) {
        let satellites = {};

        satellites_list.forEach(function (item) {
            satellites[item.norad] = item;
        });


        let sorted = transmitters
            .map((d) => {
                d['satellite'] = satellites[d.satellite_norad];
                const width = (d.baud || 9600) / 1e6;
                const x0 = d.downlink_low / 1e6 - width / 2;
                const x1 = d.downlink_low / 1e6 + width / 2;
                return {
                    x0,
                    x1,
                    width,
                    type: type_translate[d.type],
                    data: d,
                };
            })
            .filter(function (d) {
                for (var prop in transmitter_filters) {
                    if (prop == 'status') {
                        if (!transmitter_filters[prop].includes('unknown') || !d.data['unknown']) {
                            if (!transmitter_filters[prop].includes(d.data[prop])) {
                                return false;
                            }

                            if (d.data['unknown']) {
                                return false;
                            }
                        }
                    }
                    else if (d.data[prop] !== transmitter_filters[prop]) {
                        return false;
                    }
                }

                for (prop in satellite_filters) {
                    if (prop == 'status') {
                        if (!satellite_filters[prop].includes('unknown') || !d.data['satellite']['unknown']) {
                            if (!satellite_filters[prop].includes(d.data['satellite'][prop])) {
                                return false;
                            }

                            if (d.data['satellite']['unknown']) {
                                return false;
                            }
                        }
                    }
                    else if (d.data['satellite'][prop] !== satellite_filters[prop]) {
                        return false;
                    }
                }
                return true;
            })
            .sort((a, b) => d3.ascending(a.x0, b.x0));
        const xMin = d3.min(sorted, (d) => d.x0);
        const xMax = d3.max(sorted, (d) => d.x1);
        const rows = [];
        var types = [gettext('Приемник'),gettext('Передатчик'), gettext('Приемопередатчик')];

        types.forEach((type) => {
            let typeSorted = sorted.filter((d) => type_translate[d.data.type] === type);
            while (typeSorted.length > 0) {
                const picked = [];
                const left = [];
                let x = xMin - 1;
                typeSorted.forEach((d, i) => {
                    if (d.x0 > x) {
                        picked.push(i);
                        x = d.x1;
                    } else {
                        left.push(i);
                    }
                });
                const row = picked.map((i) => typeSorted[i]);
                row.type = type;
                rows.push(row);
                typeSorted = left.map((i) => typeSorted[i]);
            }
        });
        return {
            rows,
            types,
            xMin,
            xMax,
        };
    }


    function renderTooltip(container) {
        const tooltip = container.append('div').attr('class', 'chart-tooltip');
        let tooltipBox,
            containerBox;

        function show(content, colorClass) {
            tooltip.attr('class', `chart-tooltip ${colorClass}`).html(content);
            tooltipBox = tooltip.node().getBoundingClientRect();
            containerBox = container.node().getBoundingClientRect();
            tooltip.classed('show', true);
        }

        function hide() {
            tooltip.classed('show', false);
        }

        function move(event) {
            let [x, y] = d3.pointer(event, container.node());
            x = x - tooltipBox.width / 2;
            if (x < 0) { x = 0; }
            if (x + tooltipBox.width > containerBox.width) { x = containerBox.width - tooltipBox.width; }
            y = y - tooltipBox.height - 8;
            if (y < 0) {
                y = y + tooltipBox.height + 8 * 2;
            }
            tooltip.style('transform', `translate(${x}px,${y}px)`);
        }

        return {
            show,
            hide,
            move,
        };
    }


    function renderLegend(container, items) {
        const legend = container.append('div').attr('class', 'chart-legend');
        const legendItem = legend
            .selectAll('.legend-item')
            .data(items)
            .enter().append('div')
            .attr('class', 'legend-item');
        legendItem
            .append('div')
            .attr('class', (d) => `legend-swatch color-${d.color.toLowerCase()}`);
        legendItem
            .append('div')
            .attr('class', 'legend-value')
            .text((d) => d.name);
    }

    function renderChart(container, data, tooltip) {
        const dimension = {
            rowHeight: 20,
            rowPadding: 4,
            margin: {
                top: 40,
                right: 20,
                bottom: 20,
                left: 20,
            },
            totalWidth: null,
            get width() {
                return this.totalWidth - this.margin.left - this.margin.right;
            },
            get height() {
                return this.rowHeight * data.rows.length;
            },
            get totalHeight() {
                return this.height + this.margin.top + this.margin.bottom;
            },
            minBarWidth: 2,
        };

        const zoom = d3.zoom().on('zoom', zoomed);

        const x = d3.scaleLinear().domain([data.xMin, data.xMax]);
        let xz = x.copy();

        const y = d3
            .scaleBand()
            .domain(d3.range(data.rows.length))
            .range([0, dimension.height])
            .paddingInner(dimension.rowPadding / dimension.rowHeight)
            .paddingOuter(dimension.rowPadding / dimension.rowHeight / 2);

        const xAxisTop = (g, x) =>
            g.call(
                d3
                    .axisTop(x)
                    .ticks(dimension.width / 80)
                    .tickPadding(6)
                    .tickSizeInner(-dimension.height)
                    .tickSizeOuter(0)
            );
        const xAxisBottom = (g, x) =>
            g.call(
                d3
                    .axisBottom(x)
                    .ticks(dimension.width / 80)
                    .tickPadding(6)
                    .tickSize(0)
            );

        const svg = container
            .append('svg')
            .attr('class', 'chart-svg')
            .call(zoom)
            .on('click', () => { });
        const clipRect = svg
            .append('defs')
            .append('clipPath')
            .attr('id', 'transmitters-chart-bars-clip')
            .append('rect');
        const g = svg
            .append('g')
            .attr(
                'transform',
                `translate(${dimension.margin.left},${dimension.margin.top})`
            );
        const axisUnit = g
            .append('text')
            .attr('class', 'axis axis__unit')
            .attr('y', -20)
            .attr('text-anchor', 'end')
            .text(gettext('Частота (МГц)'));
        const gAxisTop = g.append('g').attr('class', 'axis axis--top');
        const gAxisBottom = g
            .append('g')
            .attr('class', 'axis axis--bottom')
            .attr('transform', `translate(0,${dimension.height})`);
        const gRows = g
            .append('g')
            .attr('class', 'bar-rows')
            .attr('clip-path', 'url(#transmitters-chart-bars-clip)');
        const gRow = gRows
            .selectAll('.bar-row')
            .data(data.rows)
            .join('g')
            .attr('class', 'bar-row')
            .attr('transform', (_, i) => `translate(0,${y(i)})`);
        const bar = gRow
            .selectAll('.bar')
            .data((d) => d)
            .join('rect')
            .attr('class', (d) => {
                let class_str = 'bar';
                if (d.data.satellite.is_frequency_violator) {
                    class_str += ' color-violator';
                } else {
                    class_str += ` color-${colorType[d.type].toLowerCase()}`;
                }
                if (d.data.status != 'active') {
                    class_str += ' color-inactive';
                }
                if (d.data.unknown) {
                    class_str += ' color-unknown';
                }
                return class_str;
            })
            .attr('height', y.bandwidth())
            .on('mouseenter', entered)
            .on('mouseleave', left)
            .on('mousemove', moved)
            .on('click', clicked);

        resize();
        window.addEventListener('resize', resize);

        function entered(_, d) {
            d3.select(this).raise();
            tooltip.show(tooltipContent(d.data), `color-${d.type.toLowerCase()}`);
        }

        function left() {
            tooltip.hide();
        }

        function moved(event) {
            tooltip.move(event);
        }

        function clicked(_, d) {
            window.open('/satellites/' + d.data.satellite.sat_id + '#transmitters-cards', 'transmitter');
        }

        function zoomed({ transform }) {
            const range = x.range().map(transform.invertX, transform);
            const domain = range.map(x.invert, x);
            xz.domain(domain);
            render();
        }

        function resize() {
            dimension.totalWidth = container.node().clientWidth;
            svg.attr('viewBox', [0, 0, dimension.totalWidth, dimension.totalHeight]);
            clipRect.attr('width', dimension.width).attr('height', dimension.height);
            axisUnit.attr('x', dimension.width);
            x.range([0, dimension.width - dimension.minBarWidth]);
            xz.range([0, dimension.width - dimension.minBarWidth]);
            render();
        }

        function render() {
            gAxisTop.call(xAxisTop, xz);
            gAxisBottom.call(xAxisBottom, xz);
            bar
                .attr('x', (d) => xz(d.x0))
                .attr('width', (d) =>
                    Math.max(dimension.minBarWidth, xz(d.x1) - xz(d.x0))
                );
        }

        function tooltipContent(d) {
            var sat_title = gettext('Спутник');
            var sat_type = gettext('Тип');
            var sat_description = gettext('Описание');
            var sat_downlink = gettext('Канал вниз');
            var sat_mode = gettext('Модуляция');
            var sat_baud = gettext('Бод');
            var sat_measure=gettext('МГц');
            return `
        <table>
          <tbody>
          <tr>
              <td>${sat_title}</td>
              <td>${d.satellite.name}</td>
            </tr>
            <tr>
              <td>NORAD ID</td>
              <td>${d.satellite_norad}</td>
            </tr>

            <tr>
              <td>${sat_type}</td>
              <td>${type_translate[d.type]}</td>
            </tr>
            <tr>
              <td>${sat_description}</td>
              <td>${d.description}</td>
            </tr>

            <tr>
              <td>${sat_downlink}</td>
              <td>${d3.format(',')(d.downlink_low / 1e6) + sat_measure}</td>
            </tr>
            <tr>
              <td>${sat_mode}</td>
              <td>${d.downlink_mode}</td>
            </tr>

            <tr>
              <td>${sat_baud}</td>
              <td>${d.baud ? d3.format(',')(d.baud) : ''}</td>
            </tr>
          </tbody>
        </table>
      `;
        }

        return {
            zoom: (minFreq, maxFreq) => {
                svg
                    .transition()
                    .call(
                        zoom.transform,
                        d3.zoomIdentity
                            .scale(dimension.width / (x(maxFreq) - x(minFreq)))
                            .translate(-x(minFreq), 0)
                    );
            },
        };


    }

    return {
        zoom,
    };
}