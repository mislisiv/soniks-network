/* global gettext */
$(document).ready(function () {
    const launchTimeUTC = new Date(document.getElementById('launched_timer').dataset.startDate);

    /**
     * Update the countdown timer, displaying the number of days, hours, minutes, and
     * seconds until the launch. If the launch has already happened, display
     * 'Запущен'.
     */
    function updateCountdown() {
        const now = new Date().getTime();
        let timeLeft = launchTimeUTC - now;
        if (timeLeft < 0) {
            document.getElementById('launched_timer').innerHTML = gettext('Запущен');
            return;
        }
        const days = Math.floor(timeLeft / (1000 * 60 * 60 * 24));
        timeLeft %= (1000 * 60 * 60 * 24);

        const hours = Math.floor(timeLeft / (1000 * 60 * 60));
        timeLeft %= (1000 * 60 * 60);

        const minutes = Math.floor(timeLeft / (1000 * 60));
        timeLeft %= (1000 * 60);

        const seconds = Math.floor(timeLeft / 1000);

        const countdownString = `${days}Д. ${hours}Ч. ${minutes}М. ${seconds}С.`;

        document.getElementById('launched_timer').innerHTML = countdownString;
    }
    setInterval(updateCountdown, 1000);

});
