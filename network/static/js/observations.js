/*global moment tempusDominus,gettext*/

$(document).ready(function () {
    'use strict';

    var locale=document.documentElement.lang;

    $('#show-all-filters').click(function() {
        $(this).toggleClass('show');
        $('.hidden-filters').slideToggle();

        if($(this)[0].innerText == gettext('Больше фильтров')){
            $(this).text(gettext('Меньше фильтров'));
        }
        else {
            $(this).text(gettext('Больше фильтров'));
        }
    });

    tempusDominus.extend(window.tempusDominus.plugins.customDateFormat);
    var dateConfiguration = {
        useCurrent: false,
        display: {
            icons: {
                type: 'icons',
                time: 'bi bi-clock',
                date: 'bi bi-calendar3',
                up: 'bi bi-arrow-up',
                down: 'bi bi-arrow-down',
                previous: 'bi bi-chevron-left',
                next: 'bi bi-chevron-right',
                today: 'bi bi-calendar-check',
                clear: 'bi bi-trash',
                close: 'bi bi-x-lg'
            },
            sideBySide: true,
            components: {
                useTwentyfourHour: true
            },
            buttons: {
                close: true
            }
        },
        localization: {
            startOfTheWeek: 1,
            format: 'yyyy-MM-dd HH:mm',
            hourCycle: 'h23',
            locale: locale
        }
    };
    var start = new tempusDominus.TempusDominus(document.getElementById('datetimepicker-start'), dateConfiguration);
    var end = new tempusDominus.TempusDominus(document.getElementById('datetimepicker-end'), dateConfiguration);

    const otherValidFormats = [
        'YYYY-MM-DD H:mm', 'YYYY-MM-DD HH', 'YYYY-MM-DD H', 'YYYY-MM-DD HH:m',
        'YYYY-MM-DD H:m', 'YYYY-MM-D HH:mm', 'YYYY-MM-D H:mm', 'YYYY-MM-D HH',
        'YYYY-MM-D H', 'YYYY-MM-D HH:m', 'YYYY-MM-D H:m', 'YYYY-M-D HH:mm', 'YYYY-M-D H:mm',
        'YYYY-M-D HH', 'YYYY-M-D H', 'YYYY-M-D HH:m', 'YYYY-M-D H:m', 'YYYY-M-DD HH:mm',
        'YYYY-M-DD H:mm', 'YYYY-M-DD HH', 'YYYY-M-DD H', 'YYYY-M-DD HH:m', 'YYYY-M-DD H:m'
    ];
    $('#datetimepicker-start input').on('blur', function () {
        if (!moment(this.value, 'YYYY-MM-DD HH:mm', true).isValid()) {
            var date = moment(this.value, otherValidFormats, true);
            if (date.isValid()) {
                var newDate = date.format('YYYY-MM-DD HH:mm');
                start.dates.setFromInput(newDate);
            } else {
                start.clear();
            }
        }
    });
    $('#datetimepicker-end input').on('blur', function () {
        if (!moment(this.value, 'YYYY-MM-DD HH:mm', true).isValid()) {
            var date = moment(this.value, otherValidFormats, true);
            if (date.isValid()) {
                var newDate = date.format('YYYY-MM-DD HH:mm');
                end.dates.setFromInput(newDate);
            } else {
                end.clear();
            }
        }
    });
    start.subscribe(tempusDominus.Namespace.events.error, (e) => {
        if (e.oldDate) {
            var oldDateFormatted = moment(e.oldDate).format('YYYY-MM-DD HH:mm');
            start.dates.setFromInput(oldDateFormatted);
        } else {
            start.clear();
        }
    });
    end.subscribe(tempusDominus.Namespace.events.error, (e) => {
        if (e.oldDate) {
            var oldDateFormatted = moment(e.oldDate).format('YYYY-MM-DD HH:mm');
            end.dates.setFromInput(oldDateFormatted);
        } else {
            end.clear();
        }
    });
    start.subscribe(tempusDominus.Namespace.events.change, (e) => {
        if (e.date) {
            var newMinEndDate = moment(e.date);
            var newMinEndDateFormatted = newMinEndDate.format('YYYY-MM-DD HH:mm');
            if (end.dates.lastPicked && moment(end.dates.lastPicked) < newMinEndDate) {
                end.dates.setFromInput(newMinEndDateFormatted);
            }
            end.updateOptions({
                restrictions: { minDate: newMinEndDateFormatted },
                localization: { format: 'yyyy-MM-dd HH:mm' }
            });
        } else {
            end.updateOptions(dateConfiguration, true);
        }
    });
    end.subscribe(tempusDominus.Namespace.events.change, (e) => {
        if (e.date) {
            var newMaxStartDate = moment(e.date);
            var newMaxStartDateFormatted = newMaxStartDate.format('YYYY-MM-DD HH:mm');
            if (start.dates.lastPicked && moment(start.dates.lastPicked) > newMaxStartDate) {
                start.dates.setFromInput(newMaxStartDateFormatted);
            }
            start.updateOptions({
                restrictions: { maxDate: newMaxStartDateFormatted },
                localization: { format: 'yyyy-MM-dd HH:mm' }
            });
        } else {
            start.updateOptions(dateConfiguration, true);
        }
    });

    var start_value = $('#datetimepicker-start').data('value');
    var end_value = $('#datetimepicker-end').data('value');
    if (start_value) {
        start.dates.setFromInput(moment(start_value).format('YYYY-MM-DD HH:mm'));
    }
    if (end_value) {
        end.dates.setFromInput(moment(end_value).format('YYYY-MM-DD HH:mm'));
    }
    $('#observationsTable td')
        .click(function(e){
            var tag = e.target.nodeName;
            if(e.target.getAttribute('href')==='#'){
                // обход модалки
            }
            else if(tag=='INPUT'){
                // обход чекбокса на странице наблюдений
            }
            else if(tag === 'A'){
                window.location = $(this).data('href'); // переход по ссылке в ячейке на страницу запланировавшего наблюдения или страницу станции
            }
            else {
                if (e.ctrlKey) {
                    window.open($(this).parent().data('href'), '_blank'); // при нажатии с ctrl наблюдение откроется в новой вкладке
                    e. preventDefault();
                }
                else{
                    window.location = $(this).parent().data('href'); //переход на страницу наблюдение
                }
            }
        });

    $('.checkbox-group input').click(function () {
        var input = $(this);

        if (input.prop('checked')) {
            input.parent().addClass('inactive');
        } else {
            input.parent().removeClass('inactive');
        }
    });

    $('.submit-vet-filter').click(() => {
        window.location = $('#observationsFilteringForm').data('url_query');
    });


    $('#select-col-all').click(select_future_all);
    $('.future-obs-show').click(show_future_obs);
    $('.future-obs-delete').click(delete_future_obs);

    view_buttons();
});

function select_future_all() {
    var select_btn = $('#select-col-all');
    var all_btns = $('.select-column input');

    if (select_btn.prop('checked')) {
        all_btns.prop('checked', true);
    }
    else {
        all_btns.prop('checked', false);
    }
}

function view_buttons() {
    var user_auth = ($('thead .select-columns').data('auth') === 'True');
    if (user_auth &&
        window.location.href.includes('good') &&
        window.location.href.includes('bad') &&
        window.location.href.includes('unknown') &&
        window.location.href.includes('failed') &&
        !window.location.href.includes('future=0')) {
        $('.submit-vet-filter').toggleClass('d-none');
        $('.future-obs-delete').toggleClass('d-none');
        $('.future-obs-show').toggleClass('d-none');
        $('.select-column').toggleClass('d-none');
        $('.select-columns').toggleClass('d-none');
    }
}

function show_future_obs() {
    var selected_obs = $('.select-column input:checkbox:checked');
    selected_obs.each(function (i, el) {
        window.open('/observations/' + $(el).data('obsid'), '_blank');
    });
}

function delete_future_obs() {
    var selected_obs = $('.select-column input:checkbox:checked');

    var obs_id = [];
    selected_obs.each(function (i, el) {
        obs_id.push($(el).data('obsid'));
    });
    var data = { 'id': obs_id };
    $.ajax({
        type: 'POST',
        url: '/observations/delete_several/',
        data: data,
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
        }
    }).done(function (data) {
        var id;
        var success = data['success'].split(',');
        for (var i of success) {
            id = parseInt(i);
            var r = $('tr[data-obs="' + id + '"] input');
            r.prop('checked', false);
            $('tr[data-obs="' + id + '"]').hide();
        }
        var mes = '';
        if (data['success']) {
            mes = gettext('Наблюдения успешно удалены: ') + data['success'];
            create_alert('success', mes);
        }
        if (data['bad']) {
            mes = gettext('\nВы не можете удалить эти наблюдения: ') + data['bad'];
            create_alert('error', mes);
        }
    });

}

function create_alert(type, msg) {
    var alert_container = $('#alert-messages');
    if (!alert_container.find('.row').length) {
        alert_container.append('<div class="row"></div>');
    }
    $('#alert-messages .row').append(
        `<div class="alert alert-${type} alert-dismissible" role="alert">
          ${msg}
          <button type="button" class="btn btm-sm btn-without-border-red" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" class="bi bi-x"></span>
          </button>
        </div>`);
}
