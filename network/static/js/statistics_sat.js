/* global moment tempusDominus,gettext */

$(document).ready(function () {
    'use strict';
    var locale=document.documentElement.lang;
    // eslint-disable-next-line new-cap
    $('#sats').DataTable({
        // the dom field controls the layout and visibility of datatable items
        // and is not intuitive at all. Without layout we have dom: 'Bftrilp'
        // https://datatables.net/reference/option/dom
        dom: '<"row actions"Bp>' +
            '<"responsive-table"tr>' +
            '<"row under-table"ilp>',
        buttons: [{
            extend: 'colvis',
            columnText: function (dt, idx, title) {
                if (idx == 12 && title === '') {
                    return gettext('Страна');
                }
                return title;
            }
        }],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: 'column'
            }
        },
        columnDefs: [
            {
                className: 'control',
                orderable: false,
                targets: 0
            },
        ],
        language: {
            info: `${gettext('Показаны от')}  _START_  ${gettext('до')}  _END_  ${gettext('из')}  _TOTAL_  ${gettext('элементов')}`,
            lengthMenu:`${gettext('Показать')} _MENU_ ${gettext('элементов')}`,
            search: gettext('Фильтр:'),
            buttons: {
                colvis: gettext('Столбцы'),
            },
            paginate: {
                'first': gettext('Первая'),
                'previous': '«',
                'next': '»',
                'last': gettext('Последняя')
            }
        },
        order: [3, 'desc'],
        pageLength: 25
    });

    $('#sats_wrapper button').click(function () {
        var list = $('#sats_wrapper .dropdown-menu a');
        var item;
        var name;
        for (item of list) {
            if (item.text) {
                name = 'Stat_sat_' + item.text.replace(' ', '_');
                if (item.classList.contains('dt-button-active')) {
                    document.cookie = name + '=true';
                }
                else {
                    document.cookie = name + '=false';
                }
            }
            else {
                item.style.display = 'none';
            }
        }
        $('.dropdown-menu a').click(function () {
            var el = $(this)[0];
            if (el.text) {
                if (el.classList.contains('dt-button-active')) {
                    document.cookie = 'Stat_sat_' + el.text.replace(' ', '_') + '=true';
                }
                else {
                    document.cookie = 'Stat_sat_' + el.text.replace(' ', '_') + '=false';
                }
            }
        });
    });

    $('.buttons-colvis').removeClass('btn-secondary').addClass('btn-orange btn-small');

    $('.status-selector').click(function () {
        var input = $(this).find('input');

        $(this).addClass('active');
        $(this).siblings().removeClass('active').find('input').val('0');
        input.val('1');
    });

    for (var el of $('.with-procent')) {
        if (el.classList.contains('counted')){
            continue;
        }
        var total_data = parseInt(el.dataset.full);
        var data = parseInt(el.innerText);
        if (total_data != 0) {
            el.innerHTML += ` (${(100 / total_data * data).toFixed(2)}%)`;
        }
        el.className += ' counted';
    }

    tempusDominus.extend(window.tempusDominus.plugins.customDateFormat);
    var dateConfiguration = {
        useCurrent: false,
        display: {
            icons: {
                type: 'icons',
                time: 'bi bi-clock',
                date: 'bi bi-calendar3',
                up: 'bi bi-arrow-up',
                down: 'bi bi-arrow-down',
                previous: 'bi bi-chevron-left',
                next: 'bi bi-chevron-right',
                today: 'bi bi-calendar-check',
                clear: 'bi bi-trash',
                close: 'bi bi-x-lg'
            },
            sideBySide: true,
            components: {
                useTwentyfourHour: true
            },
            buttons: {
                close: true
            }
        },
        localization: {
            startOfTheWeek: 1,
            format: 'yyyy-MM-dd HH:mm',
            hourCycle: 'h23',
            locale: locale
        }
    };

    $('body').on('click', function(){
        for (var el of $('.with-procent')) {
            if (el.classList.contains('counted')){
                continue;
            }
            var total_data = parseInt(el.dataset.full);
            var data = parseInt(el.innerText);
            if (total_data != 0) {
                el.innerHTML += ` (${(100 / total_data * data).toFixed(2)}%)`;
            }
            el.className += ' counted';
        }
    });

    var start = new tempusDominus.TempusDominus(document.getElementById('datetimepicker-start'), dateConfiguration);
    var end = new tempusDominus.TempusDominus(document.getElementById('datetimepicker-end'), dateConfiguration);

    const otherValidFormats = [
        'YYYY-MM-DD H:mm', 'YYYY-MM-DD HH', 'YYYY-MM-DD H', 'YYYY-MM-DD HH:m',
        'YYYY-MM-DD H:m', 'YYYY-MM-D HH:mm', 'YYYY-MM-D H:mm', 'YYYY-MM-D HH',
        'YYYY-MM-D H', 'YYYY-MM-D HH:m', 'YYYY-MM-D H:m', 'YYYY-M-D HH:mm', 'YYYY-M-D H:mm',
        'YYYY-M-D HH', 'YYYY-M-D H', 'YYYY-M-D HH:m', 'YYYY-M-D H:m', 'YYYY-M-DD HH:mm',
        'YYYY-M-DD H:mm', 'YYYY-M-DD HH', 'YYYY-M-DD H', 'YYYY-M-DD HH:m', 'YYYY-M-DD H:m'
    ];
    $('#datetimepicker-start input').on('blur', function () {
        if (!moment(this.value, 'YYYY-MM-DD HH:mm', true).isValid()) {
            var date = moment(this.value, otherValidFormats, true);
            if (date.isValid()) {
                var newDate = date.format('YYYY-MM-DD HH:mm');
                start.dates.setFromInput(newDate);
            } else {
                start.clear();
            }
        }
    });
    $('#datetimepicker-end input').on('blur', function () {
        if (!moment(this.value, 'YYYY-MM-DD HH:mm', true).isValid()) {
            var date = moment(this.value, otherValidFormats, true);
            if (date.isValid()) {
                var newDate = date.format('YYYY-MM-DD HH:mm');
                end.dates.setFromInput(newDate);
            } else {
                end.clear();
            }
        }
    });
    start.subscribe(tempusDominus.Namespace.events.error, (e) => {
        if (e.oldDate) {
            var oldDateFormatted = moment(e.oldDate).format('YYYY-MM-DD HH:mm');
            start.dates.setFromInput(oldDateFormatted);
        } else {
            start.clear();
        }
    });
    end.subscribe(tempusDominus.Namespace.events.error, (e) => {
        if (e.oldDate) {
            var oldDateFormatted = moment(e.oldDate).format('YYYY-MM-DD HH:mm');
            end.dates.setFromInput(oldDateFormatted);
        } else {
            end.clear();
        }
    });
    start.subscribe(tempusDominus.Namespace.events.change, (e) => {
        if (e.date) {
            var newMinEndDate = moment(e.date);
            var newMinEndDateFormatted = newMinEndDate.format('YYYY-MM-DD HH:mm');
            if (end.dates.lastPicked && moment(end.dates.lastPicked) < newMinEndDate) {
                end.dates.setFromInput(newMinEndDateFormatted);
            }
            end.updateOptions({
                restrictions: { minDate: newMinEndDateFormatted },
                localization: { format: 'yyyy-MM-dd HH:mm' }
            });
        } else {
            end.updateOptions(dateConfiguration, true);
        }
    });
    end.subscribe(tempusDominus.Namespace.events.change, (e) => {
        if (e.date) {
            var newMaxStartDate = moment(e.date);
            var newMaxStartDateFormatted = newMaxStartDate.format('YYYY-MM-DD HH:mm');
            if (start.dates.lastPicked && moment(start.dates.lastPicked) > newMaxStartDate) {
                start.dates.setFromInput(newMaxStartDateFormatted);
            }
            start.updateOptions({
                restrictions: { maxDate: newMaxStartDateFormatted },
                localization: { format: 'yyyy-MM-dd HH:mm' }
            });
        } else {
            start.updateOptions(dateConfiguration, true);
        }
    });

    var start_value = $('#datetimepicker-start').data('value');
    var end_value = $('#datetimepicker-end').data('value');
    if (start_value) {
        start.dates.setFromInput(moment(start_value).format('YYYY-MM-DD HH:mm'));
    }
    if (end_value) {
        end.dates.setFromInput(moment(end_value).format('YYYY-MM-DD HH:mm'));
    }


    function date_to_format_string(date) {
        var month = date.getMonth().toString();
        var day = date.getDate().toString();
        var hours = date.getHours().toString();
        var minutes = date.getMinutes().toString();
        if (month.length == 1) {
            month = '0' + month;
        }
        if (day.length == 1) {
            day = '0' + day;
        }
        if (hours.length == 1) {
            hours = '0' + hours;
        }
        if (minutes.length == 1) {
            minutes = '0' + minutes;
        }

        return date.getFullYear() + '-' + month + '-' +
            day + ' ' + hours + ':' + minutes;
    }

    $('#data-period-selection').on('changed.bs.select', function () {
        var period = $(this).val();
        var start_time_input = $('#datetimepicker-start input');
        var end_time_input = $('#datetimepicker-end input');
        var now = new Date();
        now.setMonth(now.getMonth() + 1);
        if (period == 'a') {
            start_time_input.val('');
            end_time_input.val('');
        }
        else if (period == 'h') {
            end_time_input.val(date_to_format_string(now));
            now.setHours(now.getHours() - 1);
            start_time_input.val(date_to_format_string(now));
        }
        else if (period == 'd') {
            end_time_input.val(date_to_format_string(now));
            now.setDate(now.getDate() - 1);
            start_time_input.val(date_to_format_string(now));
        }
        else if (period == 'w') {
            end_time_input.val(date_to_format_string(now));
            now.setDate(now.getDate() - 7);
            start_time_input.val(date_to_format_string(now));
        }
        else if (period == 'm') {
            end_time_input.val(date_to_format_string(now));
            now.setMonth(now.getMonth() - 1);
            start_time_input.val(date_to_format_string(now));
        }
        else if (period == 'y') {
            end_time_input.val(date_to_format_string(now));
            now.setFullYear(now.getFullYear() - 1);
            start_time_input.val(date_to_format_string(now));
        }
    });
});
