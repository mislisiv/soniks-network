"""Define functions and settings for the django admin base interface"""

from django.contrib import admin, messages
from django.db import transaction
from django.shortcuts import redirect
from django.urls import re_path, reverse

from network.base.models import (
    Antenna,
    AntennaType,
    DemodData,
    ExportedFrameset,
    FrequencyRange,
    LatestTleSet,
    Launch,
    Mode,
    Network,
    NetworkSchedule,
    Observation,
    Operator,
    Satellite,
    SatelliteLaunch,
    Station,
    StationSchedule,
    StationScheduleSec,
    StationStat,
    StationStatusLog,
    Telemetry,
    Tle,
    Transmitter,
    TransmitterStats,
)
from network.base.tasks import decode_all_data, decode_all_nondecoded_data
from network.base.utils import export_as_csv, export_station_status

# Admin inline models


class DemodDataInline(admin.TabularInline):
    """
    Define DemodData inline template for use in Observation view in django admin UI
    """

    model = DemodData
    exclude = [
        "data_id",
        "lat",
        "lng",
        "version",
        "satellite",
        "station",
        "transmitter",
    ]


class SatelliteLauchInline(admin.TabularInline):
    """
    Define SatelliteLauch inline template for use in Launch view in django admin UI
    """

    model = SatelliteLaunch


# Admin panels


@admin.register(FrequencyRange)
class FrequenyRangeAdmin(admin.ModelAdmin):
    """
    Define Frequency Range view in django admin UI
    """

    list_display = (
        "id",
        "min_frequency",
        "max_frequency",
        "antenna",
        "antenna_type",
        "station",
    )

    def antenna_type(self, obj):  # pylint: disable=no-self-use
        """Return the antenna type that use this frequency range"""
        return obj.antenna.antenna_type

    def station(self, obj):  # pylint: disable=no-self-use
        """Return the antenna station that use this frequency range"""
        return str(obj.antenna.station.id) + " - " + obj.antenna.station.name

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "antenna":
            kwargs["queryset"] = Antenna.objects.order_by("station_id")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(AntennaType)
class AntennaTypeAdmin(admin.ModelAdmin):
    """
    Define Antenna Type view in django admin UI
    """

    list_display = ("id", "__str__", "antenna_count", "antenna_list", "station_list")

    def antenna_count(self, obj):  # pylint: disable=no-self-use
        """Return the number of antennas use this antenna type"""
        return obj.antennas.all().count()

    def antenna_list(self, obj):  # pylint: disable=no-self-use
        """Return antennas that use the antenna type"""
        return ",\n".join([str(s.id) for s in obj.antennas.all().order_by("id")])

    def station_list(self, obj):  # pylint: disable=no-self-use
        """Return antennas that use the antenna type"""
        return ",\n".join(
            [str(s.station.id) for s in obj.antennas.all().order_by("id")]
        )


@admin.register(Antenna)
class AntennaAdmin(admin.ModelAdmin):
    """
    Define Antenna Type view in django admin UI
    """

    list_display = ("id", "antenna_type", "station", "ranges_list")
    list_filter = ("antenna_type", "station")
    show_full_result_count = False

    def ranges_list(self, obj):  # pylint: disable=no-self-use
        """
        Return frequeny ranges for this antenna
        """
        return ",\n".join(
            [
                str(s.min_frequency) + " - " + str(s.max_frequency)
                for s in obj.frequency_ranges.all()
            ]
        )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "station":
            kwargs["queryset"] = Station.objects.order_by("id")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("frequency_ranges")


@admin.register(Station)
class StationAdmin(admin.ModelAdmin):
    """
    Define Station view in django admin UI
    """

    list_display = (
        "id",
        "name",
        "owner",
        "get_email",
        "lat",
        "lng",
        "qthlocator",
        "client_version",
        "created_date",
        "state",
        "target_utilization",
        "violator_scheduling",
        "client_id",
    )
    list_filter = ("status", "created", "client_version", "violator_scheduling")
    search_fields = ("id", "name", "owner__username")

    actions = [export_as_csv, export_station_status]
    export_as_csv.short_description = "Export selected as CSV"
    export_station_status.short_description = "Export selected status"

    def created_date(self, obj):  # pylint: disable=no-self-use
        """
        Return when the station was created
        """
        return obj.created.strftime("%d.%m.%Y, %H:%M")

    def get_email(self, obj):  # pylint: disable=no-self-use
        """
        Return station owner email address
        """
        if obj.owner:
            return obj.owner.email
        return None

    get_email.admin_order_field = "email"
    get_email.short_description = "Owner Email"

    def get_actions(self, request):
        """
        Return the list of actions for station admin view
        """
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("owner")


@admin.register(StationStat)
class StationStatAdmin(admin.ModelAdmin):
    """
    Define StationStat view in django admin UI
    """

    list_display = ("id", "station")


@admin.register(StationStatusLog)
class StationStatusLogAdmin(admin.ModelAdmin):
    """
    Define StationStatusLog view in django admin UI
    """

    list_display = ("id", "station", "status", "changed")
    list_filter = ("station", "status")
    search_fields = ("id", "station__id")

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("station")


@admin.register(Satellite)
class SatelliteAdmin(admin.ModelAdmin):
    """
    Define Satellite view in django admin UI
    """

    list_display = (
        "id",
        "sat_id",
        "name",
        "norad_cat_id",
        "call_sign",
        "status",
        "is_frequency_violator",
        "approved",
    )
    list_filter = ("status", "is_frequency_violator")
    readonly_fields = ("name", "names")
    search_fields = ("name", "norad_cat_id", "call_sign")

    def sat_id(self, obj):  # pylint: disable=R0201
        """
        Return the Satellite Identifier for that satellite
        """
        return obj.sat_id

    def get_urls(self):
        """
        Returns django urls for the Satellite view
        """
        urls = super().get_urls()
        my_urls = [
            re_path(
                r"^decode_all_data/(?P<sat_id>[A-Z]{4,4}(?:-\d\d\d\d){4,4})/$",
                self.decode_all_data,
                name="decode_all_data",
            ),
            re_path(
                r"^decode_all_nondecoded_data/(?P<sat_id>[A-Z]{4,4}(?:-\d\d\d\d){4,4})/$",
                self.decode_all_nondecoded_data,
                name="decode_all_nondecoded_data",
            ),
        ]
        return my_urls + urls

    def decode_all_data(self, request, sat_id):  # pylint: disable=R0201
        """
        Returns the admin home page, while triggering a Celery decode task
        Forces a decode of all data for a Satellite Identifier. This could be very resource
        intensive but necessary when catching a satellite up with a new decoder

        Args:
            sat_id(str): Sat_id of satellite which should be full decoded

        """
        # Allow decoding data only for Satellites that are not merged and
        # suggest user trigger decoding for the associated_satellite which will
        # include all DemodData of the satellites that are associated with it
        decode_all_data.delay(sat_id)
        messages.success(
            request, f"Полное декодирование пакетов для спутника {sat_id} запущено!"
        )
        return redirect(reverse("admin:index"))

    def decode_all_nondecoded_data(self, request, sat_id):  # pylint: disable=R0201
        """
        Returns the admin home page, while triggering a Celery decode task
        Forces a decode of non decoded data for a Satellite Identifier
        """
        decode_all_nondecoded_data.delay(sat_id)
        messages.success(
            request,
            f"Декодирование нерасшифрованных пакетов пакетов для спутника {sat_id} запущено!",
        )
        return redirect(reverse("admin:index"))

    def get_actions(self, request):
        actions = super().get_actions(request)
        return actions


@admin.register(Observation)
class ObservationAdmin(admin.ModelAdmin):
    """
    Define Observation view in django admin UI
    """

    show_full_result_count = False
    list_display = (
        "id",
        "author",
        "satellite",
        "transmitter",
        "start",
        "end",
        "archived",
        "audio_zipped",
        "status",
        "payload",
        "waterfall",
    )
    list_filter = (
        "start",
        "end",
        "archived",
        "audio_zipped",
        "status",
        "satellite",
        "author",
    )
    search_fields = (
        "id",
        "satellite__name",
        "satellite__names",
        "satellite__norad_cat_id",
        "author__username",
    )
    inlines = [
        DemodDataInline,
    ]
    raw_id_fields = ["transmitter", "satellite", "tle"]

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .prefetch_related(
                "author",
                "ground_station",
                "satellite",
                "transmitter",
            )
        )


@admin.register(DemodData)
class DemodDataAdmin(admin.ModelAdmin):
    """
    Define DemodData view in django admin UI
    """

    show_full_result_count = False
    list_display = ("id", "observation", "demodulated_data", "is_image")
    list_filter = ("is_image", "is_decoded")
    search_fields = ("id", "observation__id", "satellite__name")
    raw_id_fields = ["satellite", "observation"]

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .prefetch_related("satellite", "transmitter", "observation", "station")
        )


@admin.register(Transmitter)
class TransmitterAdmin(admin.ModelAdmin):
    """
    Define Transmitter view in django admin UI
    """

    list_display = (
        "uuid",
        "description",
        "type",
        "downlink_mode",
        "baud",
        "status",
        "satellite",
    )
    list_filter = ("type", "downlink_mode", "unknown")
    search_fields = ("uuid", "description", "satellite__name")

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .prefetch_related(
                "satellite",
                "downlink_mode",
            )
        )


@admin.register(TransmitterStats)
class TransmitterStatsAdmin(admin.ModelAdmin):
    """
    Define TransmitterStats view in django admin UI
    """

    list_display = ("id", "transmitter")
    search_fields = ("transmitter__uuid", "transmitter__satellite__name")


@admin.register(Tle)
class TleAdmin(admin.ModelAdmin):
    """
    Define Tle view in django admin UI
    """

    list_display = ("id", "tle_source", "satellite", "updated")
    search_fields = ("id", "satellite__name")

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("satellite")


@admin.register(LatestTleSet)
class LatestTleSetAdmin(admin.ModelAdmin):
    """
    Define LatestTleSet view in django admin UI
    """

    list_display = ("id", "satellite", "latest", "last_modified")
    search_fields = ("id", "satellite__name", "satellite__norad_cat_id")
    raw_id_fields = ["satellite", "latest", "latest_distributable"]

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("latest")


@admin.register(StationSchedule)
class StationScheduleAdmin(admin.ModelAdmin):
    """
    Define StationSchedule view in django admin UI
    """

    list_display = ("id", "station", "active")


@admin.register(StationScheduleSec)
class StationScheduleSecAdmin(admin.ModelAdmin):
    """
    Define StationScheduleSec view in django admin UI
    """

    list_display = ("id", "station", "active")


@admin.register(NetworkSchedule)
class NetworkScheduleAdmin(admin.ModelAdmin):
    """
    Define NetworkSchedule view in django admin UI
    """

    list_display = ("id", "active")


@admin.register(Operator)
class OperatorAdmin(admin.ModelAdmin):
    """Define Operator view in django admin UI"""

    list_display = ("id", "name", "description")
    list_filter = ("name", "names")
    search_fields = ("name", "names")


@admin.register(Network)
class NetworkAdmin(admin.ModelAdmin):
    """Define Network view in django admin UI"""

    list_display = ("id", "name", "description")
    list_filter = ("name", "description")
    search_fields = ("name", "description")


@admin.register(Mode)
class ModeAdmin(admin.ModelAdmin):
    """Define Mode view in django admin UI"""

    list_display = ("id", "name")
    list_filter = ("id", "name")
    search_fields = ("id", "name")


@admin.register(Telemetry)
class TelemetryAdmin(admin.ModelAdmin):
    """Define Telemetry view in django admin UI"""

    list_display = ("satellite", "name", "decoder")
    list_filter = ("satellite", "name")
    search_fields = ("satellite__name", "name")

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("satellite")


@admin.register(ExportedFrameset)
class FramesetAdmin(admin.ModelAdmin):
    list_display = ("id", "satellite", "start", "end")
    list_filter = ("satellite",)
    search_fields = ("satellite__name",)

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("satellite")


@admin.register(Launch)
class LaunchAdmin(admin.ModelAdmin):
    list_display = ("launch_name", "id", "launch_date")
    list_filter = ("launch_name",)
    search_fields = ("launch_name",)
    inlines = [
        SatelliteLauchInline,
    ]
    show_full_result_count = False

    def get_queryset(self, request):
        return super().get_queryset(request)


@admin.register(SatelliteLaunch)
class SatelliteLaunchAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "sat_id",
        "norad",
        "frequency",
        "sat_status",
        "launch",
    )
    list_filted = ("name", "norad", "launch")
    search_fields = ("name", "norad", "launch")
