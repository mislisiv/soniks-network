"""Django database base model for SONIKS Network"""

import codecs
import re
from datetime import datetime, timedelta
from os import path

import satnogsdecoders
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.files.storage import DefaultStorage
from django.core.validators import (
    MaxLengthValidator,
    MaxValueValidator,
    MinLengthValidator,
    MinValueValidator,
    URLValidator,
)
from django.db import models
from django.db.models import Count, Q
from django.urls import reverse
from django.utils.html import format_html
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField
from nanoid import generate
from shortuuidfield import ShortUUIDField
from storages.backends.s3boto3 import S3Boto3Storage

from network.base.managers import ObservationManager
from network.base.utils import bands_from_range
from network.users.models import User

OBSERVATION_STATUSES = (
    ("unknown", "Unknown"),
    ("good", "Good"),
    ("bad", "Bad"),
    ("failed", "Failed"),
)
STATION_STATUSES = (
    (2, "Online"),
    (1, "Testing"),
    (0, "Offline"),
)
STATION_VIOLATOR_SCHEDULING_CHOICES = (
    (0, _("Никто")),
    (1, _("Только операторы")),
    (2, _("Все")),
)

TRANSLATED_ANTENNAS_TYPE_NAME = {
    "Dipole": _("Диполь"),
    "V-Dipole": _("V-Дипольная"),
    "Discone": _("Дисконная"),
    "Ground Plane": _("Базовая"),
    "Yagi": _("Яги"),
    "Cross Yagi": _("Кросс-яги"),
    "Helical": _("Спиральная"),
    "Parabolic": _("Параболическая"),
    "Vertical": _("Вертикальная"),
    "Turnstile": _("Турникетная"),
    "Quadrafilar": _("Квадрифилярная"),
    "Eggbeater": _("Взбивалка"),
    "Lindenblad": _("Линденблад"),
    "Parasitic Lindenblad": _("Паразитная линденблад"),
    "Patch": _("Патч"),
    "Other Directional": _("Другая направленная"),
    "Other Omni-Directional": _("Другая всенаправленная"),
}

SATELLITE_STATUS = ["alive", "dead", "future", "re-entered"]
TRANSMITTER_STATUS = ["active", "inactive", "invalid"]
TRANSMITTER_TYPE = ["Transmitter", "Transceiver", "Transponder"]

URL_REGEX = (
    r"(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$"
)

MIN_FREQ = 0
MAX_FREQ = 40000000000
MIN_FREQ_MSG = "Убедитесь, что это значение больше или равно 0 Гц"
MAX_FREQ_MSG = "Убедитесь, что это значение меньше или равно 40 ГГц."
IARU_COORDINATION_STATUS = [
    "IARU Coordinated",
    "IARU Declined",
    "IARU Uncoordinated",
    "N/A",
]

ROCKET_TYPES = [
    ("Союз-2.1а", _("Союз-2.1а")),
    ("Союз-2.1б", _("Союз-2.1б")),
    ("Союз-2.1в", _("Союз-2.1в")),
    ("Ангара-А5", _("Ангара-А5")),
    ("Чанчжен-5", _("Чанчжен-5")),
    ("Чанчжен-6", _("Чанчжен-6")),
    ("Чанчжен-7", _("Чанчжен-7")),
    ("Nuri", _("Nuri")),
    ("Vega", _("Vega")),
    ("SSLV", _("SSLV")),
    ("Firefly Alpha", _("Firefly Alpha")),
    ("Falcon Heavy", _("Falcon Heavy")),
    ("Falcon 9", _("Falcon 9")),
    ("Неизвестный", _("Неизвестный")),
]

BOOST_TYPES = [
    ("Фрегат-М", _("Фрегат-М")),
    ("Фрегат-МТ", _("Фрегат-МТ")),
    ("Фрегат-СБ", _("Фрегат-СБ")),
    ("ДМ-03", _("ДМ-03")),
    ("Бриз-КМ", _("Бриз-КМ")),
    ("Бриз-М", _("Бриз-М")),
]

SPACE_PORTS = [
    ("Канаверал", _("Канаверал")),
    ("Байконур", _("Байконур")),
    ("Восточный", _("Восточный")),
    ("Вэньчан", _("Вэньчан")),
    ("Плесецк ", _("Плесецк ")),
    ("Цзюцюань", _("Цзюцюань")),
    ("Куру", _("Куру")),
    ("База Вандерберг", _("База Вандерберг")),
]


MISSION_TYPES = [
    ("Авиационная", _("Авиационная")),
    ("Любительская", _("Любительская")),
    ("Транслирующая", _("Транслирующая")),
    ("Изучение Земли", _("Изучение Земли")),
    ("Фиксированная", _("Фиксированная")),
    ("Межспутниковая", _("Межспутниковая")),
    ("Морская", _("Морская")),
    ("Метеорологическая", _("Метеорологическая")),
    ("Мобильная", _("Мобильная")),
    ("Радиолокационная", _("Радиолокационная")),
    ("Радионавигационная", _("Радионавигационная")),
    ("Космические операции", _("Космические операции")),
    ("Космические исследования", _("Космические исследования")),
    ("Технологическая", _("Технологическая")),
    ("Биологическая", _("Биологическая")),
    ("Стандарты частоты", _("Стандарты частоты")),
    ("Неизвестная", _("Неизвестная")),
]

SATELLITELAUNCH_STATUS = [
    ("Активный", _("Активный")),
    ("Неизвестный", _("Неизвестный")),
    ("Неактивный", _("Неактивный")),
    ("Будущий", _("Будущий")),
    ("Сошел с орбиты", _("Сошел с орбиты")),
]

# custom fields


class ModifiedArrayField(ArrayField):
    def formfield(self, **kwargs):
        """
        Return a form field for the ArrayField with multiple choice options.

        Overrides the default formfield method to provide a MultipleChoiceField
        with a CheckboxSelectMultiple widget. The choices are derived from the
        base field of the ArrayField.

        Args:
            **kwargs: Additional keyword arguments to customize the form field.

        Returns:
            Field: A form field configured for multiple choice selection.
        """
        defaults = {
            "form_class": forms.MultipleChoiceField,
            "choices": self.base_field.choices,
            "widget": forms.CheckboxSelectMultiple,
            **kwargs,
        }
        return super(ArrayField, self).formfield(**defaults)


# Model functions


def _decode_pretty_hex(binary_data: bytes):
    """
    Return the binary data as hex dump of the following form: `DE AD C0 DE`

    Args:
        binary_data(bytes): Binary data from demoddata file

    Returns:
        str: Hex dump of the following form
    """

    data = codecs.encode(binary_data, "hex").decode("ascii").upper()
    return " ".join(data[i : i + 2] for i in range(0, len(data), 2))


def _name_observation_data(instance, filename):
    """
    Return a filepath formatted by Observation ID\\
    for payload and waterfall

    Returns:
        str: Filepath to files
    """
    return "data_obs/{0}/{1}/{2}/{3}/{4}/{5}".format(
        instance.start.year,
        instance.start.month,
        instance.start.day,
        instance.start.hour,
        instance.id,
        filename,
    )


def _name_observation_demoddata(instance, filename: str):
    """
    Return a filepath for DemodData formatted by Observation ID\\
    for demodulated_data and payload_json

    Args:
        filename(str): Name of DemodData file

    Returns:
        str: Filepath to demoddata
    """
    # On change of the string bellow, change it also at api/views.py
    if instance.observation is not None:
        return "data_obs/{0}/{1}/{2}/{3}/{4}/{5}".format(
            instance.observation.start.year,
            instance.observation.start.month,
            instance.observation.start.day,
            instance.observation.start.hour,
            instance.observation.id,
            filename,
        )
    return "data_obs/{0}/{1}/{2}/{3}/{4}/{5}".format(
        datetime.now().year,
        datetime.now().month,
        datetime.now().day,
        datetime.now().hour,
        0,
        filename,
    )


def _select_audio_storage():
    return S3Boto3Storage() if settings.USE_S3_STORAGE_FOR_AUDIO else DefaultStorage()


def _select_waterfall_storage():
    return (
        S3Boto3Storage() if settings.USE_S3_STORAGE_FOR_WATERFALL else DefaultStorage()
    )


def _select_data_storage():
    return S3Boto3Storage() if settings.USE_S3_STORAGE_FOR_DATA else DefaultStorage()


def _name_exported_frames(instance, filename: str):  # pylint: disable=W0613
    """
    Returns path for a exported frames file

    Args:
    filename(str): Name of exported file

    Returns:
        str: Path to exported frameset file
    """
    return path.join("download/", filename)


def validate_image(fieldfile_obj):
    """
    Validates image size for station image

    Args:
    fieldfile_obj(FileField): File from Django Filefield
    """
    filesize = fieldfile_obj.file.size
    megabyte_limit = 2.0
    if filesize > megabyte_limit * 1024 * 1024:
        raise ValidationError("Max file size is %sMB" % str(megabyte_limit))


def validate_sat_id(value: str):
    """
    Validate a Satellite Identifier for Satellite

    Args:
        value(str): Sat id for validating
    """
    if not re.compile(r"^[A-Z]{4,4}(?:-\d\d\d\d){4,4}$").match(value):
        raise ValidationError(
            '%(value)s недействительный идентификатор спутника. \
             Идентификатор спутника должен иметь \
             формат "CCCC-NNNN-NNNN-NNNN-NNNN" где C - это {A-Z} и N - это {0-9}',
            params={"value": value},
        )


def generate_sat_id():
    """
    Generate a Satellite Identifier

    Returns:
        str: Sat id string formatted by
            ``{0}-{1}-{2}-{3}-{4}``
    """
    numeric = "0123456789"
    uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    first_segment = generate(uppercase, 4)
    second_segment = generate(numeric, 4)
    third_segment = generate(numeric, 4)
    fourth_segment = generate(numeric, 4)
    fifth_segment = generate(numeric, 4)
    return "{0}-{1}-{2}-{3}-{4}".format(
        first_segment, second_segment, third_segment, fourth_segment, fifth_segment
    )


def generate_norad_id():
    """
    Generate a Satellite NoradID

    Returns:
        int: Generated satellite NoradID
    """
    satellites = Satellite.objects.filter(
        Q(norad_cat_id__gt=70000) & Q(norad_cat_id__lt=100000)
    ).order_by("-norad_cat_id")
    norad = satellites[0].norad_cat_id
    for satellite in satellites:
        if norad != satellite.norad_cat_id:
            return norad
        norad -= 1
    return satellites[0].norad_cat_id + 1


def get_default_itu_notification_field():
    """
    Generate default value for itu_notification field of TransmitterEntry model
    """
    return {"urls": []}


def get_default_station_configuration_schema():
    """
    Generate default value for schema field of StationConfigurationSchema model
    """
    return {}


def get_default_station_configuration():
    """
    Generate default value for schema field of StationConfiguration model
    """
    return {}


# DB models


class Station(models.Model):
    """
    Model for SONIKS ground stations
    """

    owner = models.ForeignKey(
        User,
        related_name="ground_stations",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    name = models.CharField(max_length=45)
    image = models.ImageField(
        upload_to="ground_stations", blank=True, validators=[validate_image]
    )
    qthlocator = models.CharField(max_length=8, blank=True)
    client_version = models.CharField(max_length=45, blank=True)
    client_id = models.CharField(max_length=128, blank=True)
    satnogs_soapy_rx_device = models.CharField(max_length=40, blank=True)
    satnogs_antenna = models.CharField(max_length=40, blank=True)

    status = models.IntegerField(choices=STATION_STATUSES, default=0)
    violator_scheduling = models.IntegerField(
        choices=STATION_VIOLATOR_SCHEDULING_CHOICES, default=0
    )
    target_utilization = models.IntegerField(
        validators=[MaxValueValidator(100), MinValueValidator(0)],
        help_text="Целевой коэффициент нагрузки для вашей станции",
        null=True,
        blank=True,
    )
    horizon = models.PositiveIntegerField(help_text="В градусах выше 0", default=10)
    satnogs_rx_samp_rate = models.PositiveIntegerField(null=True, blank=True)

    alt = models.PositiveIntegerField(
        null=True, blank=True, help_text="В метрах над уровнем моря"
    )
    lat = models.FloatField(
        null=True,
        blank=True,
        validators=[MaxValueValidator(90), MinValueValidator(-90)],
        help_text="напр. 38.01697",
    )
    lng = models.FloatField(
        null=True,
        blank=True,
        validators=[MaxValueValidator(180), MinValueValidator(-180)],
        help_text="напр. 23.7314",
    )
    featured_date = models.DateField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    last_seen = models.DateTimeField(null=True, blank=True)
    description = models.TextField(
        max_length=500, blank=True, help_text="Макс. 500 символов"
    )
    satnogs_rf_gain = models.FloatField(null=True, blank=True)
    testing = models.BooleanField(default=True)

    class Meta:
        ordering = ["-status"]
        indexes = [
            models.Index(fields=["-status", "id"]),
            models.Index(fields=["client_id"]),
        ]

    @property
    def is_online(self):
        """
        Return true if station is online
        """
        try:
            heartbeat = self.last_seen + timedelta(
                minutes=int(settings.STATION_HEARTBEAT_TIME)
            )
            return heartbeat > now()
        except TypeError:
            return False

    @property
    def is_offline(self):
        """
        Return true if station is offline
        """
        return not self.is_online

    @property
    def has_location(self):
        """
        Return true if station location is defined
        """
        if self.alt is None or self.lat is None or self.lng is None:
            return False
        return True

    @property
    def is_testing(self):
        """
        Return true if station is online and in testing mode
        """
        if self.is_online:
            if self.status == 1:
                return True
        return False

    @property
    def success_rate(self):
        """
        Return the success rate of the station - successful observation over failed ones
        """
        rate = cache.get("station-{0}-rate".format(self.id))
        if not rate:
            observations = self.observations.exclude(testing=True).exclude(
                end__gt=now()
            )
            stats = observations.aggregate(
                bad=Count("pk", filter=Q(status__range=(-100, -1))),
                good=Count("pk", filter=Q(status__gte=100)),
                unknown=Count("pk", filter=Q(status__range=(0, 99))),
            )
            good_count = 0 if stats["good"] is None else stats["good"]
            bad_count = 0 if stats["bad"] is None else stats["bad"]
            unknown_count = 0 if stats["unknown"] is None else stats["unknown"]
            total = good_count + bad_count + unknown_count
            if total != 0:
                rate = {
                    "good": round(100 / total * good_count, 2),
                    "unknown": round(100 / total * unknown_count, 2),
                    "bad": 100
                    - (
                        round(100 / total * good_count)
                        + round(100 / total * unknown_count)
                    ),
                    "total": total,
                }
                cache.set("station-{0}-rate".format(self.id), rate, 60 * 60 * 6)
            else:
                rate = {
                    "good": good_count,
                    "unknown": bad_count,
                    "bad": unknown_count,
                    "total": total,
                }
        return rate

    @property
    def status_sort(self):
        """
        Returns the status of the station taking into account "never been online"
        """
        if self.last_seen is None:
            return -1
        return self.status

    def __str__(self):
        if self.pk:
            return "%d - %s" % (self.pk, self.name)
        return "%s" % (self.name)

    def get_image(self):
        """
        Return the image of the station or the default image if there is a defined one
        """
        if self.image and hasattr(self.image, "url"):
            return self.image.url
        return settings.STATION_DEFAULT_IMAGE

    def state(self):
        """
        Return the station status in html format
        """
        if not self.status:
            return format_html('<span style="color:red;">' + _("Не в сети") + "</span>")
        if self.status == 1:
            return format_html(
                '<span style="color:orange;">' + _("Тестируется") + "</span>"
            )
        return format_html('<span style="color:green">' + _("В сети") + "</span>")

    def update_status(self, created: bool = False):
        """
        Update the status of the station

        Args:
            created(bool): Whether the model is being created
        """
        if not created:
            current_status = self.status
            if self.is_offline:
                self.status = 0
            elif self.testing:
                self.status = 1
            else:
                self.status = 2
            self.save()
            if self.status != current_status:
                StationStatusLog.objects.create(station=self, status=self.status)
        else:
            StationStatusLog.objects.create(station=self, status=self.status)


class StationStat(models.Model):
    station = models.OneToOneField(
        Station, related_name="stat", on_delete=models.CASCADE
    )
    all_alt_svg = models.FileField(
        upload_to="station_stat_images", null=True, blank=True
    )
    all_az_svg = models.FileField(
        upload_to="station_stat_images", null=True, blank=True
    )
    low_az_svg = models.FileField(
        upload_to="station_stat_images", null=True, blank=True
    )
    month_polar_svg = models.FileField(
        upload_to="station_stat_images", null=True, blank=True
    )

    class Meta:
        indexes = [models.Index(fields=["station"])]


class AntennaType(models.Model):
    """
    Model for antenna types
    """

    name = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return self.name

    def get_translated_name(self):
        return TRANSLATED_ANTENNAS_TYPE_NAME[self.name]


class Antenna(models.Model):
    """
    Model for antennas of SONIKS ground stations
    """

    antenna_type = models.ForeignKey(
        AntennaType, on_delete=models.PROTECT, related_name="antennas"
    )
    station = models.ForeignKey(
        Station, on_delete=models.CASCADE, related_name="antennas"
    )

    @property
    def bands(self):
        """
        Return comma separated string of the bands that the antenna works on
        """
        bands = []
        for frequency_range in self.frequency_ranges.all():
            for band in bands_from_range(
                frequency_range.min_frequency, frequency_range.max_frequency
            ):
                if band not in bands:
                    bands.append(band)
        return ", ".join(bands)

    def __str__(self):
        if self.pk:
            return "%d - %s (#%s)" % (self.pk, self.antenna_type.name, self.station.id)
        if self.station.id:
            return "%s (#%s)" % (self.antenna_type.name, self.station.id)
        return "%s" % (self.antenna_type.name)


class FrequencyRange(models.Model):
    """
    Model for frequency ranges of antennas
    """

    antenna = models.ForeignKey(
        Antenna, on_delete=models.CASCADE, related_name="frequency_ranges"
    )
    min_frequency = models.BigIntegerField()
    max_frequency = models.BigIntegerField()

    @property
    def bands(self):
        """
        Return comma separated string of the bands that of the frequeny range
        """
        bands = bands_from_range(self.min_frequency, self.max_frequency)
        return ", ".join(bands)

    class Meta:
        ordering = ["min_frequency"]

    def clean(self):
        if self.max_frequency < self.min_frequency:
            raise ValidationError(
                {
                    "min_frequency": (
                        "Minimum frequency is greater than the maximum one ({0} > {1}).".format(
                            self.min_frequency, self.max_frequency
                        )
                    ),
                    "max_frequency": (
                        "Maximum frequency is less than the minimum one ({0} < {1}).".format(
                            self.max_frequency, self.min_frequency
                        )
                    ),
                }
            )
        if self.min_frequency < settings.MIN_FREQUENCY_FOR_RANGE:
            raise ValidationError(
                {
                    "min_frequency": (
                        "Minimum frequency should be more than {0}."
                    ).format(settings.MIN_FREQUENCY_FOR_RANGE)
                }
            )
        if self.max_frequency > settings.MAX_FREQUENCY_FOR_RANGE:
            raise ValidationError(
                {
                    "max_frequency": (
                        "Maximum frequency should be less than {0}."
                    ).format(settings.MAX_FREQUENCY_FOR_RANGE)
                }
            )


class StationStatusLog(models.Model):
    """
    Model for keeping Status log for Station
    """

    station = models.ForeignKey(
        Station,
        related_name="station_logs",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    status = models.IntegerField(choices=STATION_STATUSES, default=0)
    changed = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-changed"]
        indexes = [models.Index(fields=["-changed"])]

    def __str__(self):
        return "{0} - {1}".format(self.station, self.status)

    def get_status_for_logs(self):
        """Display status for station log

        Created because we cannot translate status directly because of html specialties
        """
        if self.status == 0:
            return _("Не в сети")
        elif self.status == 1:
            return _("Тестируется")
        else:
            return _(" В сети")


class Operator(models.Model):
    """
    Satellite Owner/Operator
    """

    name = models.CharField(max_length=255, unique=True)
    names = models.TextField(blank=True)
    description = models.TextField(blank=True)
    website = models.URLField(
        blank=True,
        validators=[URLValidator(schemes=["http", "https"], regex=URL_REGEX)],
    )

    def __str__(self):
        return self.name


class Network(models.Model):
    """
    Model for Satellite Networks
    """

    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True)
    website = models.URLField(
        blank=True,
        validators=[URLValidator(schemes=["http", "https"], regex=URL_REGEX)],
    )

    def __str__(self):
        return self.name


class Launch(models.Model):
    """
    Model for satellite launches
    """

    launch_name = models.CharField(max_length=255)
    rocket_type = models.CharField(
        max_length=255, blank=True, null=True, choices=ROCKET_TYPES
    )
    boost_type = models.CharField(
        max_length=255, blank=True, null=True, choices=BOOST_TYPES
    )
    space_port = models.CharField(max_length=255, blank=True, choices=SPACE_PORTS)
    mission_description = models.TextField(blank=True)
    launch_date = models.DateTimeField(blank=True, null=True)
    image = models.ImageField(upload_to="launch_images/", blank=True, null=True)
    country = CountryField(blank=True, multiple=True, blank_label="(select countries)")
    status = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = "Launch"
        verbose_name_plural = "Launches"

    def __str__(self):
        """
        Returns a string representation of the Launch object.

        Returns:
            str: A string representation of the Launch object.
        """
        return self.launch_name + self.launch_date.strftime(" (%Y)")


class SatelliteLaunch(models.Model):
    """
    Model for satellite launches
    """

    # Foreign keys
    launch = models.ForeignKey(
        Launch, blank=True, on_delete=models.CASCADE, related_name="launch_satellites"
    )
    network = models.ForeignKey(
        Network,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="non_db_satellite_network",
    )
    # Char fields
    name = models.CharField(max_length=255, blank=True)
    sat_id = models.CharField(max_length=24, validators=[validate_sat_id], blank=True)
    deployer = models.CharField(max_length=255, blank=True, null=True, default=None)
    mission_type = ModifiedArrayField(
        models.TextField(max_length=255, blank=True, null=True, choices=MISSION_TYPES),
        blank=True,
        null=True,
        default=None,
    )
    sat_status = models.CharField(
        max_length=255, choices=SATELLITELAUNCH_STATUS, blank=True, null=True
    )
    call_sign = models.CharField(max_length=10, blank=True, null=True, default=None)
    # Integer fields
    norad = models.PositiveIntegerField(blank=True, null=True)

    # Array fields
    frequency = ArrayField(models.FloatField(), blank=True, null=True)
    # Boolean fields
    deploy_directly = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Satellite in Launch"
        verbose_name_plural = "Satellites in Launches"

    def __str__(self):
        """
        Returns a string representation of the SatelliteLaunch object.

        Returns:
            str: A string representation of the SatelliteLaunch object.
        """
        return self.name

    def get_mission_type_display(self):
        missions = self.mission_type
        missions_types = dict(MISSION_TYPES)
        return [missions_types[mis_type] for mis_type in missions]


class Satellite(models.Model):
    """
    Model for SONIKS satellites
    """

    # Foreign keys
    launch = models.ForeignKey(
        Launch,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
        related_name="db_satellites",
    )
    operator = models.ForeignKey(
        Operator,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="satellite_operator",
    )
    network = models.ForeignKey(
        Network,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="satellite_network",
    )
    created_by = models.ForeignKey(
        User,
        related_name="created_satellites",
        null=True,
        on_delete=models.SET_NULL,
        default=None,
    )

    # Char fields
    name = models.CharField(max_length=45)
    sat_id = models.CharField(
        default=generate_sat_id,
        unique=True,
        max_length=24,
        validators=[validate_sat_id],
    )
    sat_id_by_changes = models.CharField(blank=True, max_length=24, null=True)
    names = models.TextField(blank=True)
    call_sign = models.CharField(max_length=10, blank=True, null=True, default=None)
    citation = models.CharField(
        max_length=512,
        default="",
        help_text="Источник (желательно ссылка) для этого добавления или изменения",
        blank=False,
    )
    dashboard_url = models.URLField(
        blank=True,
        null=True,
        max_length=200,
        validators=[URLValidator(schemes=["http", "https"], regex=URL_REGEX)],
    )
    website = models.URLField(
        blank=True,
        validators=[URLValidator(schemes=["http", "https"], regex=URL_REGEX)],
    )

    deployer = models.CharField(max_length=255, blank=True, null=True, default=None)

    # Integer fields
    norad_cat_id = models.PositiveIntegerField(auto_created=True, blank=True, null=True)
    norad_follow_id = models.PositiveIntegerField(blank=True, null=True, default=None)
    norad_id_by_changes = models.PositiveIntegerField(
        db_index=True, blank=True, null=True
    )

    # Float fields
    altitude = models.FloatField(null=True, blank=True)
    apogee = models.FloatField(null=True, blank=True)
    perigee = models.FloatField(null=True, blank=True)

    # Boolean fields
    approved = models.BooleanField(default=False)
    changes = models.BooleanField(default=False)
    unknown = models.BooleanField(default=False)
    is_frequency_violator = models.BooleanField(default=False)
    deploy_directly = models.BooleanField(default=True)
    # Date and time fields
    launched = models.DateTimeField(null=True, blank=True, default=None)
    deployed = models.DateTimeField(null=True, blank=True, default=None)
    decayed = models.DateTimeField(null=True, blank=True, default=None)
    decay_date = models.DateField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    # Other fields
    image = models.ImageField(
        upload_to="satellites", blank=True, help_text="Ideally: 250x250", default=None
    )
    countries = CountryField(
        blank=True, multiple=True, blank_label="(select countries)"
    )
    status = models.CharField(
        choices=list(zip(SATELLITE_STATUS, SATELLITE_STATUS)),
        max_length=10,
        default="alive",
    )
    trans_decode = models.JSONField(
        default=None,
        null=True,
        blank=True,
        help_text="JSON format,like {'multiplier':{'pid':0.5},'translation':{'ru':{'pid':'пид',}}}",
    )

    class Meta:
        ordering = ["norad_cat_id"]
        indexes = [
            models.Index(fields=["norad_cat_id"]),
            models.Index(fields=["sat_id"]),
        ]

    def get_image(self):
        """
        Return the satellite image or the default if doesn't exist one
        """
        if self.image:
            return self.image
        return settings.SATELLITE_DEFAULT_IMAGE

    @property
    def countries_str(self):
        """
        Returns countries for this Satellite in comma seperated string format

        Returns:
            str: countries for this Satellite in comma seperated string format
        """
        return ",".join(map(str, self.countries))

    @property
    def is_active(self):
        """
        Returns a boolean value of satellite activity
        depending on the status of the transmitters

        Returns:
            bool: satellite is active or not
        """
        if self.transmitter_entries.filter(status="active").count():
            return True
        return False

    @property
    def demoddata_count(self):
        """
        Returns the number of received data files for the satellite

        Returns:
            int: demoddata count
        """
        return self.telemetry_data.count()

    def __str__(self):
        return self.name

    def get_mission_type_display(self):
        missions = self.mission_type
        missions_types = dict(MISSION_TYPES)
        return [missions_types[mis_type] for mis_type in missions]


class Telemetry(models.Model):
    """
    Model for satellite telemetry decoders
    """

    satellite = models.ForeignKey(
        Satellite, null=True, related_name="telemetries", on_delete=models.SET_NULL
    )
    name = models.CharField(max_length=45)
    decoder = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name_plural = "Telemetries"

    def __str__(self):
        return self.name

    def get_kaitai_fields(self):
        """
        Return an empty-value dict of fields for this kaitai.io struct\\
        Beware the overuse of "decoder" in satnogsdecoders and "decoder" the\\
        field above in this Telemetry model

        Returns:
            dict: dict with kaitai_fields
        """
        results = {}
        try:
            decoder_class = getattr(satnogsdecoders.decoder, self.decoder.capitalize())
            results = satnogsdecoders.decoder.get_fields(decoder_class, empty=True)
        except AttributeError:
            pass
        return results


class Mode(models.Model):
    """
    A satellite transmitter RF mode. For example: FM
    """

    name = models.CharField(max_length=25, unique=True)

    class CacheOptions:  # pylint: disable=C0115,R0903
        default_cache_key = "mode"
        cache_purge_keys = ["mode"]

    def __str__(self):
        return self.name


class Transmitter(models.Model):
    """
    Model for SONIKS transmitters
    """

    # FK fields
    satellite = models.ForeignKey(
        Satellite,
        null=True,
        related_name="transmitter_entries",
        on_delete=models.SET_NULL,
        default=None,
    )
    downlink_mode = models.ForeignKey(
        Mode,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="transmitter_downlink_mode",
        help_text="Режим модуляции для нисходящей линии связи",
    )
    uplink_mode = models.ForeignKey(
        Mode,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="transmitter_uplink_mode",
        help_text="Режим модуляции для восходящей линии связи",
    )

    # char fields
    description = models.TextField(default="")
    type = models.CharField(
        choices=list(zip(TRANSMITTER_TYPE, TRANSMITTER_TYPE)),
        max_length=20,
        default="Transmitter",
    )
    status = models.CharField(
        choices=list(zip(TRANSMITTER_STATUS, TRANSMITTER_STATUS)),
        max_length=16,
        default="active",
        help_text="Функциональное состояние этого передатчика",
    )
    uuid_by_changes = models.CharField(
        blank=True,
        default="",
        max_length=30,
    )
    citation = models.CharField(
        max_length=512,
        default="",
        help_text="Источник (желательно ссылка) для этого добавления или изменения",
        blank=False,
    )
    iaru_coordination = models.CharField(
        choices=list(zip(IARU_COORDINATION_STATUS, IARU_COORDINATION_STATUS)),
        max_length=25,
        default="N/A",
        help_text="Статус координации частоты с IARU",
    )
    iaru_coordination_url = models.URLField(
        blank=True,
        help_text="Ссылка для подробностей по координации этой частоты",
        validators=[URLValidator(schemes=["http", "https"], regex=URL_REGEX)],
    )

    # int fields
    uplink_low = models.BigIntegerField(
        blank=True,
        null=True,
        validators=[
            MinValueValidator(MIN_FREQ, message=MIN_FREQ_MSG),
            MaxValueValidator(MAX_FREQ, message=MAX_FREQ_MSG),
        ],
        help_text="Частота (в Гц) для восходящей линии связи или нижняя граница диапазона \
                   восходящей линии связи для транспондера",
    )
    uplink_high = models.BigIntegerField(
        blank=True,
        null=True,
        validators=[
            MinValueValidator(MIN_FREQ, message=MIN_FREQ_MSG),
            MaxValueValidator(MAX_FREQ, message=MAX_FREQ_MSG),
        ],
        help_text="Частота (в Гц) верхней границы диапазона восходящей линии связи \
                   для транспондера",
    )
    uplink_drift = models.IntegerField(
        blank=True,
        null=True,
        validators=[MinValueValidator(-99999), MaxValueValidator(99999)],
        help_text="Отклонение приемника от опубликованной частоты восходящей линии связи в \
                   частях на миллиард (PPB)",
    )
    downlink_low = models.BigIntegerField(
        blank=True,
        null=True,
        validators=[
            MinValueValidator(MIN_FREQ, message=MIN_FREQ_MSG),
            MaxValueValidator(MAX_FREQ, message=MAX_FREQ_MSG),
        ],
        help_text="Частота (в Гц) для нисходящей линии связи или нижняя часть диапазона нисходящей \
                   линии связи для транспондера",
    )
    downlink_high = models.BigIntegerField(
        blank=True,
        null=True,
        validators=[
            MinValueValidator(MIN_FREQ, message=MIN_FREQ_MSG),
            MaxValueValidator(MAX_FREQ, message=MAX_FREQ_MSG),
        ],
        help_text="Частота (в Гц) верхней границы диапазона нисходящей линии связи \
                   для транспондера",
    )
    downlink_drift = models.IntegerField(
        blank=True,
        null=True,
        validators=[MinValueValidator(-99999), MaxValueValidator(99999)],
        help_text="Отклонение передатчика от опубликованной частоты нисходящей линии связи в \
                   частях на миллиард (PPB)",
    )
    baud = models.BigIntegerField(
        validators=[MinValueValidator(0)],
        blank=True,
        null=True,
        help_text="Количество модулированных символов, которые передатчик \
                   отправляет каждую секунду",
    )

    # Other fields
    uuid = ShortUUIDField()
    itu_notification = models.JSONField(default=get_default_itu_notification_field)
    invert = models.BooleanField(
        default=False, help_text="Да, если это инвертированный транспондер"
    )
    unconfirmed = models.BooleanField(default=False, null=True)
    approved = models.BooleanField(default=False)
    unknown = models.BooleanField(default=False)

    class Meta:
        ordering = ["uuid"]
        indexes = [models.Index(fields=["uuid"])]

    def __str__(self):
        return str(self.uuid)

    def create_stat_for_transmitter(self):
        """
        Creating TransmitterStats if they do not exist
        Needed for normal data fetching from satnogs
        """
        if not TransmitterStats.objects.filter(transmitter=self).exists():
            TransmitterStats.objects.create(transmitter=self)


class TransmitterStats(models.Model):
    """
    Model for transmitter statistics
    """

    # FK fields
    transmitter = models.OneToOneField(
        Transmitter, related_name="stat", on_delete=models.CASCADE
    )
    # int fields
    total_count = models.IntegerField(default=0)
    unknown_count = models.IntegerField(default=0)
    good_count = models.IntegerField(default=0)
    bad_count = models.IntegerField(default=0)
    failed_count = models.IntegerField(default=0)
    # other fields
    transmitter_uuid = ShortUUIDField(auto=False)

    class Meta:
        indexes = [
            models.Index(fields=["transmitter"]),
            models.Index(fields=["transmitter_uuid"]),
        ]


class Tle(models.Model):
    """
    Model for TLEs
    """

    # Foreign Keys
    satellite = models.ForeignKey(
        Satellite,
        null=True,
        blank=True,
        related_name="tle_sets",
        on_delete=models.SET_NULL,
    )

    # Text fields
    tle0 = models.CharField(
        max_length=69,
        blank=True,
        validators=[MinLengthValidator(1), MaxLengthValidator(69)],
    )
    tle1 = models.CharField(
        max_length=69,
        blank=True,
        validators=[MinLengthValidator(69), MaxLengthValidator(69)],
    )
    tle2 = models.CharField(
        max_length=69,
        blank=True,
        validators=[MinLengthValidator(69), MaxLengthValidator(69)],
    )
    tle_source = models.CharField(max_length=300, blank=True)
    url = models.URLField(max_length=200, blank=True, null=True)

    # other fields
    updated = models.DateTimeField(auto_now=True, blank=True)

    class Meta:
        ordering = ["-updated"]
        indexes = [
            models.Index(fields=["-updated"]),
        ]
        permissions = [("access_all_tles", "Access all TLEs")]

    def __str__(self):
        return "{:d} - {:s}".format(self.id, self.tle0)

    @property
    def str_array(self):
        """Return TLE in string array format"""
        # tle fields are unicode, pyephem and others expect python strings
        return [str(self.tle0), str(self.tle1), str(self.tle2)]


class LatestTleSet(models.Model):
    """
    LatestTleSet holds the latest entry of a Satellite Tle Set
    """

    # fk fields
    satellite = models.OneToOneField(
        Satellite, related_name="latest_tle_set", on_delete=models.CASCADE
    )
    latest = models.ForeignKey(
        Tle, null=True, related_name="latest", on_delete=models.SET_NULL
    )
    latest_distributable = models.ForeignKey(
        Tle, null=True, related_name="latest_distributable", on_delete=models.SET_NULL
    )
    # other fields
    last_modified = models.DateTimeField(auto_now=True)

    class CacheOptions:  # pylint: disable=C0115,R0903
        cache_purge_keys = ["latesttleset_with_perms", "latesttleset_without_perms"]


class Observation(models.Model):
    """
    Model for SONIKS observations
    """

    # fk fields
    satellite = models.ForeignKey(
        Satellite,
        related_name="observations",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    transmitter = models.ForeignKey(
        Transmitter,
        related_name="observations",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    tle = models.ForeignKey(
        Tle,
        related_name="observations",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    author = models.ForeignKey(
        User,
        related_name="observations",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    ground_station = models.ForeignKey(
        Station,
        related_name="observations",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    waterfall_status_user = models.ForeignKey(
        User,
        related_name="waterfalls_vetted",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    # char fields
    client_version = models.CharField(max_length=255, blank=True)
    client_metadata = models.TextField(blank=True)
    archive_identifier = models.CharField(max_length=255, blank=True)
    vetted_status = models.CharField(
        choices=OBSERVATION_STATUSES, max_length=20, default="unknown"
    )
    """
    Meaning of values:\\
    x < -100      -> Failed\\
    -100 =< x < 0 -> Bad\\
    0 =< x < 100  -> Unknown (Future if observation not completed)\\
    100 =< x      -> Good
    """

    archive_url = models.URLField(blank=True, null=True)

    # int fields
    center_frequency = models.BigIntegerField(blank=True, null=True)
    transmitter_frequency_low = models.BigIntegerField(
        blank=True, null=True, default=None
    )
    transmitter_frequency_high = models.BigIntegerField(
        blank=True, null=True, default=None
    )
    transmitter_frequency_drift = models.BigIntegerField(
        blank=True, null=True, default=None
    )
    rise_azimuth = models.FloatField(blank=True, null=True)
    max_altitude = models.FloatField(blank=True, null=True)
    set_azimuth = models.FloatField(blank=True, null=True)
    status = models.SmallIntegerField(default=0)

    # other fields
    start = models.DateTimeField(db_index=True)
    end = models.DateTimeField(db_index=True)
    testing = models.BooleanField(default=False)
    is_auto = models.BooleanField(default=False)
    audio_zipped = models.BooleanField(default=False)
    archived = models.BooleanField(default=False)
    waterfall_status = models.BooleanField(blank=True, null=True, default=None)
    """
    Meaning of values:\\
    True -> Waterfall has signal of the observed satellite (with-signal)\\
    False -> Waterfall has not signal of the observed satellite (without-signal)\\
    None -> Uknown whether waterfall has or hasn't signal of the observed satellite (unknown)
    """

    waterfall_status_datetime = models.DateTimeField(null=True, blank=True)

    payload = models.FileField(
        upload_to=_name_observation_data, storage=_select_audio_storage, blank=True
    )
    waterfall = models.ImageField(
        upload_to=_name_observation_data, storage=_select_waterfall_storage, blank=True
    )

    objects = ObservationManager.as_manager()

    @property
    def is_past(self):
        """
        Return true if observation is in the past (end time is in the past)
        """
        return self.end < now()

    @property
    def is_future(self):
        """
        Return true if observation is in the future (end time is in the future)
        """
        return self.end > now()

    @property
    def is_started(self):
        """
        Return true if observation has started (start time is in the past)
        """
        return self.start < now()

    # The values bellow are used as returned values in the API and for css rules in templates
    @property
    def status_badge(self):
        """
        Return badge for status field
        """
        if self.is_future:
            return "future"
        if self.status < -100:
            return "failed"
        if -100 <= self.status < 0:
            return "bad"
        if 0 <= self.status < 100:
            return "unknown"
        return "good"

    # The values bellow are used as displayed values in templates
    @property
    def status_display(self):
        """
        Return display name for status field
        """
        if self.is_future:
            return "Future"
        if self.status < -100:
            return "Failed"
        if -100 <= self.status < 0:
            return "Bad"
        if 0 <= self.status < 100:
            return "Unknown"
        return "Good"

    # The values bellow are used as returned values in the API and for css rules in templates
    @property
    def waterfall_status_badge(self):
        """
        Return badge for waterfall_status field
        """
        if self.waterfall_status is None:
            return "unknown"
        if self.waterfall_status:
            return "with-signal"
        return "without-signal"

    # The values bellow are used as displayed values in templates
    @property
    def waterfall_status_display(self):
        """
        Return display name for waterfall_status field
        """
        if self.waterfall_status is None:
            return "Unknown"
        if self.waterfall_status:
            return "With Signal"
        return "Without Signal"

    @property
    def has_waterfall(self):
        """
        Run some checks on the waterfall for existence of data
        """
        if self.waterfall:
            return True
        return False

    @property
    def has_audio(self):
        """
        Run some checks on the payload for existence of data
        """
        if self.archive_url:
            return True
        if self.payload:
            return True
        return False

    @property
    def audio_url(self):
        """
        Return url for observation's audio file
        """
        if self.has_audio:
            if self.archive_url:
                return self.archive_url
            return self.payload.url
        return ""

    class Meta:
        ordering = ["-start", "-end"]
        indexes = [models.Index(fields=["-start", "-end"])]
        permissions = (("can_vet", "Can vet observations"),)

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        """
        Return absolute url of the model object
        """
        return reverse("base:observation_view", kwargs={"observation_id": self.id})


class DemodData(models.Model):
    """
    Model for DemodData
    """

    # fk fields
    observation = models.ForeignKey(
        Observation,
        related_name="demoddata",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    satellite = models.ForeignKey(
        Satellite, null=True, related_name="telemetry_data", on_delete=models.SET_NULL
    )
    transmitter = models.ForeignKey(
        Transmitter, null=True, blank=True, on_delete=models.SET_NULL
    )
    station = models.ForeignKey(
        Station, null=True, related_name="data", on_delete=models.SET_NULL
    )

    # char fields
    observer = models.CharField(max_length=60, blank=True)
    version = models.CharField(max_length=45, blank=True)

    # int fields
    lng = models.FloatField(
        validators=[MaxValueValidator(180), MinValueValidator(-180)], default=0
    )
    lat = models.FloatField(
        validators=[MaxValueValidator(90), MinValueValidator(-90)], default=0
    )
    data_id = models.PositiveIntegerField(
        blank=True, null=True
    )  # ноль использований в коде, зачем оно вообще....

    # other fields
    is_image = models.BooleanField(default=False)
    sonik_data = models.BooleanField(default=True)
    is_decoded = models.BooleanField(default=False, db_index=True)
    sent_to_satnogs = models.BooleanField(default=False)
    used_in_gallery = models.BooleanField(default=False)
    gallery_cover = models.BooleanField(default=False)
    timestamp = models.DateTimeField(null=True, db_index=True)
    demodulated_data = models.FileField(
        upload_to=_name_observation_demoddata, storage=_select_data_storage, blank=True
    )
    payload_json = models.FileField(
        upload_to=_name_observation_demoddata, storage=_select_data_storage, blank=True
    )

    class Meta:
        indexes = [models.Index(fields=["is_image"])]

    def display_payload_hex(self):
        """
        Return the content of the data file as hex dump of the following form: `DE AD C0 DE`
        """

        if self.demodulated_data:
            with self.demodulated_data.storage.open(
                self.demodulated_data.name, mode="rb"
            ) as data_file:
                payload = data_file.read()
        return _decode_pretty_hex(payload)

    def __str__(self):
        if self.satellite:
            return "data-for-{0}".format(self.satellite.sat_id)
        return self.demodulated_data.name


class ExportedFrameset(models.Model):
    """
    Model for exported frames
    """

    satellite = models.ForeignKey(Satellite, null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(get_user_model(), null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True)
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    exported_file = models.FileField(
        upload_to=_name_exported_frames, blank=True, null=True
    )


class StationSchedule(models.Model):
    """
    Station schedule model
    """

    station = models.OneToOneField(
        Station, related_name="schedule", on_delete=models.CASCADE
    )
    active = models.BooleanField(default=True)
    satellites = models.JSONField(null=True, blank=True)
    param = models.JSONField(null=True, blank=True)


class StationScheduleSec(models.Model):
    """
    Station schedule model for non-primary schedule
    """

    station = models.OneToOneField(
        Station, related_name="schedulesec", on_delete=models.CASCADE
    )
    active = models.BooleanField(default=True)
    satellites = models.JSONField(null=True, blank=True)
    param = models.JSONField(null=True, blank=True)


class NetworkSchedule(models.Model):
    """
    Singleton network schedule model
    """

    id = models.BigAutoField(primary_key=True)
    active = models.BooleanField(default=True)
    satellites = models.JSONField(null=True, blank=True)
    param = models.JSONField(null=True, blank=True)
