from django.db import migrations


def initialize_antenna_types(apps, schema_editor):
    AntennaType = apps.get_model("base", "AntennaType")
    AntennaType.objects.bulk_create(
        [
            AntennaType(name="Dipole"),
            AntennaType(name="V-Dipole"),
            AntennaType(name="Discone"),
            AntennaType(name="Ground Plane"),
            AntennaType(name="Yagi"),
            AntennaType(name="Cross Yagi"),
            AntennaType(name="Helical"),
            AntennaType(name="Parabolic"),
            AntennaType(name="Vertical"),
            AntennaType(name="Turnstile"),
            AntennaType(name="Quadrafilar"),
            AntennaType(name="Eggbeater"),
            AntennaType(name="Lindenblad"),
            AntennaType(name="Parasitic Lindenblad"),
            AntennaType(name="Patch"),
            AntennaType(name="Other Directional"),
            AntennaType(name="Other Omni-Directional"),
        ]
    )


def revert_initialize_antenna_types(apps, schema_editor):
    AntennaType = apps.get_model("base", "AntennaType")
    AntennaType.objects.filter(
        name__in=[
            "Dipole", "V-Dipole", "Discone", "Ground Plane", "Yagi", "Cross Yagi", "Helical",
            "Parabolic", "Vertical", "Turnstile", "Quadrafilar", "Eggbeater", "Lindenblad",
            "Parasitic Lindenblad", "Patch", "Other Directional", "Other Omni-Directional"
        ]
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0003_create_operators_group'),
    ]

    operations = [
        migrations.RunPython(initialize_antenna_types, revert_initialize_antenna_types),
    ]
