from django.db import migrations


def forwards(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    Group.objects.create(name='Moderators')


def backwards(apps, schema_editor):
    """Delete Peers group."""
    Group = apps.get_model('auth', 'Group')
    Group.objects.filter(name='Moderators').delete()


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
