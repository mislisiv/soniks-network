from datetime import datetime, timedelta
from typing import Any, Optional, Union

from django.conf import settings
from django.db.models import Q
from skyfield.api import EarthSatellite, load, wgs84
from skyfield.timelib import Time
from skyfield.toposlib import GeographicPosition
from skyfield.vectorlib import VectorSum

from network.base.models import (
    FrequencyRange,
    Observation,
    Satellite,
    Station,
    Transmitter,
)
from network.users.models import User


class SatelliteStats:
    """
    Class for representing statistics of planned observations for a satellite
    """

    def __init__(self, norad: int):
        """
        Args:
            norad (int): Satellite NORAD ID
        """
        self.norad = norad
        self.observation_count = 0
        self.good_obs = 0
        self.is_violator = False
        self.violator_obs = None


class SatelliteWindow:
    """
    Class for representing a specific flyby of a single satellite
    """

    def __init__(
        self,
        satellite: EarthSatellite,
        difference_dist: VectorSum,
        satellite_stats: SatelliteStats,
        transmitter: Transmitter,
    ):
        """
        Args:
            satellite (EarthSatellite): Earth satellite object Skyfield
            difference_dist (VectorSum): Position of the satellite relative to the observer
            satellite_stats (SatelliteStats): Satellite statistics for planned observations
            transmitter (Transmitter): Transmitter of the satellite from which observation is planned
        """
        self.sky_satellite = satellite
        self.difference_dist = difference_dist
        self.satellite_priority = satellite_stats
        self.transmitter = transmitter
        self.norad_id = 0
        self.sat_id = None
        self.start_time = None
        self.end_time = None
        self.culminate_times = []
        self.scheduled = False
        self.next_item = None

    def get_altaz(self, time: Time):
        """
        Method for calculating the position of a satellite relative to an observer at a given time

        Args:
            time (Time): Time to calculate coordinates

        Returns:
            Altitude, Azimuth, Distance
        """
        topocentric = self.difference_dist.at(time)
        return topocentric.altaz()

    def get_culminate_time(self, start_time: datetime, end_time: datetime):
        """
        Method for checking whether a satellite has a maximum elevation in a given time interval

        Args:
            start_time (datetime): Start of period
            end_time (datetime): End of period

        Returns:
            (Time): Found time of culmination or None
        """
        for culminate_time in self.culminate_times:
            if start_time <= culminate_time.utc_datetime() <= end_time:
                return culminate_time
        return None


class StationWindows:
    """
    Class for representing a satellite flyby window.
    Satellites flying close to each other with a slight deviation
    from each other are combined into one window - StationWindows
    to simplify the planning of communication sessions
    """

    def __init__(self, station: GeographicPosition):
        """
        Args:
            station (GeographicPosition): Observer object Skyfield
        """
        self.sky_station = station
        self.min_time = None
        self.max_time = None
        self.satellites = []

    def add_window(self, window_sat: SatelliteWindow):
        """
        Adds a satellite to the list of satellites in the current window,
        preserving the relationship between the list elements.

        Args:
            window_sat (SatelliteWindow): Item to add to list
        """
        if len(self.satellites) == 0:
            window_sat.next_item = window_sat
        else:
            next_satellite = self.satellites[-1].next_item
            self.satellites[-1].next_item = window_sat
            window_sat.next_item = next_satellite
        self.satellites.append(window_sat)

    def calculate_times(self):
        """
        Calculate the window time period based on the available satellite periods
        """
        for satellite in self.satellites:
            if satellite.scheduled:
                continue
            if self.min_time is None:
                self.min_time = satellite.start_time.utc_datetime()
            elif satellite.start_time.utc_datetime() < self.min_time:
                self.min_time = satellite.start_time.utc_datetime()
            if self.max_time is None:
                self.max_time = satellite.end_time.utc_datetime()
            elif satellite.end_time.utc_datetime() > self.max_time:
                self.max_time = satellite.end_time.utc_datetime()

    def get_next_satellite(
        self,
        start: datetime,
        end: datetime,
        current_sat: Optional[SatelliteWindow] = None,
        bad_elevation: Optional[bool] = False,
    ) -> Optional[SatelliteWindow]:
        """
        Selection of the next satellite from the current window for planning observations.
        The selection is based on the correspondence in time interval, elevation restrictions,
        and also taking into account the satellite priority.

        Args:
            start (datetime): Session start time
            end (datetime): Session end time
            current_sat (SatelliteWindow): Current satellite to start searching from
            bad_elevation (bool): Flag to indicate that the current satellite has bad elevation.
                If True, the current satellite will not be included in the search.

        Returns:
            (SatelliteWindow): Next satellite to plan observations
        """
        available_satellites = []  # Satellites available for planning
        satellites_with_culmination = []  # Satellites available with good elevation in the current window
        satellite_culmination_count_min = None
        satellite_count = len(self.satellites)
        save_current_sat = current_sat
        if current_sat is None:
            current_sat = self.satellites[0]
        else:
            current_sat = current_sat.next_item

        while satellite_count:
            if bad_elevation and current_sat == save_current_sat:
                return None

            if (start >= current_sat.start_time.utc_datetime()) and (
                end <= current_sat.end_time.utc_datetime()
            ):
                if not current_sat.scheduled and (
                    not current_sat.satellite_priority.is_violator
                    or current_sat.satellite_priority.violator_obs
                ):
                    available_satellites.append(current_sat)

                culminate_time = current_sat.get_culminate_time(start, end)
                if (
                    culminate_time is not None
                    and not current_sat.satellite_priority.is_violator
                ):
                    elevation = current_sat.get_altaz(culminate_time)[0].degrees
                    if elevation > settings.LAUNCH_GOOD_ELEVATION:
                        satellites_with_culmination.append(current_sat)
                        if (
                            satellite_culmination_count_min is None
                            or current_sat.satellite_priority.good_obs
                            > satellite_culmination_count_min.satellite_priority.good_obs
                        ):
                            satellite_culmination_count_min = current_sat

            current_sat = current_sat.next_item
            satellite_count -= 1

        if len(available_satellites) == 0:
            return None

        # Search for a satellite with a minimum number of good observations (by elevation)
        if len(satellites_with_culmination):
            prio_index, prio_count = (
                0,
                satellites_with_culmination[0].satellite_priority.good_obs,
            )
            for i in range(1, len(satellites_with_culmination)):
                if (
                    satellites_with_culmination[i].satellite_priority.good_obs
                    < prio_count
                ):
                    prio_index = i
                    prio_count = satellites_with_culmination[
                        prio_index
                    ].satellite_priority.observation_count
            return satellites_with_culmination[prio_index]

        # Search for a satellite with a minimum number of observations overall
        prio_index, prio_count = (
            0,
            available_satellites[0].satellite_priority.observation_count,
        )
        for i in range(0, len(available_satellites)):
            obs_count = available_satellites[i].satellite_priority.observation_count
            if (
                obs_count < prio_count
                and available_satellites[i] != satellite_culmination_count_min
            ):
                prio_index = i
                prio_count = available_satellites[
                    i
                ].satellite_priority.observation_count

        return available_satellites[prio_index]


def update_station_windows(
    observer_object: GeographicPosition,
    satellite_object: EarthSatellite,
    window_groups: list[StationWindows],
    satellite_stats: SatelliteStats,
    norad: int,
    sat_id: str,
    transmitter: Transmitter,
    times: tuple[Time],
    events: tuple[int],
    difference: VectorSum,
    elevation: int,
):
    """
    Function for generating observation windows at the station and update StationWindows objects

    Args:
        observer_object (GeographicPosition): Observer object Skyfield
        satellite_object (EarthSatellite): Earth satellite object Skyfield
        window_groups (list[StationWindows]): List of observation windows for the current station
        satellite_stats (SatelliteStats): Observation statistics for the current satellite
        norad (int): NORAD ID for the current satellite
        sat_id (str): Satellite ID for the current satellite
        transmitter (Transmitter): Current satellite transmitter for observation planning
        times (tuple[Time]): List of event times of the current satellite relative to the observer
        events (tuple[int]): Event list to identify an event of a specific time:

            * 0 — Satellite rose above ``altitude_degrees``.
            * 1 — Satellite culminated and started to descend again.
            * 2 — Satellite fell below ``altitude_degrees``.

        difference (VectorSum): Position of the satellite relative to the observer
        elevation (int): Minimum value of maximum satellite elevation for planning observations
    """
    window = SatelliteWindow(satellite_object, difference, satellite_stats, transmitter)
    for time, event in zip(times, events):
        if event == 0:
            window = SatelliteWindow(
                satellite_object, difference, satellite_stats, transmitter
            )
            window.norad_id = norad
            window.sat_id = sat_id
            window.start_time = time
        elif event == 1:
            altitude = window.get_altaz(time)[0]
            if altitude.degrees >= elevation:
                window.culminate_times.append(time)
        else:
            # Event check: window formation occurs only in case of full flight (sunrise -> culmination -> sunset)
            if window.start_time is None or len(window.culminate_times) == 0:
                continue
            window.end_time = time
            window_inserted = False
            if len(window_groups) > 0:
                # Finding an existing flight window where the current observation fits
                for group in window_groups:
                    if (
                        group.min_time - timedelta(minutes=5)
                        < window.start_time.utc_datetime()
                        < group.max_time
                    ):
                        group.add_window(window)
                        window_inserted = True
                        break
            # If a suitable window is not found, a new window is created.
            if len(window_groups) == 0 or not window_inserted:
                station_windows = StationWindows(observer_object)
                station_windows.add_window(window)
                station_windows.calculate_times()
                window_groups.append(station_windows)


def launch_scheduler(
    launch_id: int,
    start: datetime,
    end: datetime,
    violator_count: int,
    min_horizon: int,
    min_elevation: int,
    split_duration: int,
    sat_status: str,
) -> list[dict[str, Any]]:
    """
    The main function of calculating and planning observations for a given launch

    Args:
        launch_id (int): Launch ID
        start (datetime): Scheduling period start time
        end (datetime): Scheduling period end time
        violator_count (int): Number of observations allowed for violator satellites
        min_horizon (int): Minimum station horizon
        min_elevation (int): Minimum satellite elevation
        split_duration (int): Duration of observation
        sat_status (str): Satellite status for planning (None, alive or unknown)

    Returns:
        (list[dict[str, Any]]): List of dictionaries containing key parameters for creating Observation objects
    """
    ts = load.timescale()
    start_time = ts.from_datetime(start)
    end_time = ts.from_datetime(end)
    stations_windows = {}
    satellite_control = {}
    stations = Station.objects.filter(status=2)

    satellite_filter = {"launch__id": launch_id}
    if sat_status:
        satellite_filter["status"] = "alive"
        if sat_status == "unknown":
            satellite_filter["unknown"] = True
        else:
            satellite_filter["unknown"] = False
    satellites = Satellite.objects.filter(**satellite_filter)

    # Calculation of all possible satellite flights over active stations, grouping flights by windows
    for satellite in satellites:
        sat_stat = SatelliteStats(satellite.norad_cat_id)
        if satellite.is_frequency_violator:
            sat_stat.is_violator = True
            sat_stat.violator_obs = violator_count
            sat_stat.observation_count = 10
        satellite_control[satellite.norad_cat_id] = sat_stat
        try:
            tle = satellite.latest_tle_set.latest
        except AttributeError:
            continue
        satellite_sky_obj = EarthSatellite(tle.tle1, tle.tle2, tle.tle0, ts)
        for station in stations:
            transmitter = get_available_transmitter(satellite, station)
            if transmitter is None:
                continue
            station_sky_obj = wgs84.latlon(station.lat, station.lng, station.alt)
            difference = satellite_sky_obj - station_sky_obj
            times, events = satellite_sky_obj.find_events(
                station_sky_obj, start_time, end_time, altitude_degrees=min_horizon
            )
            if station.id not in stations_windows:
                stations_windows[station.id] = []
            update_station_windows(
                station_sky_obj,
                satellite_sky_obj,
                stations_windows[station.id],
                satellite_control[satellite.norad_cat_id],
                satellite.norad_cat_id,
                satellite.sat_id,
                transmitter,
                times,
                events,
                difference,
                min_elevation,
            )

    # Formation of specific windows for observations
    observations_data = []
    for station_id, station in stations_windows.items():
        for window in station:
            window.calculate_times()
            current_start_time = window.min_time
            current_end_time = current_start_time + timedelta(seconds=split_duration)
            if len(window.satellites) == 1:
                current_end_time = window.max_time
            current_satellite = window.get_next_satellite(
                current_start_time, current_end_time
            )
            while current_satellite:
                start_alt, start_az, start_dist = current_satellite.get_altaz(
                    ts.from_datetime(current_start_time)
                )
                end_alt, end_az, end_dist = current_satellite.get_altaz(
                    ts.from_datetime(current_end_time)
                )
                elevation_time = current_satellite.get_culminate_time(
                    current_start_time, current_end_time
                )
                if elevation_time is None:
                    if (
                        start_alt.degrees >= min_elevation
                        or end_alt.degrees >= min_elevation
                    ):
                        if start_alt.degrees > end_alt.degrees:
                            elevation = start_alt
                        else:
                            elevation = end_alt
                    else:
                        if (
                            current_end_time + timedelta(minutes=1)
                            <= current_satellite.end_time.utc_datetime()
                        ):
                            current_end_time += timedelta(minutes=1)
                        else:
                            current_satellite = window.get_next_satellite(
                                current_start_time,
                                current_end_time,
                                current_satellite,
                                True,
                            )
                        continue
                else:
                    elevation = current_satellite.get_altaz(elevation_time)[0]
                if elevation.degrees > settings.LAUNCH_GOOD_ELEVATION:
                    current_satellite.satellite_priority.good_obs += 1

                observation = {
                    "station_id": station_id,
                    "satellite_norad": current_satellite.norad_id,
                    "satellite_id": current_satellite.sat_id,
                    "transmitter": current_satellite.transmitter,
                    "start_time": current_start_time,
                    "end_time": current_end_time,
                    "elevation": float(format(elevation.degrees, ".1f")),
                    "start_az": float(format(start_az.degrees, ".1f")),
                    "end_az": float(format(end_az.degrees, ".1f")),
                }

                observations_data.append(observation)

                current_satellite.scheduled = True
                current_satellite.satellite_priority.observation_count += 1
                if current_satellite.satellite_priority.is_violator:
                    current_satellite.satellite_priority.observation_count += 10
                    current_satellite.satellite_priority.violator_obs -= 1
                window.calculate_times()
                next_start_time = current_end_time + timedelta(
                    seconds=settings.LAUNCH_BREAK_DURATION
                )
                current_start_time = next_start_time
                if current_start_time < window.min_time:
                    current_start_time = window.min_time
                current_end_time = current_start_time + timedelta(
                    seconds=split_duration
                )
                current_satellite = window.get_next_satellite(
                    current_start_time, current_end_time, current_satellite
                )

    return observations_data


def get_available_transmitter(
    satellite: Satellite, station: Station
) -> Optional[Transmitter]:
    """
    Search for an available transmitter according to the station's antenna frequencies

    Args:
        satellite (Satellite): The satellite whose transmitter needs to be found
        station (Station): The station according to whose frequencies the search for a transmitter takes place

    Returns:
        (Optional[Transmitter]): Available transmitter or None if none of the satellite transmitters
            match the frequencies of the specified station
    """
    transmitters = satellite.transmitter_entries.filter(status="active")
    frequency_ranges = FrequencyRange.objects.filter(antenna__station=station)
    for freq_range in frequency_ranges:
        transmitter = transmitters.filter(
            downlink_low__range=(freq_range.min_frequency, freq_range.max_frequency)
        )
        if transmitter.count():
            return transmitter[0]

    return None


def create_new_launch_observations(
    launch_observations: list[dict[str, Any]], author: User
) -> dict[Union[str, int], int]:
    """
    Creating and saving new objects Observation

    Args:
        launch_observations (list[dict[str, Any]]): List of parameters for forming observations
        author (User): The user on whose behalf the observations will be scheduled

    Returns:
        (dict[Union[str, int], int]): Number of scheduled observations for each satellite
            and total number of scheduled observations
    """
    observations_count = 0
    observations_stat = {}
    for launch_obs in launch_observations:
        station = Station.objects.get(id=launch_obs["station_id"])
        # Search for existing observations that overlap this window
        # If an overlapping observation is found, the current window is not saved
        overlapped_obs = Observation.objects.filter(
            Q(ground_station=station)
            & (
                (
                    Q(start__lte=launch_obs["start_time"])
                    & Q(end__gte=launch_obs["start_time"])
                )
                | (
                    Q(start__lte=launch_obs["end_time"])
                    & Q(end__gte=launch_obs["end_time"])
                )
            )
        )
        if len(overlapped_obs) == 0:
            transmitter = launch_obs["transmitter"]
            satellite = transmitter.satellite
            tle = satellite.latest_tle_set.latest
            observation = Observation(
                satellite=satellite,
                tle=tle,
                transmitter=transmitter,
                ground_station=station,
                author=author,
                start=launch_obs["start_time"],
                end=launch_obs["end_time"],
                rise_azimuth=launch_obs["start_az"],
                max_altitude=launch_obs["elevation"],
                set_azimuth=launch_obs["end_az"],
                center_frequency=None,
                is_auto=True,
            )
            observation.save()
            observations_count += 1
            if satellite.norad_cat_id in observations_stat:
                observations_stat[satellite.norad_cat_id] += 1
            else:
                observations_stat[satellite.norad_cat_id] = 1
    observations_stat["Total_Observations"] = observations_count
    return observations_stat
