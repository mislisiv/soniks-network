import io
from datetime import datetime, timedelta, timezone
from typing import List, Tuple

import matplotlib.pyplot as plt
import numpy as np
from django.db.models.query import QuerySet
from skyfield.api import EarthSatellite, load, wgs84
from skyfield.timelib import Time

from network.base.models import DemodData, Observation, Station, Tle

COLOR_EDGE = "#fafafa"
COLOR_FILL = "#fe5e22"
COLOR_TEXT = "#fafafa"


class StationStatCalculator:
    """
    Class to calculate statistics for a station.

    This class encapsulates the logic to calculate the polar diagram for a
    station. It fetches the necessary data from the database and calculates
    the coordinates for the polar diagram.

    Attributes:
        station (Station): The station object.
        demoddata_coordinates (List[Tuple[float, float, Time]]): The list of
            coordinates for the polar diagram. The coordinates are in the
            format (altitude in degrees, azimuth in degrees, timestamp).
    """

    # Примечание: в кортежах координах сначала идет высота, потом азимут
    # Координаты высоты надо удваивать, т.к ширина графика на 1 пиксель соответствует 0.5 градуса
    # Для полярного нужно поворачивать метки на 90 градусов против часовой стрелки, т.к
    # базово pyplot рисует полярный с нулем на оси x
    def __init__(self, station_id: int) -> None:
        """
        Initialize the StationStatCalculator with the given station_id.

        It fetches the station object and populates the `self.demoddata_coordinates` list
        with empty list.

        Args:
            station_id (int): ID of the station to calculate statistics for.

        Returns:
            None
        """
        self.station: Station = Station.objects.only("id", "lat", "lng", "alt").get(
            id=station_id
        )
        self.demoddata_coordinates: List[Tuple[float, float, Time]] = []

    def set_demoddata_coordinates(self) -> None:
        """
        Sets the demoddata coordinates for the station.

        This function calculates the altitude and azimuth for each DemodData
        associated with observations of the station. It populates the
        `self.demoddata_coordinates` list with tuples containing the altitude
        in degrees, azimuth in degrees, and the timestamp.

        The function filters observations associated with the station and
        calculates the topocentric coordinates for each DemodData object
        using the satellite's TLE data and the station's geographic coordinates.

        Exceptions during the calculation process are caught and logged
        with a print statement indicating the DemodData id and the error.

        Returns:
            None
        """
        self.demoddata_coordinates = []  # clear up coordinates
        ts = load.timescale()
        station = wgs84.latlon(self.station.lat, self.station.lng, self.station.alt)
        observations = Observation.objects.filter(ground_station=self.station)
        for observation in observations:
            tle: Tle = observation.tle
            if tle is None:
                continue
            sat: EarthSatellite = EarthSatellite(tle.tle1, tle.tle2, tle.tle0, ts)
            obs_demoddata: QuerySet[DemodData] = DemodData.objects.filter(
                observation=observation
            ).only("id", "timestamp")
            difference = sat - station
            for demoddata in obs_demoddata.values("id", "timestamp"):
                try:
                    t: Time = ts.from_datetime(demoddata["timestamp"])
                    topocentric = difference.at(t)
                    alt, az, _ = topocentric.altaz()
                    self.demoddata_coordinates.append((alt.degrees, az.degrees, t))
                except Exception as e:
                    print(f"Error processing demoddata with id {demoddata['id']}: {e}")

    def calculate_svg_altitude(self) -> str:
        """
        Generates an SVG histogram of the altitude distribution from the
        demoddata coordinates.

        This function creates a histogram plot of altitudes, with bins ranging
        from 0 to 90 degrees at 5-degree intervals. The plot is styled with
        specific axis limits and hidden top and right spines for a cleaner
        appearance. The histogram is saved in SVG format and returned as a
        string.

        Returns:
            str: A string containing the SVG representation of the altitude
            histogram.
        """
        with io.BytesIO() as buffer:
            fig, ax = plt.subplots(figsize=(5, 5))
            altitudes: List[float] = [x[0] for x in self.demoddata_coordinates]
            ax.hist(
                altitudes, bins=range(0, 91, 5), edgecolor=COLOR_EDGE, color=COLOR_FILL
            )
            ax.set_ylim(0, max(ax.get_ylim()[1], 1))
            ax.set_xlim(0, max(ax.get_xlim()[1], 1))
            ax.spines["bottom"].set_position(("zero"))
            ax.spines["bottom"].set_color(COLOR_EDGE)
            ax.spines["left"].set_position(("zero"))
            ax.spines["left"].set_color(COLOR_EDGE)
            ax.spines["top"].set_visible(False)
            ax.spines["right"].set_visible(False)
            ax.xaxis.label.set_color(COLOR_TEXT)
            ax.yaxis.label.set_color(COLOR_TEXT)
            ax.tick_params(axis="x", colors=COLOR_TEXT)
            ax.tick_params(axis="y", colors=COLOR_TEXT)
            plt.savefig(
                buffer,
                format="svg",
                bbox_inches="tight",
                pad_inches=0,
                transparent=True,
            )
            plt.close(fig)
            return buffer.getvalue().decode("utf-8")

    def calculate_svg_azimuth(self, elevation_max: int = 91) -> str:
        """
        Generates an SVG histogram of the azimuth distribution from the
        demoddata coordinates at or below a given elevation.

        This function creates a histogram plot of azimuths, with bins ranging
        from 0 to 360 degrees at 10-degree intervals. The y-axis is scaled to
        the maximum number of observations at a given azimuth. The x-axis is
        limited to the elevation given by the elevation_max parameter.

        The plot is styled with specific axis limits and hidden top and right
        spines for a cleaner appearance. The histogram is saved in SVG format
        and returned as a string.

        Parameters:
            elevation_max (int): The maximum elevation to include in the
                histogram, in degrees. Defaults to 90 degrees.

        Returns:
            str: A string containing the SVG representation of the azimuth
            histogram.
        """
        with io.BytesIO() as buffer:
            fig, ax = plt.subplots(figsize=(5, 5))
            azimuths = [
                x[1] for x in self.demoddata_coordinates if x[0] <= elevation_max
            ]
            ax.hist(
                azimuths, bins=range(0, 361, 10), edgecolor=COLOR_EDGE, color=COLOR_FILL
            )
            ax.set_ylim(0, max(ax.get_ylim()[1], 1))
            ax.set_xlim(0, 360)
            ax.spines["bottom"].set_color(COLOR_EDGE)
            ax.spines["bottom"].set_position(("zero"))
            ax.spines["left"].set_color(COLOR_EDGE)
            ax.spines["left"].set_position(("zero"))
            ax.spines["top"].set_visible(False)
            ax.spines["right"].set_visible(False)
            ax.xaxis.label.set_color(COLOR_TEXT)
            ax.yaxis.label.set_color(COLOR_TEXT)
            ax.tick_params(axis="x", colors=COLOR_TEXT)
            ax.tick_params(axis="y", colors=COLOR_TEXT)
            plt.savefig(
                buffer,
                format="svg",
                bbox_inches="tight",
                pad_inches=0,
                transparent=True,
            )
            plt.close(fig)
            return buffer.getvalue().decode("utf-8")

    def calculate_svg_polar(self, time_period: str = "month") -> str:
        """
        Generates an SVG polar plot of the satellite passes within a given time period.

        This function creates a polar plot with 24 radial lines representing the
        hours of the day, with the outermost circle representing an altitude of
        90 degrees and the innermost circle representing an altitude of 0 degrees.
        The x-axis represents the time of day, and the y-axis represents the
        altitude of the satellite.

        The plot is styled with specific axis limits and hidden top and right
        spines for a cleaner appearance. The scatter points are colored blue.

        Parameters:
            time_period (str): The time period to include in the plot. Can be
                "month", "week", or "day". Defaults to "month".

        Returns:
            str: A string containing the SVG representation of the polar plot.
        """
        with io.BytesIO() as buffer:
            fig = plt.figure(figsize=(5, 5))
            ax = fig.add_subplot(polar=True)
            for i in range(24):
                angle = (90 - i * 15) * np.pi / 180
                ax.text(
                    angle,
                    195,
                    str(i * 15),
                    fontsize=10,
                    ha="center",
                    va="bottom",
                    color=COLOR_TEXT,
                )
            for i in range(20, 181, 20):
                r = i
                circle = plt.Circle(
                    (0, 0),
                    r,
                    transform=ax.transData._b,
                    fill=False,
                    edgecolor=COLOR_EDGE,
                    facecolor="k",
                )
                ax.add_patch(circle)
                angle = (0 * 15 + 90) * np.pi / 180
                ax.text(
                    angle,
                    r + 5,
                    str((180 - r) / 2),
                    fontsize=8,
                    ha="center",
                    va="center",
                    color=COLOR_TEXT,
                )
            ax.set_ylim(0, 180)
            ax.set_yticklabels([])
            ax.set_xticklabels([])
            if time_period == "month":
                time_window = load.timescale().from_datetime(
                    datetime.now(timezone.utc) - timedelta(days=30)
                )
            elif time_period == "week":
                time_window = load.timescale().from_datetime(
                    datetime.now(timezone.utc) - timedelta(days=7)
                )
            elif time_period == "day":
                time_window = load.timescale().from_datetime(
                    datetime.now(timezone.utc) - timedelta(days=1)
                )
            else:
                raise ValueError("Invalid time period")
            for demoddata in self.demoddata_coordinates:
                if demoddata[2] > time_window:
                    angle = (90 + demoddata[1]) * np.pi / 180
                    elevation = 180 - (demoddata[0] * 2)
                    ax.scatter(
                        angle,
                        elevation,
                        c=COLOR_FILL,
                        s=5,
                    )
            plt.savefig(
                buffer,
                format="svg",
                bbox_inches="tight",
                pad_inches=0,
                transparent=True,
            )
            plt.close(fig)
            return buffer.getvalue().decode("utf-8")
