"""Django base URL routings for SONIKS Network"""

from django.urls import path, re_path

from network.base.views import (
    auto_scheduling,
    db,
    generic,
    launch,
    observation,
    satellite,
    scheduling,
    station,
    statistics,
    transmitter,
)

RE_SAT_ID = r"[A-Z]{4,4}(?:-\d\d\d\d){4,4})/$"

BASE_URLPATTERNS = (
    [
        # Generic
        path("", generic.index, name="home"),
        path("about/", generic.about, name="about"),
        path("robots.txt", generic.robots, name="robots"),
        path("settings_site/", generic.settings_site, name="settings_site"),
        # Partners
        path("rko/", generic.space_education, name="space_education"),
        path(r"openid_logout/", generic.openid_logout, name="openid_logout"),
        path(
            "space-education-development/",
            generic.space_education_redirect,
            name="space_education_redirect",
        ),
        path(r"education/", generic.education, name="education"),
        # Observations
        path(
            "observations/",
            observation.ObservationListView.as_view(),
            name="observations_list",
        ),
        re_path(
            r"^observations/(?P<observation_id>[0-9]+)/$",
            observation.observation_view,
            name="observation_view",
        ),
        re_path(
            r"^observations/(?P<observation_id>[0-9]+)/delete/$",
            observation.observation_delete,
            name="observation_delete",
        ),
        path(
            "vet-observations/",
            observation.VetObservationsView.as_view(),
            name="vet_observations",
        ),
        path(
            "vet-observations-chunks/",
            observation.VetObservationsChunkListView.as_view(),
            name="vet_observations_chunks",
        ),
        re_path(
            r"^waterfall_vet/(?P<observation_id>[0-9]+)/$",
            observation.waterfall_vet,
            name="waterfall_vet",
        ),
        re_path(
            r"^observations/delete_several/$",
            observation.observation_delete_several,
            name="observation_delete_several",
        ),
        # Satellites
        path(
            "satellites/", satellite.SatelliteListView.as_view(), name="satellites_list"
        ),
        re_path(
            r"^satellites-popup/(?P<norad_id>[0-9]+)/$",
            satellite.satellite_modal_view,
            name="satellite_modal_view",
        ),
        re_path(
            r"^satellites/(?P<sat_id>[A-Z]{4,4}(?:-\d\d\d\d){4,4})/$",
            satellite.satellite_view,
            name="satellite_view",
        ),
        re_path(
            r"^satellites/(?P<norad_id>[0-9]+)/$",
            satellite.satellite_view,
            name="satellite_view",
        ),
        # Transmitters
        path(
            "transmitters/",
            transmitter.TransmitterListView.as_view(),
            name="transmitters_view",
        ),
        path(
            "transmitters_list/", scheduling.transmitters_list, name="transmitters_list"
        ),
        path("spectrum/", generic.spectrum, name="spectrum"),
        path(
            "edit_transmitter/",
            transmitter.TransmitterEditView.as_view(),
            name="edit_transmitter",
        ),
        # Stations
        path("stations_all/", station.station_all_view, name="stations_all"),
        path("stations/", station.StationListView.as_view(), name="stations_list"),
        re_path(
            r"^stations/(?P<station_id>[0-9]+)/$",
            station.station_view,
            name="station_view",
        ),
        re_path(
            r"^stations/(?P<station_id>[0-9]+)/delete/$",
            station.station_delete,
            name="station_delete",
        ),
        re_path(
            r"^stations/(?P<station_id>[0-9]+)/delete_future_observations/$",
            station.station_delete_future_observations,
            name="station_delete_future_observations",
        ),
        re_path(
            "stations/register/step(?P<step>1)/$",
            station.station_register,
            name="station_register",
        ),
        re_path(
            "stations/register/step(?P<step>2)/$",
            station.station_register,
            name="station_register",
        ),
        re_path(
            "stations/register/step(?P<step>2)/(?P<station_id>[0-9]+)/$",
            station.station_register,
            name="station_register",
        ),
        path("stations/edit/", station.station_edit, name="station_edit"),
        re_path(
            r"^stations/edit/(?P<station_id>[0-9]+)/$",
            station.station_edit,
            name="station_edit",
        ),
        re_path(
            r"^station-popup/(?P<sat_id>[A-Z]{4,4}(?:-\d\d\d\d){4,4})/(?P<station_id>[0-9]+)/$",
            station.station_view_popup,
            name="station_view_popup",
        ),
        # Scheduling
        path("observations/new/", scheduling.observation_new, name="observation_new"),
        path(
            "prediction_windows/",
            scheduling.prediction_windows,
            name="prediction_windows",
        ),
        re_path(
            r"^pass_predictions/(?P<station_id>[\w.@+-]+)/$",
            scheduling.pass_predictions,
            name="pass_predictions",
        ),
        re_path(
            r"^pass_predictions/(?P<station_id>[\w.@+-]+)/(?P<sat_id>" + RE_SAT_ID,
            scheduling.pass_predictions,
            name="pass_predictions",
        ),
        path(
            "scheduling_stations/",
            scheduling.scheduling_stations,
            name="scheduling_stations",
        ),
        # Telemetry
        path(
            "frames/<int:sat_pk>/", satellite.request_export, name="request_export_all"
        ),
        path(
            "frames/<int:sat_pk>/<int:period>/",
            satellite.request_export,
            name="request_export",
        ),
        path("export_tle/<int:sat_pk>/", satellite.request_tle, name="request_tle_all"),
        path(
            "export_tle/<int:sat_pk>/<int:period>/",
            satellite.request_tle,
            name="request_tle",
        ),
        re_path(
            r"^decoded_data/(?P<sat_id>" + RE_SAT_ID,
            satellite.recent_decoded_cnt,
            name="decoded_data",
        ),
        # Statistics
        path("statistics/", statistics.statistics_view, name="statistics"),
        re_path(
            "statistics/modes/", statistics.statistics_modes, name="statistics_modes"
        ),
        re_path(
            "statistics/top/satellites/",
            statistics.top_satellites,
            name="top_satellites",
        ),
        re_path(
            "statistics/top/stations/", statistics.top_stations, name="top_stations"
        ),
        path(
            "satellite_statistics/",
            statistics.satellite_statistics,
            name="satellite_statistics",
        ),
        path(
            "station_statistics/",
            statistics.station_statistics,
            name="station_statistics",
        ),
        # DB
        path("db/", db.db_view, name="db_view"),
        re_path(r"^db/gallery/$", db.gallery_view, name="gallery_view"),
        re_path(
            r"^db/gallery/load/(?P<sat_id>" + RE_SAT_ID,
            db.gallery_load_images,
            name="gallery_load_images",
        ),
        re_path(
            r"^db/gallery/(?P<sat_id>" + RE_SAT_ID,
            db.satellite_gallery,
            name="satellite_gallery",
        ),
        # auto-scheduling_station
        path(
            "save_auto_schedule/",
            auto_scheduling.save_schedule,
            name="save_auto_scheduling",
        ),
        path(
            "save_auto_schedule_status/",
            auto_scheduling.save_status,
            name="save_auto_scheduling_status",
        ),
        path(
            "save_auto_schedule_parameters/",
            auto_scheduling.save_parameters,
            name="save_auto_scheduling_parameters",
        ),
        path(
            "parameters_list/",
            auto_scheduling.parameters_list,
            name="parameters_list",
        ),
        # auto-scheduling_station second schedule
        path(
            "save_auto_schedule_sec/",
            auto_scheduling.save_schedule_sec,
            name="save_auto_scheduling_sec",
        ),
        path(
            "save_auto_schedule_status_sec/",
            auto_scheduling.save_status_sec,
            name="save_auto_scheduling_status_sec",
        ),
        path(
            "save_auto_schedule_parameters_sec/",
            auto_scheduling.save_parameters_sec,
            name="save_auto_scheduling_parameters_sec",
        ),
        path(
            "parameters_list_sec/",
            auto_scheduling.parameters_list_sec,
            name="parameters_list_sec",
        ),
        # auto_scheduling_network
        path(
            "save_auto_schedule_network/",
            auto_scheduling.save_schedule_network,
            name="save_auto_scheduling_network",
        ),
        path(
            "save_auto_schedule_status_network/",
            auto_scheduling.save_status_network,
            name="save_auto_scheduling_status_network",
        ),
        path(
            "save_auto_schedule_parameters_network/",
            auto_scheduling.save_parameters_network,
            name="save_auto_scheduling_parameters_network",
        ),
        path(
            "parameters_list_network/",
            auto_scheduling.parameters_list_network,
            name="parameters_list_network",
        ),
        # Launch
        path(
            "launches/",
            launch.LaunchListView.as_view(),
            name="launches",
        ),
        re_path(
            r"^launches/(?P<launch_id>[0-9]+)/$",
            launch.launch_view,
            name="launch_view",
        ),
        re_path(
            r"^launches/(?P<launch_id>[0-9]+)/schedule/$",
            launch.schedule_launch_observations,
            name="schedule_launch_observations",
        ),
        re_path(
            r"^launches/(?P<launch_id>[0-9]+)/delete_observations/$",
            launch.delete_observations_for_launch_scheduler,
            name="delete_observations_for_launch_scheduler",
        ),
    ],
    "base",
)
