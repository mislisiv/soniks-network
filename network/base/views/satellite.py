"""Django base views for SONIKS Network"""

from datetime import timedelta
from typing import Optional

from bootstrap_modal_forms.generic import BSModalCreateView
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import MultipleObjectsReturned
from django.db.models import Count, IntegerField, Max, OuterRef, Q, Subquery
from django.db.models.functions import Coalesce
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView

from network.api import serializers
from network.base.forms import SatelliteCreateForm, SatelliteEditForm
from network.base.models import (
    DemodData,
    LatestTleSet,
    Network,
    Observation,
    Operator,
    Satellite,
    Station,
    Telemetry,
    Transmitter,
    generate_norad_id,
    generate_sat_id,
)
from network.base.orbital_satellite_params import OrbitalSatelliteParams
from network.base.perms import schedule_perms
from network.base.stats import get_satellite_stats_by_transmitter_list
from network.base.tasks import export_frames, export_tle
from network.base.utils import millify


class SatelliteListView(ListView):  # pylint: disable=R0901
    """
    Displays a list of satellites
    """

    model = Satellite
    context_object_name = "satellites"
    flag_filters = ["alive", "dead", "future", "re_entered"]
    name_filters = ["norad_cat_id", "operator", "network", "start", "end"]
    more_filtered = None

    @property
    def template_name(self):
        """Defining a template depending on the language"""
        # TODO устарело
        return "base/satellites.html"

    def get_filter_params(self):
        """
        Get the parsed filter parameters from the HTTP GET parameters
        """

        filter_params = {}
        for param_name in self.name_filters:
            filter_params[param_name] = self.request.GET.get(param_name, "")
        for param_name in self.flag_filters:
            param = self.request.GET.get(param_name, 1)
            filter_params[param_name] = param != "0"

        return filter_params

    def get_queryset(self):
        """
        Get queryset into account the filter
        """

        start = self.request.GET.get("start", False)
        end = self.request.GET.get("end", False)
        has_decoder = self.request.GET.get("decoder", False)
        name = self.request.GET.get("name", None)
        parameter_filter_mapping = {
            "norad_cat_id": "norad_cat_id",
            "operator": "operator__id",
            "network": "network__id",
        }
        filter_params = self.get_filter_params()
        filter_dict = {}
        for key, val in parameter_filter_mapping.items():
            if filter_params[key] == "":
                continue
            filter_dict[val] = filter_params[key]

        satellites = Satellite.objects.prefetch_related("operator", "network").exclude(
            approved=False
        )
        if has_decoder == "y":
            satellites = satellites.exclude(telemetries=None)
        elif has_decoder == "n":
            satellites = satellites.filter(telemetries=None)

        if start or end:
            observation_subquery = None
            if start and end:
                observation_subquery = (
                    Observation.objects.filter(
                        Q(satellite=OuterRef("pk"))
                        & Q(start__gt=start)
                        & Q(end__lt=end)
                    )
                    .values("satellite")
                    .annotate(count=Count("id", distinct=True))
                    .values("count")
                )
            elif start:
                observation_subquery = (
                    Observation.objects.filter(
                        Q(satellite=OuterRef("pk")) & Q(start__gt=start)
                    )
                    .values("satellite")
                    .annotate(count=Count("id", distinct=True))
                    .values("count")
                )
            elif end:
                observation_subquery = (
                    Observation.objects.filter(
                        Q(satellite=OuterRef("pk")) & Q(end__lt=end)
                    )
                    .values("satellite")
                    .annotate(count=Count("id", distinct=True))
                    .values("count")
                )

            satellites = satellites.filter(**filter_dict).annotate(
                approved_transmitters_count=Count(
                    "transmitter_entries", filter=Q(approved=True), distinct=True
                ),
                total_obs=Coalesce(
                    Subquery(observation_subquery, output_field=IntegerField()), 0
                ),
            )
        else:
            satellites = satellites.filter(**filter_dict).annotate(
                approved_transmitters_count=Count(
                    "transmitter_entries", filter=Q(approved=True), distinct=True
                ),
                total_obs=Count("observations", distinct=True),
            )
        if name is not None:
            satellites = satellites.filter(
                Q(name__icontains=name)
                | Q(norad_cat_id__icontains=name)
                | Q(call_sign__icontains=name)
            )
        for i in self.flag_filters:
            if not filter_params[i]:
                if i == "re_entered":
                    satellites = satellites.exclude(status="re-entered")
                else:
                    satellites = satellites.exclude(status=i)

        self.more_filtered = start or end or has_decoder

        return satellites

    def get_display_columns(self):
        """
        Keep columns displayed when refreshing page
        """

        columns_list = [
            "Satellite_ID",
            "Name",
            "NORAD",
            "Status",
            "Operator",
            "Network",
            "Transmitters",
            "Data",
            "Obs",
            "Alt_Names",
            "Followed_Norad",
            "Launched",
            "Website",
            "Country",
            "Название",
            "Статус",
            "Оператор",
            "Сеть",
            "Передатчики",
            "Позывной",
            "Данные",
            "Наблюдения",
            "Запуск",
            "Страна",
        ]

        columns_display = {}
        for column in columns_list:
            columns_display[column] = self.request.COOKIES.get(column)

        return columns_display

    def get_context_data(self, **kwargs):
        """
        Saving filter context
        """

        context = super().get_context_data(**kwargs)
        context["alive"] = self.request.GET.get("alive", "1")
        context["re_entered"] = self.request.GET.get("re_entered", "1")
        context["future"] = self.request.GET.get("future", "1")
        context["dead"] = self.request.GET.get("dead", "1")
        context["operators"] = Operator.objects.all()
        context["networks"] = Network.objects.all()
        context["start"] = self.request.GET.get("start", None)
        context["end"] = self.request.GET.get("end", None)
        context["period"] = self.request.GET.get("period", None)
        context["name"] = self.request.GET.get("name", None)
        selected_operator = self.request.GET.get("operator", None)
        if selected_operator is not None and selected_operator != "":
            context["selected_operator"] = int(selected_operator)
        selected_network = self.request.GET.get("network", None)
        if selected_network is not None and selected_network != "":
            context["selected_network"] = int(selected_network)
        norad_cat_id = self.request.GET.get("norad_cat_id", None)
        if norad_cat_id is not None and norad_cat_id != "":
            context["norad_cat_id"] = int(norad_cat_id)
        context["more_filtered"] = bool(self.more_filtered)
        context["columns"] = self.get_display_columns()
        context["has_decoder"] = self.request.GET.get("decoder", "")
        context["page"] = "satellites"

        return context


def satellite_view(request, sat_id=None, norad_id=None, json=None):
    """
    Satellite page view
    """
    if sat_id:
        satellite = get_object_or_404(Satellite, sat_id=sat_id)
    else:
        satellite = get_object_or_404(Satellite, norad_cat_id=norad_id)
        sat_id = satellite.sat_id
    transmitters = Transmitter.objects.filter(
        satellite__norad_cat_id=satellite.norad_cat_id
    )

    recent_observers = (
        Observation.objects.prefetch_related("satellite", "ground_station")
        .filter(satellite__sat_id=sat_id, demoddata__isnull=False)
        .annotate(latest_data=Max("demoddata__timestamp"))
        .order_by("-end")[:5]
    )

    stations = Station.objects.all()
    try:
        decoder = Telemetry.objects.get(satellite=satellite)
    except Telemetry.DoesNotExist:
        decoder = None
    except MultipleObjectsReturned:
        decoder = Telemetry.objects.filter(satellite=satellite).latest("id")
    try:
        tle = LatestTleSet.objects.get(satellite=satellite).latest
    except LatestTleSet.DoesNotExist:
        tle = None

    sat_orbit_parameters = {}
    try:
        orbit_obj = OrbitalSatelliteParams(
            satellite_name=satellite.name, tle1=tle.tle1, tle2=tle.tle2
        )
        sat_orbit_parameters["inclination"] = round(
            orbit_obj.get_inclination(), 1
        )  # degrees
        sat_orbit_parameters["period"] = round(orbit_obj.get_period(), 1)  # min
        sat_orbit_parameters["semi_major_axis"] = round(
            orbit_obj.get_semi_axe(), 1
        )  # km
        sat_orbit_parameters["velocity"] = round(orbit_obj.get_velocity(), 2)  # km/sec
    except Exception:  # pylint: disable=W0703
        sat_orbit_parameters = {}

    showmap = False
    if satellite.status not in ["re-entered", "future"] and tle:
        if tle.updated > (now() - timedelta(days=7)):
            showmap = True

    if json:
        return {
            "satellite": satellite,
        }

    obs_count = Observation.objects.filter(satellite=satellite).count()
    demoddata_count = DemodData.objects.filter(satellite=satellite).count()

    data = {
        "satellite": satellite,
        "decoder": decoder,
        "obs_count": obs_count,
        "data_count": millify(demoddata_count),
        "transmitters": transmitters,
        "stations": stations,
        "recent_observers": recent_observers,
        "mapbox_id": settings.MAPBOX_MAP_ID,
        "mapbox_token": settings.MAPBOX_TOKEN,
        "latest_tle": tle,
        "showmap": showmap,
        "parameters": sat_orbit_parameters,
        "can_schedule": schedule_perms(request.user),
    }
    return render(request, "base/satellite_view.html", data)


class SatelliteCreateView(LoginRequiredMixin, BSModalCreateView):
    """
    Display Satellite create form
    """

    template_name = "base/modals/create_satellite.html"
    model = Satellite
    form_class = SatelliteCreateForm
    success_message = _(
        "Ваше предложение спутника успешно сохранено и будет \n проверено модератором. Спасибо за вклад!"
    )

    user = get_user_model()

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        satellite = form.instance
        # Create Satellite Identifier only when POST request is for saving and
        # NORAD ID is not used by other Satellite.
        # Check if request is an AJAX one
        if not self.request.headers.get("x-requested-with") == "XMLHttpRequest":
            try:
                # If the form doesn't contain NORAD ID, create a new satellite
                if satellite.norad_cat_id:
                    try:
                        if Satellite.objects.get(norad_cat_id=satellite.norad_cat_id):
                            satellite.norad_cat_id = generate_norad_id()
                    except Satellite.DoesNotExist:
                        pass
                else:
                    satellite.sat_id = generate_sat_id()
                    satellite.norad_cat_id = generate_norad_id()
            except Satellite.DoesNotExist:
                satellite.sat_id = generate_sat_id()
        satellite.approved = False
        satellite.created = now()
        satellite.created_by = self.user
        response = super().form_valid(form)

        return response

    def get_success_url(self):
        return self.request.META.get("HTTP_REFERER")


class SatelliteEditView(LoginRequiredMixin, BSModalCreateView):
    """
    Display Satellite edit form
    """

    template_name = "base/modals/edit_satellite.html"
    model = Satellite
    form_class = SatelliteEditForm
    success_message = _(
        "Ваше предложение спутника успешно сохранено и будет \n проверено модератором. Спасибо за вклад!"
    )

    error_message = _("Форма недействительна")

    user = get_user_model()

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        satellite = form.instance

        try:
            satellite_obj = Satellite.objects.get(sat_id=satellite.sat_id_by_changes)
            if satellite.norad_cat_id == satellite_obj.norad_cat_id:
                satellite.norad_cat_id = satellite.norad_cat_id * 100
            else:
                try:
                    Satellite.objects.get(norad_cat_id=satellite.norad_cat_id)
                    messages.error(self.request, "This norad_id already exists")
                    return redirect(self.request.META.get("HTTP_REFERER"))
                except Satellite.DoesNotExist:
                    pass
            satellite.sat_id = generate_sat_id()
            satellite.sat_id_by_changes = satellite_obj.sat_id
            satellite.norad_id_by_changes = satellite_obj.norad_cat_id
            satellite.changes = True
            satellite.approved = False
            satellite.created = now()
            satellite.created_by = self.user
            return super().form_valid(form)
        except Satellite.DoesNotExist:
            messages.error(self.request, self.error_message)
            return redirect(self.request.META.get("HTTP_REFERER"))

    def get_success_url(self):
        return self.request.META.get("HTTP_REFERER")


def satellite_modal_view(request, norad_id: int):
    """
    Returns a satellite JSON object with information and statistics
    """
    try:
        sat = Satellite.objects.get(norad_cat_id=norad_id)
    except Satellite.DoesNotExist:
        data = {"error": _("Не удалось найти этот спутник.")}
        return JsonResponse(data, safe=False)

    transmitters = (
        Transmitter.objects.filter(satellite__norad_cat_id=norad_id)
        .order_by("status", "-stat__total_count")
        .annotate(
            future_count=Count("observations", filter=Q(observations__end__gt=now()))
        )
    )

    satellite_stats = get_satellite_stats_by_transmitter_list(transmitters)

    transmitters = serializers.TransmitterSerializer(transmitters, many=True).data

    data = {
        "id": str(norad_id),
        "name": sat.name,
        "names": sat.names,
        "image": sat.image.name,
        "status": sat.status,
        "unknown": sat.unknown,
        "success_rate": satellite_stats["success_rate"],
        "good_count": satellite_stats["good_count"],
        "bad_count": satellite_stats["bad_count"],
        "unknown_count": satellite_stats["unknown_count"],
        "future_count": satellite_stats["future_count"],
        "total_count": satellite_stats["total_count"],
        "transmitters": transmitters,
        "can_schedule": schedule_perms(request.user),
    }

    return JsonResponse(data, safe=False)


def recent_decoded_cnt(request, sat_id: str):
    """
    Retrieving decoded data for a specific satellite for the last 30 days
    """
    data = (
        DemodData.objects.filter(
            satellite__sat_id=sat_id, timestamp__gte=(now() - timedelta(days=30))
        )
        .values("timestamp__day", "timestamp__month", "timestamp__year")
        .annotate(count=Count("id"))
        .order_by(
            "timestamp__year",
            "timestamp__month",
            "timestamp__day",
        )
    )

    return JsonResponse({"data": list(data)}, safe=False)


@login_required
def request_export(request, sat_pk: int, period: Optional[int] = None):
    """
    View to request frames export download \\
    This triggers a request to collect and zip up the requested data for\\
    download, which the user is notified of via email when the celery task is\\
    completed

    Args:
        sat_pk(int): Primary ID of satellite whick data need to export
        period(int,optional): number represent a period of exporting
            1-week,2(or other num)-month,None-all

    """
    satellite_obj = get_object_or_404(Satellite, id=sat_pk)
    export_frames.delay(satellite_obj.sat_id, request.user.id, period)
    messages.success(
        request,
        _(
            "Ваш запрос на загрузку получен. "
            "Вы получите электронное письмо, когда оно будет готово"
        ),
    )
    return redirect(
        reverse("base:satellite_view", kwargs={"sat_id": satellite_obj.sat_id})
    )


@login_required
def request_tle(request, sat_pk: int, period: Optional[int] = None):
    """
    View to request tle  download \\
    This triggers a request to collect and zip tle for download \\
    which the user is notified of via email when the celery task is\\
    completed

    Args:
        sat_pk(int): Primary ID of satellite whick tle need to export
        period(int,optional): number represent a period of exporting
            1-week,2(or other num)-month,None-all

    """
    satellite_obj = get_object_or_404(Satellite, id=sat_pk)
    export_tle.delay(satellite_obj.sat_id, request.user.id, period)
    messages.success(
        request,
        _(
            "Ваш запрос на загрузку TLE получен. "
            "Вы получите электронное письмо, когда оно будет готово"
        ),
    )
    return redirect(
        reverse("base:satellite_view", kwargs={"sat_id": satellite_obj.sat_id})
    )
