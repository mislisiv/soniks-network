"""Django base views for SONIKS Network"""

from urllib.parse import urlparse

import skyfield
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import Case, Count, F, Q, Value, When
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template.context_processors import csrf
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.timezone import now, timedelta
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView

from network.base.decorators import ajax_required
from network.base.models import (
    DemodData,
    Observation,
    Satellite,
    Station,
    Transmitter,
    TransmitterStats,
)
from network.base.perms import delete_perms, schedule_perms, vet_perms
from network.base.rating_tasks import rate_observation
from network.base.utils import community_get_discussion_details
from network.users.models import User


def get_two_days_ago():
    """
    Helper function to get the datetime 48 hours ago as formatted string

    Returns:
        str: formatted string of datetime 48 hours ago
    """
    return (now() - timedelta(days=2)).strftime("%Y-%m-%d %H:%M")


class ObservationListBaseView(ListView):  # pylint: disable=R0901
    """
    Base class for displaying a list of observations
    """

    model = Observation
    context_object_name = "observations"
    paginate_by = settings.ITEMS_PER_PAGE
    str_filters = [
        "norad",
        "observer",
        "station",
        "start",
        "end",
        "transmitter_mode",
        "transmitter_uuid",
    ]
    flag_filters = ["bad", "good", "unknown", "future", "failed"]
    more_filtered = None  # Filtered by filters hidden by default
    count_vet = 0
    start_autoset = False

    @property
    def template_name(self):
        """Defining a template depending on the language"""
        # TODO устарело
        return "base/observations.html"

    def get_filter_params(self):
        """
        Get the parsed filter parameters from the HTTP GET parameters

        Args:
            str_filters(list[str]): List of string filters,default to ''
            flag_filters(list[bool]: List of flag filters, default to False

        Returns:
            dict: dict with filter_name as key and pars param as value
        """
        filter_params = {}
        for parameter_name in self.str_filters:
            filter_params[parameter_name] = self.request.GET.get(parameter_name, "")

        for parameter_name in self.flag_filters:
            param = self.request.GET.get(parameter_name, 1)
            filter_params[parameter_name] = param != "0"

        return filter_params

    def get_queryset(self):  # pylint: disable=R0915
        """
        Optionally filter based on norad get argument
        Optionally filter based on future/good/bad/unknown/failed
        """
        filter_params = self.get_filter_params()

        results = self.request.GET.getlist("results")
        rated = self.request.GET.getlist("rated")

        observations = (
            Observation.objects.prefetch_related(
                "satellite",
                "demoddata",
                "author",
                "ground_station",
                "transmitter",
                "transmitter__downlink_mode",
            )
            .annotate(
                image_count=Count(
                    "demoddata", filter=Q(demoddata__is_image=True), distinct=True
                ),
                data_count=Count(
                    "demoddata", filter=Q(demoddata__is_image=False), distinct=True
                ),
            )
            .order_by("-start", "-end")
        )

        # Mapping between the HTTP POST parameters and the fiter keys
        parameter_filter_mapping = {
            "norad": "satellite__norad_cat_id",
            "observer": "author",
            "station": "ground_station_id",
            "start": "start__gt",
            "end": "end__lt",
            "transmitter_mode": "transmitter__downlink_mode__name__icontains",
            "transmitter_uuid": "transmitter__uuid__icontains",
        }

        # Create observations filter based on the received HTTP POST parameters
        filter_dict = {}
        for parameter_key, filter_key in parameter_filter_mapping.items():
            if filter_params[parameter_key] == "":
                continue

            filter_dict[filter_key] = filter_params[parameter_key]

        rated_filt = False
        if rated and rated != [""]:
            rated_filt = True

        results_filt = False
        if results and results != [""]:
            results_filt = True

        # If user has not filtered the results, display the observations of the last 24 hours
        if len(filter_dict) == 0:
            filter_dict["start__gt"] = get_two_days_ago()
            self.start_autoset = True

        # If the user has used the extra filters, display hte extra filter section expaned
        self.more_filtered = (
            results_filt
            or rated_filt
            or filter_dict.get("author")
            or filter_dict.get("ground_station_id")
            or filter_dict.get("transmitter__downlink_mode__name__icontains")
            or filter_dict.get("transmitter__uuid__icontains")
        )

        observations = observations.filter(**filter_dict)

        if not filter_params["failed"]:
            observations = observations.exclude(status__lt=-100, end__lte=now())
        if not filter_params["bad"]:
            observations = observations.exclude(
                status__range=(-100, -1), end__lte=now()
            )
        if not filter_params["unknown"]:
            observations = observations.exclude(status__range=(0, 99), end__lte=now())
        if not filter_params["future"]:
            observations = observations.exclude(end__gt=now())
        if not filter_params["good"]:
            observations = observations.exclude(status__gte=100, end__lte=now())

        if results:
            if "w0" in results:
                observations = observations.filter(waterfall="")
            elif "w1" in results:
                observations = observations.exclude(waterfall="")
            if "a0" in results:
                observations = observations.filter(archived=False, payload="")
            elif "a1" in results:
                observations = observations.exclude(archived=False, payload="")
            if "d0" in results:
                observations = observations.filter(
                    demoddata__demodulated_data__isnull=True
                )
            elif "d1" in results:
                observations = observations.exclude(
                    demoddata__demodulated_data__isnull=True
                )
            if "i1" in results:
                observations = observations.filter(demoddata__is_image=True).distinct()
            elif "i0" in results:
                observations = observations.exclude(demoddata__is_image=True)

        if rated:
            if "rwu" in rated:
                observations = observations.filter(
                    waterfall_status__isnull=True
                ).exclude(waterfall="")
            elif "rw1" in rated:
                observations = observations.filter(waterfall_status=True)
            elif "rw0" in rated:
                observations = observations.filter(waterfall_status=False)
        self.count_vet = observations.count()

        return observations

    def get_context_data(self, **kwargs):  # pylint: disable=W0221
        """
        Need to add a list of satellites to the context for the template
        """
        context = super().get_context_data(**kwargs)
        context["count_vet"] = self.count_vet
        context["satellites"] = Satellite.objects.all()
        context["authors"] = User.objects.all().order_by(
            "first_name", "last_name", "username"
        )
        context["stations"] = Station.objects.all().order_by("id")
        transmitter_uuid = self.request.GET.get("transmitter_uuid", None)
        context["future"] = self.request.GET.get("future", "1")
        context["bad"] = self.request.GET.get("bad", "1")
        context["good"] = self.request.GET.get("good", "1")
        context["unknown"] = self.request.GET.get("unknown", "1")
        context["failed"] = self.request.GET.get("failed", "1")
        context["results"] = self.request.GET.getlist("results")
        context["rated"] = self.request.GET.getlist("rated")
        context["transmitter_mode"] = self.request.GET.get("transmitter_mode", None)
        context["transmitter_uuids_info"] = Transmitter.objects.all().prefetch_related(
            "satellite"
        )
        context["page"] = "observations"
        context["more_filtered"] = bool(self.more_filtered)
        norad_cat_id = self.request.GET.get("norad", None)
        observer = self.request.GET.get("observer", None)
        station = self.request.GET.get("station", None)
        start = (
            get_two_days_ago()
            if self.start_autoset
            else self.request.GET.get("start", None)
        )
        end = self.request.GET.get("end", None)
        if norad_cat_id is not None and norad_cat_id != "":
            context["norad"] = int(norad_cat_id)
        if observer is not None and observer != "":
            context["observer_id"] = int(observer)
        if station is not None and station != "":
            context["station_id"] = int(station)
        if start is not None and start != "":
            context["start"] = start
        if end is not None and end != "":
            context["end"] = end
        if transmitter_uuid:
            context["transmitters_uuid"] = transmitter_uuid

        if "scheduled" in self.request.session:
            context["scheduled"] = self.request.session["scheduled"]
            try:
                del self.request.session["scheduled"]
            except KeyError:
                pass
        context["can_schedule"] = schedule_perms(self.request.user)
        url_query = urlparse(self.request.build_absolute_uri()).query
        if not url_query:
            vet_url_query = "&start=" + (now() - timedelta(days=2)).strftime(
                "%Y-%m-%d+%H:%M"
            )
        else:
            vet_url_query = url_query
        #     if vet_url_query == url_query:  # no 'results' parameter was given
        #         vet_url_query += "&results=w1"
        context["vet_url_query"] = vet_url_query
        return context


class ObservationListView(ObservationListBaseView):  # pylint: disable=R0901
    """
    Displays a list of observations with pagination
    """

    def get_queryset(self):
        observations = super().get_queryset()
        if (obs_count := observations.count()) > settings.OBSERVATION_MAX_QUERY_COUNT:
            observations = observations[: settings.OBSERVATION_MAX_QUERY_COUNT]
            messages.error(
                self.request,
                _("Слишком широкий поиск, игнорируется ")
                + str(obs_count - settings.OBSERVATION_MAX_QUERY_COUNT)
                + _(
                    " наблюдения. Пожалуйста, измените фильтры ниже, чтобы сузить результаты поиска."
                ),
            )
        return observations


def get_observation_demoddata_details(observation: Observation, demoddata: DemodData):
    """
    Returns details about the Demoddata of the observation

    Args:
        observation(Observation): Instance of Observation Model
        demoddata(DemodData): Instance of Demoddata model
    Return:
        turple: turple with detail about observations demoddata
    """
    demoddata_details = []
    demoddata_images = []
    show_hex_to_ascii_button = False
    demoddata_count = len(demoddata)
    images_count = 0

    if demoddata_count:
        if observation.transmitter.downlink_mode == "CW":
            content_type = "text"
        else:
            content_type = "binary"
        for datum in demoddata:
            if datum.is_image:
                if datum.demodulated_data:
                    demoddata_images.append(
                        {
                            "url": datum.demodulated_data.url,
                            "name": datum.demodulated_data.name,
                            "type": "image",
                        }
                    )
                    demoddata_count -= 1
                    images_count += 1

            else:
                show_hex_to_ascii_button = True
                if datum.demodulated_data:
                    demoddata_details.append(
                        {
                            "url": datum.demodulated_data.url,
                            "name": datum.demodulated_data.name,
                            "type": content_type,
                            "id": datum.id,
                            "decoded": True if datum.payload_json != "" else False,
                        }
                    )
        demoddata_details = sorted(demoddata_details, key=lambda d: d["name"])
    return (
        demoddata_details,
        show_hex_to_ascii_button,
        demoddata_count,
        demoddata_images,
        images_count,
    )


class VetObservationsChunkListView(ListView):  # pylint: disable=R0901
    """
    View for getting the observations to vet as HTML snippets
    """

    vet_dict = {}

    def get_queryset(self):
        ids = [int(obs_id) for obs_id in self.request.GET.get("obs_ids").split(",")]
        can_vet_list = self.request.GET.get("can_vet", None).split(",")

        ids_len = len(ids)
        len_vet = len(can_vet_list)
        for i in range(len(ids)):
            self.vet_dict[ids[i]] = eval(can_vet_list[i].title())
        return (
            Observation.objects.filter(id__in=ids)
            .annotate(demoddata_count=Count("demoddata"))
            .order_by("-start", "-end")
        )

    def get(self, request, *args, **kwargs):
        observations = self.get_queryset()
        obs_html = []
        for obs in observations:
            demoddata_details = get_observation_demoddata_details(
                obs, obs.demoddata.all()
            )
            context = {
                "observation": obs,
                "from_vetting": True,
                "can_vet": self.vet_dict[obs.id],
                "demoddata_details": demoddata_details[0],
                "show_hex_to_ascii_button": demoddata_details[1],
                "demoddata_count": demoddata_details[2],
                "demoddata_images": demoddata_details[3],
                "image_count": demoddata_details[4],
            }
            context.update(csrf(request))

            rendered = render_to_string("includes/observation_detail.html", context)
            obs_html.append(rendered)
        return JsonResponse(obs_html, safe=False)


class VetObservationsView(ObservationListBaseView):  # pylint: disable=R0901
    """
    View for vetting multiple observations
    """

    object_list = []

    @property
    def template_name(self):
        return "base/vet_observation_container.html"

    def get_queryset(self):
        """Limits the queryset to those observations that the user can vet.
        works, similar to 'vet_perms' function.
        """
        queryset = super().get_queryset()
        if not self.request.user.is_authenticated:
            # can only watch
            queryset = queryset.annotate(can_vet=Value(False))
        elif not (
            self.request.user.is_superuser
            or self.request.user.groups.filter(name="Moderators").exists()
            or self.request.user.has_perm("base.can_vet")
            or self.request.user.ground_stations.filter(status=2).exists()
        ):
            # can vet only his station
            queryset = queryset.prefetch_related("author", "groud_station").annotate(
                can_vet=Case(
                    When(
                        Q(author=self.request.user)
                        | Q(
                            ground_station__isnull=False,
                            ground_station__owner=self.request.user,
                        ),
                        then=True,
                    ),
                    default=False,
                )
            )
        else:
            queryset = queryset.annotate(can_vet=Value(True))
        if (obs_count := queryset.count()) > settings.OBSERVATION_MAX_QUERY_COUNT:
            queryset = queryset[: settings.OBSERVATION_MAX_QUERY_COUNT]
            parsed_url = urlparse(self.request.build_absolute_uri())
            observation_search_url = (
                reverse("base:observations_list") + "?" + parsed_url.query
            )
            messages.error(
                self.request,
                _("Слишком широкий поиск, игнорируется ")
                + str(obs_count - settings.OBSERVATION_MAX_QUERY_COUNT)
                + _(" наблюдений. Пожалуйста, измените фильтры")
                + '<a href="'
                + observation_search_url
                + '">observation page</a>'
                + _(" чтобы сузить результаты поиска."),
            )
        return queryset

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        ids = queryset.values_list("id", flat=True)
        can_vet = queryset.values_list("can_vet", flat=True)
        context = {}
        parsed_url = urlparse(request.build_absolute_uri())
        context["query_url"] = reverse("base:vet_observations_chunks")
        context["obs_ids"] = list(ids)
        context["observation_search_url"] = (
            reverse("base:observations_list") + "?" + parsed_url.query
        )
        context["page_size"] = settings.VET_ITEMS_PER_CHUNK
        context["chunks_buffer_headstart"] = settings.CHUNKS_BUFFER_HEADSTART
        context["fwd_buffer_chunks"] = settings.FWD_BUFFER_CHUNKS
        context["bwd_buffer_chunks"] = settings.BWD_BUFFER_CHUNKS
        context["total_items"] = queryset.count()
        context["can_vet"] = list(can_vet)
        return self.render_to_response(context)


def observation_view(request, observation_id: int):
    """
    View for single observation page

    Args:
        observation_id(int): ID of observation
    """
    observation = get_object_or_404(Observation, id=observation_id)
    can_vet = vet_perms(request.user, observation)
    can_delete = delete_perms(request.user, observation)

    if observation.has_audio and not observation.audio_url:
        messages.error(
            request,
            _(
                "Аудиофайл в настоящее время недоступен,"
                " если проблема не исчезнет, ​​пожалуйста, свяжитесь с администратором.",
            ),
        )

    has_comments = False
    discuss_url = ""
    discuss_slug = ""
    # ! В будущем необходимо поменять весь кусок, когда введём свой форум
    # if settings.ENVIRONMENT == "production":
    #     discussion_details = community_get_discussion_details(
    #         observation.id,
    #         observation.satellite.name,
    #         observation.satellite.norad_cat_id,
    #         "https:%2F%2F{}{}".format(request.get_host(), request.path),
    #     )
    #     has_comments = discussion_details["has_comments"]
    #     discuss_url = discussion_details["url"]
    #     discuss_slug = discussion_details["slug"]

    demoddata = observation.demoddata.all()
    (
        demoddata_details,
        show_hex_to_ascii_button,
        demoddata_count,
        demoddata_images,
        image_count,
    ) = get_observation_demoddata_details(observation, demoddata)

    data = {
        "observation": observation,
        "demoddata_count": demoddata_count,
        "demoddata_details": demoddata_details,
        "show_hex_to_ascii_button": show_hex_to_ascii_button,
        "demoddata_images": demoddata_images,
        "image_count": image_count,
        "can_vet": can_vet,
        "can_delete": can_delete,
        "has_comments": has_comments,
        "discuss_url": discuss_url,
        "discuss_slug": discuss_slug,
        "tle_datetime": calculate_datetime_from_tle(observation.id),
    }
    return render(request, "base/observation_view.html", data)


@login_required
def observation_delete(request, observation_id: int):
    """
    View for deleting single observation

    Args:
        observation_id(int): ID of observation which deleting
    """
    observation = get_object_or_404(Observation, id=observation_id)
    can_delete = delete_perms(request.user, observation)
    if can_delete:
        observation.delete()
        messages.success(request, _("Наблюдение успешно удалено."))
    else:
        messages.error(request, _("Доступ запрещен."))
    return redirect(reverse("base:observations_list"))


@login_required
def observation_delete_several(request):
    """
    View for deleting several observations
    """
    all_id = request.POST.getlist("id[]")
    success_obs, bad_obs = [], []
    for obs_id in all_id:
        observation = get_object_or_404(Observation, id=obs_id)
        can_delete = delete_perms(request.user, observation)
        if observation and can_delete:
            try:
                observation.delete()
                success_obs.append(obs_id)
            except AttributeError:
                pass
        else:
            bad_obs.append(obs_id)
    return JsonResponse(
        {
            "success": ",".join(str(e) for e in success_obs),
            "bad": ",".join(str(e) for e in bad_obs),
        }
    )


@login_required
@ajax_required
def waterfall_vet(request, observation_id: int):
    """
    Handles request for vetting a waterfall

    Args:
        observation_id(int): ID of observation which vetting

    TODO проставить коды JsonResponc-ам, все возвращают 200
    """
    try:
        observation = Observation.objects.get(id=observation_id)
    except Observation.DoesNotExist:
        data = {"error": _("Наблюдение не существует.")}
        return JsonResponse(data, safe=False)

    status = request.POST.get("status", None)
    can_vet = vet_perms(request.user, observation)

    if not can_vet:
        data = {"error": _("Доступ запрещен.")}
        return JsonResponse(data, safe=False)
    if not observation.has_waterfall:
        data = {"error": _("Наблюдение без водопада.")}
        return JsonResponse(data, safe=False)

    if status not in ["with-signal", "without-signal", "unknown"]:
        data = {
            "error": _(
                "Неверный статус, выберите один из вариантов: «с сигналом», «без сигнала» и «неизвестный»."
            )
        }
        return JsonResponse(data, safe=False)

    if status == "with-signal":
        observation.waterfall_status = True
    elif status == "without-signal":
        observation.waterfall_status = False
    elif status == "unknown":
        observation.waterfall_status = None

    observation.waterfall_status_user = request.user
    observation.waterfall_status_datetime = now()
    observation.save(
        update_fields=[
            "waterfall_status",
            "waterfall_status_user",
            "waterfall_status_datetime",
        ]
    )
    (observation_status, observation_status_badge, observation_status_display) = (
        rate_observation(
            observation.id, "set_waterfall_status", observation.waterfall_status
        )
    )
    data = {
        "waterfall_status_user": observation.waterfall_status_user.displayname,
        "waterfall_status_datetime": observation.waterfall_status_datetime.strftime(
            "%Y-%m-%d %H:%M:%S"
        ),
        "waterfall_status": observation.waterfall_status,
        "waterfall_status_badge": observation.waterfall_status_badge,
        "waterfall_status_display": observation.waterfall_status_display,
        "status": observation_status,
        "status_badge": observation_status_badge,
        "status_display": observation_status_display,
    }
    return JsonResponse(data, safe=False)


def calculate_datetime_from_tle(observation_id):
    """
    Converts TLE epoch to datetime object.
    :returns: timezone-aware datetime or None: The epoch for the given TLE
    """
    observation = get_object_or_404(
        Observation.objects.select_related("tle"), id=observation_id
    )

    try:
        # Load the TLE data to create a Skyfield EarthSatellite object
        satellite = skyfield.api.EarthSatellite(
            observation.tle.tle1, observation.tle.tle2, observation.tle.tle0
        )
        # Get the TLE epoch time as a Skyfield Time object
        epoch_time = satellite.epoch
        # Convert the epoch time to a timezone-aware datetime object
        tle_datetime = epoch_time.utc_datetime()
    except (ValueError, AttributeError):
        tle_datetime = None
    return tle_datetime
