"""Django base views for SONIKS Network"""

from datetime import timedelta

from django.db import models
from django.db.models import Count, F, Max, Q
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.timezone import now

from network.base.decorators import ajax_required
from network.base.models import (
    DemodData,
    Mode,
    Observation,
    Satellite,
    Station,
    Transmitter,
)
from network.base.utils import calculate_band_stats


def statistics_view(request):
    """
    Statistics view page
    """
    satellites = Satellite.objects.all().prefetch_related(
        "created_by", "operator", "network"
    )
    total_satellites = satellites.count()
    alive_satellites = satellites.filter(status="alive").count()
    transmitters = Transmitter.objects.all().prefetch_related(
        "uplink_mode", "downlink_mode", "satellite"
    )
    total_transmitters = transmitters.count()
    alive_transmitters = transmitters.filter(status="active").count()
    stations = Station.objects.all().prefetch_related("owner")
    total_stations = stations.count()
    active_stations = stations.filter(status=2).count()
    data = (
        DemodData.objects.all()
        .prefetch_related("observation", "satellite", "transmitter")
        .aggregate(
            total_sonik_data=Count("pk", filter=Q(sonik_data=True)),
            decoded_sonik_data=Count(
                "pk",
                filter=~Q(payload_json="") & Q(sonik_data=True),
            ),
            total_data=Count("pk"),
            decoded_data=Count("pk", filter=~Q(payload_json="")),
        )
    )

    context = {
        "total_satellites": total_satellites,
        "alive_satellites": alive_satellites,
        "total_transmitters": total_transmitters,
        "alive_transmitters": alive_transmitters,
        "total_stations": total_stations,
        "active_stations": active_stations,
        "total_sonik_data": data["total_sonik_data"],
        "decoded_sonik_data": data["decoded_sonik_data"],
        "total_data": data["total_data"],
        "decoded_data": data["decoded_data"],
        "page": "statistics",
    }

    return render(request, "base/statistics.html", context)


@ajax_required
def statistics_modes(request):
    """
    Mode statistics
    """
    modes = (
        Mode.objects.annotate(count=Count("transmitter_downlink_mode"))
        .order_by("-count")
        .values("name", "count")
    )
    transmitters = Transmitter.objects.all().prefetch_related(
        "uplink_mode", "downlink_mode", "satellite"
    )
    band_label, band_count = calculate_band_stats(transmitters)

    data = {
        "modes": list(modes),
        "band_label": band_label,
        "band_count": band_count,
    }

    return JsonResponse(data, safe=False)


@ajax_required
def top_satellites(request):
    """
    Top satellites statistics
    """
    top_sat_total = (
        Satellite.objects.annotate(count=Count("telemetry_data"))
        .order_by("-count")
        .values("name", "count", "sat_id")
    )
    top_sat_total_d = (
        Satellite.objects.annotate(
            count=Count(
                "telemetry_data",
                filter=Q(telemetry_data__timestamp__gt=now() - timedelta(days=1)),
            )
        )
        .order_by("-count")
        .values("name", "count", "sat_id")
    )

    top_sat_total_w = (
        Satellite.objects.annotate(
            count=Count(
                "telemetry_data",
                filter=Q(telemetry_data__timestamp__gt=now() - timedelta(days=7)),
            )
        )
        .order_by("-count")
        .values("name", "count", "sat_id")
    )
    top_sat_total_m = (
        Satellite.objects.annotate(
            count=Count(
                "telemetry_data",
                filter=Q(telemetry_data__timestamp__gt=now() - timedelta(days=30)),
            )
        )
        .order_by("-count")
        .values("name", "count", "sat_id")
    )
    top_sat_sonik = (
        Satellite.objects.annotate(
            count=Count("telemetry_data", filter=Q(telemetry_data__sonik_data=True))
        )
        .order_by("-count")
        .values("name", "count", "sat_id")
    )
    top_sat_sonik_d = (
        Satellite.objects.annotate(
            count=Count(
                "telemetry_data",
                filter=(
                    Q(telemetry_data__sonik_data=True)
                    & Q(telemetry_data__timestamp__gt=now() - timedelta(days=1))
                ),
            )
        )
        .order_by("-count")
        .values("name", "count", "sat_id")
    )
    top_sat_sonik_w = (
        Satellite.objects.annotate(
            count=Count(
                "telemetry_data",
                filter=(
                    Q(telemetry_data__sonik_data=True)
                    & Q(telemetry_data__timestamp__gt=now() - timedelta(days=7))
                ),
            )
        )
        .order_by("-count")
        .values("name", "count", "sat_id")
    )
    top_sat_sonik_m = (
        Satellite.objects.annotate(
            count=Count(
                "telemetry_data",
                filter=(
                    Q(telemetry_data__sonik_data=True)
                    & Q(telemetry_data__timestamp__gt=now() - timedelta(days=30))
                ),
            )
        )
        .order_by("-count")
        .values("name", "count", "sat_id")
    )

    data = {
        "top_sat_sonik": list(top_sat_sonik)[0:10],
        "top_sat_sonik_d": list(top_sat_sonik_d)[0:10],
        "top_sat_sonik_w": list(top_sat_sonik_w)[0:10],
        "top_sat_sonik_m": list(top_sat_sonik_m)[0:10],
        "top_sat_total": list(top_sat_total)[0:10],
        "top_sat_total_d": list(top_sat_total_d)[0:10],
        "top_sat_total_w": list(top_sat_total_w)[0:10],
        "top_sat_total_m": list(top_sat_total_m)[0:10],
    }

    return JsonResponse(data, safe=False)


@ajax_required
def top_stations(request):
    """
    Top stations statistics
    """
    top_station_sonik = (
        DemodData.objects.filter(Q(station__isnull=False) & Q(sonik_data=True))
        .values("station__name", "station__id")
        .annotate(
            count=Count("station__name"), name=F("station__name"), id=F("station__id")
        )
        .values("name", "id", "count")
        .order_by("-count")
    )

    top_station_sonik_d = (
        DemodData.objects.filter(
            Q(station__isnull=False)
            & Q(sonik_data=True)
            & Q(timestamp__gt=now() - timedelta(days=1))
        )
        .values("station__name", "station__id")
        .annotate(
            count=Count("station__name"), name=F("station__name"), id=F("station__id")
        )
        .values("name", "id", "count")
        .order_by("-count")
    )

    top_station_sonik_w = (
        DemodData.objects.filter(
            Q(station__isnull=False)
            & Q(sonik_data=True)
            & Q(timestamp__gt=now() - timedelta(days=7))
        )
        .values("station__name", "station__id")
        .annotate(
            count=Count("station__name"), name=F("station__name"), id=F("station__id")
        )
        .values("name", "id", "count")
        .order_by("-count")
    )

    top_station_sonik_m = (
        DemodData.objects.filter(
            Q(station__isnull=False)
            & Q(sonik_data=True)
            & Q(timestamp__gt=now() - timedelta(days=30))
        )
        .values("station__name", "station__id")
        .annotate(
            count=Count("station__name"), name=F("station__name"), id=F("station__id")
        )
        .values("name", "id", "count")
        .order_by("-count")
    )

    top_station_not_sonik = (
        DemodData.objects.filter(Q(station__isnull=True) & Q(sonik_data=False))
        .values("observer")
        .annotate(count=Count("observer"))
        .order_by("-count")
    )

    top_station_not_sonik_d = (
        DemodData.objects.filter(
            Q(station__isnull=True)
            & Q(sonik_data=False)
            & Q(timestamp__gt=now() - timedelta(days=1))
        )
        .values("observer")
        .annotate(count=Count("observer"))
        .order_by("-count")
    )

    top_station_not_sonik_w = (
        DemodData.objects.filter(
            Q(station__isnull=True)
            & Q(sonik_data=False)
            & Q(timestamp__gt=now() - timedelta(days=7))
        )
        .values("observer")
        .annotate(count=Count("observer"))
        .order_by("-count")
    )

    top_station_not_sonik_m = (
        DemodData.objects.filter(
            Q(station__isnull=True)
            & Q(sonik_data=False)
            & Q(timestamp__gt=now() - timedelta(days=30))
        )
        .values("observer")
        .annotate(count=Count("observer"))
        .order_by("-count")
    )

    data = {
        "top_station_sonik": list(top_station_sonik)[0:10],
        "top_station_sonik_d": list(top_station_sonik_d)[0:10],
        "top_station_sonik_w": list(top_station_sonik_w)[0:10],
        "top_station_sonik_m": list(top_station_sonik_m)[0:10],
        "top_station_not_sonik": list(top_station_not_sonik)[0:10],
        "top_station_not_sonik_d": list(top_station_not_sonik_d)[0:10],
        "top_station_not_sonik_w": list(top_station_not_sonik_w)[0:10],
        "top_station_not_sonik_m": list(top_station_not_sonik_m)[0:10],
    }

    return JsonResponse(data, safe=False)


def satellite_statistics(request):
    """
    Satellite statistics page view
    """
    filter_data = {}
    filter_obs = {}
    sonik_param = None
    if request.GET.get("sonik", "") != "0" and request.GET.get("all", "") != "1":
        filter_data["sonik_data"] = True
        sonik_param = True

    start = request.GET.get("start", "")
    if start != "":
        filter_data["timestamp__gte"] = start
        filter_obs["start__gte"] = start

    end = request.GET.get("end", "")
    if end != "":
        filter_data["timestamp__lte"] = end
        filter_obs["end__lte"] = end

    demoddata_agg = DemodData.objects.values("satellite_id").annotate(
        total_data=Count("id", filter=Q(**filter_data)),
        decoded_data=Count("id", filter=Q(**filter_data) & ~Q(payload_json="")),
        latest_data=Max("timestamp", filter=Q(**filter_data)),
    )

    observation_agg = Observation.objects.values("satellite_id").annotate(
        tootal_obs=Count("id", filter=Q(**filter_obs)),
        success_obs=Count("id", filter=Q(status__gte=100) & Q(**filter_obs)),
        unknown_obs=Count(
            "id", filter=Q(status__gte=0) & Q(status__lt=100) & Q(**filter_obs)
        ),
        bad_obs=Count(
            "id", filter=Q(status__gte=-100) & Q(status__lt=0) & Q(**filter_obs)
        ),
        future_obs=Count("id", filter=Q(start__gte=now())),
    )

    satellites = Satellite.objects.values("name", "norad_cat_id").annotate(
        total_data=models.Subquery(
            demoddata_agg.filter(satellite_id=models.OuterRef("id")).values(
                "total_data"
            )[:1]
        ),
        decoded_data=models.Subquery(
            demoddata_agg.filter(satellite_id=models.OuterRef("id")).values(
                "decoded_data"
            )[:1]
        ),
        latest_data=models.Subquery(
            demoddata_agg.filter(satellite_id=models.OuterRef("id")).values(
                "latest_data"
            )[:1]
        ),
        tootal_obs=models.Subquery(
            observation_agg.filter(satellite_id=models.OuterRef("id")).values(
                "tootal_obs"
            )[:1]
        ),
        success_obs=models.Subquery(
            observation_agg.filter(satellite_id=models.OuterRef("id")).values(
                "success_obs"
            )[:1]
        ),
        unknown_obs=models.Subquery(
            observation_agg.filter(satellite_id=models.OuterRef("id")).values(
                "unknown_obs"
            )[:1]
        ),
        bad_obs=models.Subquery(
            observation_agg.filter(satellite_id=models.OuterRef("id")).values(
                "bad_obs"
            )[:1]
        ),
        future_obs=models.Subquery(
            observation_agg.filter(satellite_id=models.OuterRef("id")).values(
                "future_obs"
            )[:1]
        ),
    )

    return render(
        request,
        "base/satellite_statistics.html",
        {
            "satellites": satellites,
            "sonik_param": sonik_param,
            "period": request.GET.get("period", None),
            "start": request.GET.get("start", None),
            "end": request.GET.get("end", None),
            "columns": get_display_columns(request, "satellites"),
        },
    )


def station_statistics(request):
    """
    Station statistics page view
    """
    filter_data = {}
    filter_obs = {}
    start_worktime = None
    end_worktime = None
    start = request.GET.get("start", "")
    end = request.GET.get("end", "")
    if start != "":
        filter_data["timestamp__gte"] = start
        filter_obs["start__gte"] = start
        start_worktime = start
    if end != "":
        filter_data["timestamp__lte"] = end
        filter_obs["end__lte"] = end
        end_worktime = end
    demoddata_agg = (
        DemodData.objects.filter(station__isnull=False)
        .values("station_id")
        .annotate(
            total_data=Count("id", filter=Q(**filter_data)),
            decoded_data=Count("id", filter=~Q(payload_json="") & Q(**filter_data)),
            latest_data=Max("timestamp", filter=Q(**filter_data)),
        )
    )

    observation_agg = Observation.objects.values("ground_station_id").annotate(
        tootal_obs=Count("id", filter=Q(**filter_obs)),
        success_obs=Count("id", filter=Q(status__gte=100) & Q(**filter_obs)),
        unknown_obs=Count(
            "id", filter=Q(status__gte=0) & Q(status__lt=100) & Q(**filter_obs)
        ),
        bad_obs=Count(
            "id", filter=Q(status__gte=-100) & Q(status__lt=0) & Q(**filter_obs)
        ),
        future_obs=Count("id", filter=Q(start__gte=now())),
    )

    stations = Station.objects.values("id", "name").annotate(
        total_data=models.Subquery(
            demoddata_agg.filter(station_id=models.OuterRef("id")).values("total_data")[
                :1
            ]
        ),
        decoded_data=models.Subquery(
            demoddata_agg.filter(station_id=models.OuterRef("id")).values(
                "decoded_data"
            )[:1]
        ),
        latest_data=models.Subquery(
            demoddata_agg.filter(station_id=models.OuterRef("id")).values(
                "latest_data"
            )[:1]
        ),
        tootal_obs=models.Subquery(
            observation_agg.filter(ground_station_id=models.OuterRef("id")).values(
                "tootal_obs"
            )[:1]
        ),
        success_obs=models.Subquery(
            observation_agg.filter(ground_station_id=models.OuterRef("id")).values(
                "success_obs"
            )[:1]
        ),
        unknown_obs=models.Subquery(
            observation_agg.filter(ground_station_id=models.OuterRef("id")).values(
                "unknown_obs"
            )[:1]
        ),
        bad_obs=models.Subquery(
            observation_agg.filter(ground_station_id=models.OuterRef("id")).values(
                "bad_obs"
            )[:1]
        ),
        future_obs=models.Subquery(
            observation_agg.filter(ground_station_id=models.OuterRef("id")).values(
                "future_obs"
            )[:1]
        ),
    )

    return render(
        request,
        "base/station_statistics.html",
        {
            "stations": stations,
            "period": request.GET.get("period", None),
            "start": request.GET.get("start", None),
            "end": request.GET.get("end", None),
            "columns": get_display_columns(request, "stations"),
            "start_worktime": start_worktime,
            "end_worktime": end_worktime,
        },
    )


def get_display_columns(request, obj):
    """
    Caching displayed table columns
    """
    columns_list = []
    if obj == "satellites":
        columns_list = [
            "Stat_sat_Norad",
            "Stat_sat_Name",
            "Stat_sat_Data",
            "Stat_sat_Decoded",
            "Stat_sat_Latest",
            "Stat_sat_Obs_total",
            "Stat_sat_Obs_success",
            "Stat_sat_Obs_unk",
            "Stat_sat_Obs_bad",
            "Stat_sat_Obs_future",
        ]
    if obj == "stations":
        columns_list = [
            "Stat_stat_Name",
            "Stat_stat_Data",
            "Stat_stat_Latest",
            "Stat_sat_Obs_total",
            "Stat_sat_Obs_success",
            "Stat_sat_Obs_unk",
            "Stat_sat_Obs_bad",
            "Stat_sat_Obs_future",
            "Stat_stat_worktime",
        ]

    columns_display = {}
    for column in columns_list:
        columns_display[column] = request.COOKIES.get(column)
    return columns_display
