"""Django base views for SONIKS Network"""

from typing import List, Optional

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.db import DatabaseError, transaction
from django.db.models import Count, Q
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView

from network.api.serializers import SatelliteSerializer
from network.base.decorators import ajax_required
from network.base.forms import (
    AntennaInlineFormSet,
    FrequencyRangeInlineFormSet,
    StationForm,
    StationRegistrationForm,
)
from network.base.models import (
    AntennaType,
    Observation,
    Satellite,
    Station,
    StationStatusLog,
    Transmitter,
)
from network.base.perms import modify_delete_station_perms, schedule_station_perms
from network.base.serializers import StationSerializer
from network.base.utils import (
    format_frequency,
    get_dict_from_list,
    populate_formset_error_messages,
)


@ajax_required
def station_all_view(request):
    """
    Return JSON with all stations
    """
    stations = Station.objects.all()
    data = StationSerializer(stations, many=True).data
    return JsonResponse(data, safe=False)


class StationListView(ListView):  # pylint: disable=R0901
    """
    Displays a list of stations with pagination
    """

    model = Station
    context_object_name = "stations"
    flag_filters = ["online", "testing", "offline", "future"]
    is_filtered = False
    freq_filter_errors: List[str] = []

    @property
    def template_name(self):
        """Defining a template depending on the language"""
        # TODO устарело
        return "base/stations.html"

    def get_filter_params(self):
        """
        Get the parsed filter parameters from the HTTP GET parameters
        """

        filter_params = {}
        for param_name in self.flag_filters:
            param_val = self.request.GET.get(param_name, 1)
            if param_val != "0":
                filter_params[param_name] = True
            else:
                filter_params[param_name] = False
                self.is_filtered = True
        self.freq_filter_errors = []
        filter_freq = self.request.GET.get("freq", None)
        if filter_freq:
            self.is_filtered = True
            try:
                filter_freq = int(float(filter_freq))
            except ValueError:
                self.freq_filter_errors.append(_("Частота: неверное значение"))
                return filter_params
            if filter_freq < 0:
                self.freq_filter_errors.append(
                    _("Частота: значение не может быть меньше 0")
                )
            filter_params["freq"] = filter_freq

        filter_station = self.request.GET.get("station", None)
        if filter_station:
            self.is_filtered = True
            filter_params["station_id"] = filter_station

        filter_antenna = self.request.GET.get("antenna", None)
        if filter_antenna:
            self.is_filtered = True
            filter_params["antenna_type"] = filter_antenna

        return filter_params

    def get_queryset(self):
        stations = (
            Station.objects.annotate(
                total_obs=Count("observations"),
                future_obs=Count("pk", filter=Q(observations__end__gt=now())),
            )
            .select_related("owner")
            .prefetch_related(
                "antennas", "antennas__antenna_type", "antennas__frequency_ranges"
            )
            .order_by("-status", "id")
        )

        filter_params = self.get_filter_params()

        station_id = filter_params.get("station_id", None)
        if station_id:
            stations = stations.filter(id=station_id)

        if not filter_params["online"]:
            stations = stations.exclude(status=2)
        if not filter_params["testing"]:
            stations = stations.exclude(status=1)
        if not filter_params["offline"]:
            stations = stations.exclude(Q(status=0) & Q(last_seen__isnull=False))
        if not filter_params["future"]:
            stations = stations.exclude(last_seen__isnull=True)

        freq = filter_params.get("freq", None)
        if freq:
            freq_filter = Q(antennas__frequency_ranges__min_frequency__lte=freq)
            freq_filter &= Q(antennas__frequency_ranges__max_frequency__gte=freq)
            stations = stations.filter(freq_filter)

        antenna_type = filter_params.get("antenna_type", None)
        if antenna_type:
            stations = stations.filter(antennas__antenna_type__id=antenna_type)

        stations = sorted(stations, key=lambda x: x.status_sort, reverse=True)
        return stations

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.get_filter_params())
        context["page"] = "stations"
        context["is_filtered"] = self.is_filtered
        context["freq_filter_errors"] = self.freq_filter_errors
        context["antennas_all"] = AntennaType.objects.all()
        context["stations_all"] = Station.objects.all()
        station = self.request.GET.get("station", None)
        if station is not None and station != "":
            context["station_id"] = int(station)
        antenna = self.request.GET.get("antenna", None)
        if antenna is not None and antenna != "":
            context["antenna_id"] = int(antenna)

        return context


def station_view(request, station_id: int, popup: bool = False):  # pylint: disable=R0915
    """
    View for single station page

    Args:
        station_id(int): ID of station which view we need
        popul(bool): If this view is popup. Default is false
    Returns:
        dict|HttpResponce: Dict for popup or HTTPResponce for view
    """

    station = get_object_or_404(
        Station.objects.annotate(
            total_obs=Count("observations"),
            good_obs=Count("pk", filter=Q(observations__status__gte=100)),
            unknown_obs=Count(
                "pk",
                filter=(
                    Q(observations__status__gte=0) & Q(observations__status__lt=100)
                ),
            ),
            bad_obs=Count(
                "pk",
                filter=(
                    Q(observations__status__gte=-100) & Q(observations__status__lt=0)
                ),
            ),
            future_obs=Count("pk", filter=Q(observations__end__gt=now())),
        ).prefetch_related(
            "owner",
            "antennas",
            "antennas__antenna_type",
            "antennas__frequency_ranges",
            "stat",
        ),
        id=station_id,
    )
    station_log = StationStatusLog.objects.filter(station=station)

    can_schedule = schedule_station_perms(request.user, station)
    can_modify_delete_station = modify_delete_station_perms(request.user, station)
    primary_schedule: List[dict] = []
    second_schedule: List[dict] = []

    if request.user.is_authenticated:
        if request.user == station.owner:
            if station.is_offline:
                mes = _(
                    "Ваша станция отключена. Убедитесь, что она может "
                    "подключиться по API к сети."
                )

                messages.error(request, mes)
            if station.is_testing:
                mes = _(
                    "Ваша станция в тестовом режиме. Как только вы убедитесь,"
                    "что она возвращает хорошие наблюдения, вы можете "
                    "включить ей онлайн-режим."
                )
                messages.warning(request, mes)
            if hasattr(station, "schedule"):
                primary_schedule = station.schedule.satellites
                primary_schedule_satellites = Satellite.objects.filter(
                    sat_id__in=[sat.get("sat_id", None) for sat in primary_schedule]
                ).values("id", "name", "norad_cat_id", "sat_id")
                primary_schedule_transmitters = Transmitter.objects.filter(
                    uuid__in=[
                        sat.get("transmitter_uuid", None) for sat in primary_schedule
                    ]
                ).values("uuid", "downlink_low", "downlink_mode__name", "type")
                for one_sat in primary_schedule:
                    one_sat_transmitter = get_dict_from_list(
                        primary_schedule_transmitters,
                        "uuid",
                        one_sat["transmitter_uuid"],
                    )

                    one_sat_satellite = get_dict_from_list(
                        primary_schedule_satellites, "sat_id", one_sat["sat_id"]
                    )
                    one_sat["norad_cat_id"] = one_sat_satellite["norad_cat_id"]
                    one_sat["transmitter_freq"] = format_frequency(
                        one_sat_transmitter["downlink_low"]
                    )
                    one_sat["transmitter_mode"] = one_sat_transmitter[
                        "downlink_mode__name"
                    ]
                primary_schedule = sorted(
                    primary_schedule, key=lambda satellite: int(satellite["prio"])
                )
            if hasattr(station, "schedulesec"):
                second_schedule = station.schedulesec.satellites

                second_schedule_satellites = Satellite.objects.filter(
                    sat_id__in=[sat.get("sat_id", None) for sat in second_schedule]
                ).values("id", "name", "norad_cat_id", "sat_id")
                second_schedule_transmitters = Transmitter.objects.filter(
                    uuid__in=[
                        sat.get("transmitter_uuid", None) for sat in second_schedule
                    ]
                ).values("uuid", "downlink_low", "downlink_mode__name", "type")
                for one_sat in second_schedule:
                    one_sat_transmitter = get_dict_from_list(
                        second_schedule_transmitters,
                        "uuid",
                        one_sat["transmitter_uuid"],
                    )

                    one_sat_satellite = get_dict_from_list(
                        second_schedule_satellites, "sat_id", one_sat["sat_id"]
                    )
                    one_sat["norad_cat_id"] = one_sat_satellite["norad_cat_id"]
                    one_sat["transmitter_freq"] = format_frequency(
                        one_sat_transmitter["downlink_low"]
                    )
                    one_sat["transmitter_mode"] = one_sat_transmitter[
                        "downlink_mode__name"
                    ]

    if popup:
        return {
            "station": station,
            "can_schedule": can_schedule,
            "can_modify_delete_station": can_modify_delete_station,
            "total_obs": station.total_obs,
            "good_obs": station.good_obs,
            "unknown_obs": station.unknown_obs,
            "bad_obs": station.bad_obs,
            "future_obs": station.future_obs,
        }

    data = {
        "station": station,
        "mapbox_id": settings.MAPBOX_MAP_ID,
        "mapbox_token": settings.MAPBOX_TOKEN,
        "can_schedule": can_schedule,
        "can_modify_delete_station": can_modify_delete_station,
        "station_log": station_log,
        "obs_min_duration": settings.OBSERVATION_DURATION_MIN,
        "split": {
            "duration": settings.OBSERVATION_SPLIT_DURATION,
            "break": settings.OBSERVATION_SPLIT_BREAK_DURATION,
        },
        "primary_schedule": primary_schedule,
        "second_schedule": second_schedule,
    }
    return render(request, "base/station_view.html", data)


@login_required
def station_delete(request, station_id: int):
    """
    View for deleting a station

    Args:
        station_id: ID of station which deleting

    Returns:
        HttpResponce: Redirect to user view
    """
    username = request.user
    station = get_object_or_404(Station, id=station_id, owner=request.user)
    station.delete()
    messages.success(request, _("Наземная станция успешно удалена."))
    return redirect(reverse("users:view_user", kwargs={"username": username}))


@login_required
def station_delete_future_observations(request, station_id):
    """View for deleting all future observations of a given station."""
    return redirect(reverse("base:station_view", kwargs={"station_id": station_id}))


@login_required
def station_register(
    request, step: Optional[str] = None, station_id: Optional[int] = None
):
    # pylint: disable=R0911
    """
    Station register view

    Args:
        step(str,optional): Current registration step
        station_id(int): ID of the registered station
    """
    client_hash = request.GET.get("hash", None)
    if client_hash:
        client_id = cache.get(client_hash)
        if client_id is None:
            messages.error(
                request,
                _(
                    "Неправильный или истекший хеш, пожалуйста начните заново процесс регистрации станции."
                ),
            )
            return redirect(reverse("base:home"))
    else:
        messages.error(request, _("Был пропущен параметр хеша."))
        return redirect(reverse("base:home"))
    if step == "1":
        stations = Station.objects.filter(owner=request.user)
        return render(
            request, "base/station_register_step1.html", {"stations": stations}
        )
    if step == "2":
        station = None
        if station_id:
            station = get_object_or_404(Station, id=station_id, owner=request.user)

        if request.method == "POST":
            if station:
                station_form = StationRegistrationForm(request.POST, instance=station)
            else:
                station_form = StationRegistrationForm(request.POST)
            if station_form.is_valid():
                station = station_form.save(commit=False)
                station.owner = request.user
                station.save()
                cache.delete(client_hash)
                messages.success(
                    request,
                    _(
                        "Станция с номером {0} была успешно зарегистрирована."
                        "Продолжите её конфигурирование."
                    ).format(station.id),
                )
                return redirect(
                    reverse("base:station_edit", kwargs={"station_id": station.id})
                )
            messages.error(request, str(station_form.errors))
        else:
            if station:
                station_form = StationRegistrationForm(instance=station)
            else:
                station_form = StationRegistrationForm()

        return render(
            request,
            "base/station_register_step2.html",
            {"station_form": station_form, "client_id": client_id},
        )

    return redirect(reverse("base:home"))


def initialize_station_and_registration_status(request, station_id: Optional[int]):
    """
    Return Station model and its registration status if station exists

    Args:
        station_id(int): ID of station which initializing

    Returns:
        turple: Return (Station instance and registered status(bool)) if station is registrated
            else (return None and False)
    """
    if station_id:
        station = get_object_or_404(
            Station.objects.prefetch_related(
                "antennas", "antennas__antenna_type", "antennas__frequency_ranges"
            ),
            id=station_id,
            owner=request.user,
        )
        registered = bool(station.client_id)
        return (station, registered)
    return (None, False)


@login_required
def station_edit(request, station_id=None):
    """Edit or add a single station
    Args:
        station_id(int): ID of station which editing
    """
    (station, registered) = initialize_station_and_registration_status(
        request, station_id
    )
    if request.method == "POST":
        return handle_station_edit_post(request, station, registered)
    return handle_station_edit_get(request, station, registered)


def handle_station_edit_get(request, station: Station, registered: bool):
    """
    Returns the form for creating or editing a station

    Args:
        station(Station): Instance of station. Can be None
        registered(bool): Status of station registration
    """
    antenna_types = AntennaType.objects.all()
    frequency_range_formsets = {}

    if station:
        station_form = StationForm(instance=station)
        antenna_formset = AntennaInlineFormSet(instance=station, prefix="ant")
        for antenna_form in antenna_formset.forms:
            antenna_prefix = antenna_form.prefix
            frequency_range_formsets[antenna_prefix] = FrequencyRangeInlineFormSet(
                instance=antenna_form.instance, prefix=antenna_prefix + "-fr"
            )
    else:
        station_form = StationForm()
        antenna_formset = AntennaInlineFormSet(prefix="ant")

    data = {
        "registered": registered,
        "station_form": station_form,
        "antenna_formset": antenna_formset,
        "frequency_range_formsets": frequency_range_formsets,
        "antenna_types": antenna_types,
        "max_antennas_per_station": settings.MAX_ANTENNAS_PER_STATION,
        "max_frequency_ranges_per_antenna": settings.MAX_FREQUENCY_RANGES_PER_ANTENNA,
        "max_frequency_for_range": settings.MAX_FREQUENCY_FOR_RANGE,
        "min_frequency_for_range": settings.MIN_FREQUENCY_FOR_RANGE,
        "vhf_min_frequency": settings.VHF_MIN_FREQUENCY,
        "vhf_max_frequency": settings.VHF_MAX_FREQUENCY,
        "uhf_min_frequency": settings.UHF_MIN_FREQUENCY,
        "uhf_max_frequency": settings.UHF_MAX_FREQUENCY,
        "l_min_frequency": settings.L_MIN_FREQUENCY,
        "l_max_frequency": settings.L_MAX_FREQUENCY,
        "s_min_frequency": settings.S_MIN_FREQUENCY,
        "s_max_frequency": settings.S_MAX_FREQUENCY,
    }
    return render(request, "base/station_edit.html", data)


def handle_station_edit_post(request, station: Station, registered: bool):
    """
    Handles the form submission for creating or editing a station

    Args:
        station(Station): Instance of station
        registered(bool): Status of station registration
    """
    antenna_types = AntennaType.objects.all()
    validation_successful = False
    transaction_successful = False

    if station:
        station_form = StationForm(request.POST, request.FILES, instance=station)
    else:
        station_form = StationForm(request.POST, request.FILES)

    antenna_formset = AntennaInlineFormSet(
        request.POST, instance=station_form.instance, prefix="ant"
    )
    frequency_range_formsets = {}

    for antenna_form in antenna_formset:
        if not antenna_form["DELETE"].value():
            prefix = antenna_form.prefix
            frequency_range_formsets[prefix] = FrequencyRangeInlineFormSet(
                request.POST, instance=antenna_form.instance, prefix=prefix + "-fr"
            )

    if station_form.is_valid():
        station = station_form.save(commit=False)
        station.owner = request.user
        if antenna_formset.is_valid():
            for frequency_range_formset in frequency_range_formsets:
                if not frequency_range_formsets[frequency_range_formset].is_valid():
                    populate_formset_error_messages(
                        messages,
                        request,
                        frequency_range_formsets[frequency_range_formset],
                    )
                    break
            else:
                validation_successful = True
        else:
            populate_formset_error_messages(messages, request, antenna_formset)
    else:
        messages.error(request, str(station_form.errors))

    if validation_successful:
        try:
            with transaction.atomic():
                station.save()
                antenna_formset.save()
                for frequency_range_formset in frequency_range_formsets:
                    frequency_range_formsets[frequency_range_formset].save()
                transaction_successful = True
        except DatabaseError:
            messages.error(
                request,
                _(
                    "Что-то пошло не так. Если проблема останется,"
                    "то, пожалуйста, свяжитесь с администратором.",
                ),
            )

    if transaction_successful:
        messages.success(
            request, _("Станция с номером {0} успешно сохранена.").format(station.id)
        )
        return redirect(reverse("base:station_view", kwargs={"station_id": station.id}))
    data = {
        "registered": registered,
        "station_form": station_form,
        "antenna_formset": antenna_formset,
        "frequency_range_formsets": frequency_range_formsets,
        "antenna_types": antenna_types,
        "max_antennas_per_station": settings.MAX_ANTENNAS_PER_STATION,
        "max_frequency_ranges_per_antenna": settings.MAX_FREQUENCY_RANGES_PER_ANTENNA,
        "max_frequency_for_range": settings.MAX_FREQUENCY_FOR_RANGE,
        "min_frequency_for_range": settings.MIN_FREQUENCY_FOR_RANGE,
        "vhf_min_frequency": settings.VHF_MIN_FREQUENCY,
        "vhf_max_frequency": settings.VHF_MAX_FREQUENCY,
        "uhf_min_frequency": settings.UHF_MIN_FREQUENCY,
        "uhf_max_frequency": settings.UHF_MAX_FREQUENCY,
        "l_min_frequency": settings.L_MIN_FREQUENCY,
        "l_max_frequency": settings.L_MAX_FREQUENCY,
        "s_min_frequency": settings.S_MIN_FREQUENCY,
        "s_max_frequency": settings.S_MAX_FREQUENCY,
        "image_changed": "image" in station_form.changed_data,
    }
    return render(request, "base/station_edit.html", data)


def station_view_popup(request, sat_id: str, station_id: int):
    """
    Station modal view

    Args:
        sat_id(str): Sat id of satellite whose need station popup
        station_id(int): ID of station which popup
    """
    satellite = get_object_or_404(Satellite, sat_id=sat_id)
    station_info = station_view(request, station_id, True)
    data = {
        "satellite": SatelliteSerializer(satellite, many=False).data,
        "station": {
            "info": StationSerializer(station_info["station"], many=False).data,
            "can_schedule": station_info["can_schedule"],
            "can_modify_delete_station": station_info["can_modify_delete_station"],
            "total_obs": station_info["total_obs"],
            "good_obs": station_info["good_obs"],
            "unknown_obs": station_info["unknown_obs"],
            "bad_obs": station_info["bad_obs"],
            "future_obs": station_info["future_obs"],
        },
    }

    return JsonResponse(data, safe=False)
