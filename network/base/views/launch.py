"""Django base views for SONIKS Network"""

from datetime import timedelta
from typing import Union

from django.conf import settings
from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView

from network import settings
from network.base.decorators import ajax_required, staff_required
from network.base.models import Launch, Observation
from network.base.tasks import launch_scheduler_task


class LaunchListView(ListView):
    """Displays a list of Launches with pagination.

    Attributes:
        model: The model to use for the view.
        context_object_name: The name to use for the list in the context.
        paginate_by: The number of items to display per page.
        template_name: The name of the template to use for the view.

    Methods:
        get_queryset: Returns a QuerySet of all Launch objects.
    """

    model = Launch
    context_object_name = "launches"
    paginate_by = settings.ITEMS_PER_PAGE
    template_name = "base/launches.html"

    def get_queryset(self):
        """Returns a QuerySet of all Launch objects."""
        return Launch.objects.all().order_by("-launch_date")


def launch_view(request, launch_id: int):
    """
    Displays a single Launch object with the given id.

    Args:
        request (HttpRequest): The request object.
        launch_id (int): The id of the Launch to display.

    Returns:
        HttpResponse: A rendered template with the Launch object.
    """

    launch = get_object_or_404(
        Launch.objects.prefetch_related(
            "launch_satellites",
            "db_satellites",
            "db_satellites__transmitter_entries",
            "db_satellites__network",
        ),
        id=launch_id,
    )

    launch_stat = get_launch_stat(launch)

    return render(
        request,
        "base/launch_details.html",
        {
            "launch": launch,
            "launch_stat": launch_stat,
            "launch_horizon": settings.LAUNCH_MIN_HORIZON,
            "launch_elevation": settings.LAUNCH_MIN_ELEVATION,
            "launch_duration": settings.LAUNCH_SPLIT_DURATION,
        },
    )


def get_launch_stat(launch: Launch) -> dict[str, Union[int, float]]:
    """
    Counting satellites based on their status

    Args:
        launch (Launch): The launch object for which satellite statistics need to be calculated

    Returns:
        (dict[str, Union[int, float]]): Statistics on satellite launch statuses
    """
    satellite_count = launch.launch_satellites.count() + launch.db_satellites.count()
    launch_stat = {
        "total": satellite_count,
        "alive": 0,
        "re_entered": 0,
        "dead": 0,
        "future": 0,
        "unknown": 0,
    }

    if satellite_count:
        alive_db = launch.db_satellites.filter(status="alive", unknown=False).count()
        re_entered_db = launch.db_satellites.filter(
            status="re-entered", unknown=False
        ).count()
        dead_db = launch.db_satellites.filter(status="dead", unknown=False).count()
        future_db = launch.db_satellites.filter(status="future", unknown=False).count()
        unknown_db = launch.db_satellites.filter(unknown=True).count()
        alive_launch = launch.launch_satellites.filter(sat_status="Активный").count()
        re_entered_launch = launch.launch_satellites.filter(
            sat_status="Сошел с орбиты"
        ).count()
        dead_launch = launch.launch_satellites.filter(sat_status="Неактивный").count()
        future_launch = launch.launch_satellites.filter(sat_status="Будущий").count()
        unknown_launch = launch.launch_satellites.filter(
            sat_status="Неизвестный"
        ).count()

        launch_stat["alive"] = float(
            format(100 / satellite_count * (alive_db + alive_launch), ".1f")
        )
        launch_stat["re_entered"] = float(
            format(100 / satellite_count * (re_entered_db + re_entered_launch), ".1f")
        )
        launch_stat["dead"] = float(
            format(100 / satellite_count * (dead_db + dead_launch), ".1f")
        )
        launch_stat["future"] = float(
            format(100 / satellite_count * (future_db + future_launch), ".1f")
        )
        launch_stat["unknown"] = float(
            format(100 / satellite_count * (unknown_db + unknown_launch), ".1f")
        )

        launch_stat["alive_count"] = alive_db + alive_launch
        launch_stat["re_entered_count"] = re_entered_db + re_entered_launch
        launch_stat["dead_count"] = dead_db + dead_launch
        launch_stat["future_count"] = future_db + future_launch
        launch_stat["unknown_count"] = unknown_db + unknown_launch

    return launch_stat


@staff_required
def schedule_launch_observations(request, launch_id: int):
    """
    Processing a request to launch the observation scheduler on launch satellites

    Args:
        request (HttpRequest): The request object.
        launch_id (int): Launch ID for calculation observations
    """
    period = int(request.POST.get("period", settings.LAUNCH_MIN_PERIOD))
    horizon = int(request.POST.get("horizon", settings.LAUNCH_MIN_HORIZON))
    elevation = int(request.POST.get("elevation", settings.LAUNCH_MIN_ELEVATION))
    duration = int(request.POST.get("duration", settings.LAUNCH_SPLIT_DURATION))
    satellite_status = request.POST.get("satellites", "")
    launch = get_object_or_404(
        Launch.objects.prefetch_related(
            "launch_satellites", "db_satellites", "db_satellites__transmitter_entries"
        ),
        id=launch_id,
    )

    start_time = launch.launch_date + timedelta(
        minutes=settings.LAUNCH_WAITING_FOR_SATELLITE
    )
    if start_time < now():
        start_time = now()
    end_time = start_time + timedelta(hours=period)
    violator_count = period // 12
    launch_scheduler_task.delay(
        launch_id,
        start_time,
        end_time,
        violator_count,
        request.user.id,
        horizon,
        elevation,
        duration,
        satellite_status,
    )

    messages.success(
        request,
        _(
            "Задача на планирование спутников запуска отправлена. Ожидайте уведомление на почту."
        ),
    )

    return redirect(reverse("base:launch_view", kwargs={"launch_id": launch_id}))


@staff_required
@ajax_required
def delete_observations_for_launch_scheduler(request, launch_id: int):
    """
    Deleting observations for the selected period to free up windows for scheduling launch observations

    Args:
        request (HttpRequest): The request object.
        launch_id (int): Launch ID for calculation observations

    Returns:
        The number of observations successfully deleted and the numbers of observations that could not be deleted
    """
    period = int(request.POST.get("period", 0))
    is_auto = int(request.POST.get("auto", 0))
    launch = get_object_or_404(
        Launch.objects.prefetch_related(
            "launch_satellites", "db_satellites", "db_satellites__transmitter_entries"
        ),
        id=launch_id,
    )
    if launch and period:
        start_time = launch.launch_date + timedelta(
            minutes=settings.LAUNCH_WAITING_FOR_SATELLITE
        )
        if start_time < now():
            start_time = now()
        end_time = start_time + timedelta(hours=period)
        if is_auto:
            observations = Observation.objects.filter(
                start__gte=start_time, end__lte=end_time, status=0, is_auto=True
            )
        else:
            observations = Observation.objects.filter(
                start__gte=start_time, end__lte=end_time, status=0
            )
        error_id = []
        success_count = 0
        for observation in observations:
            if not observation.is_started:  # проверка на то, что время начала в будущем
                observation.delete()
                success_count += 1
            else:
                error_id.append(observation.id)
        return JsonResponse(
            {
                "success": "Удалено наблюдений: " + str(success_count),
                "error": ", ".join(str(e) for e in error_id),
            },
            status=200,
        )

    return JsonResponse(
        {"error": "Ошибка доступа или некорректные параметры запроса"}, status=400
    )
