"""SONIKS Network autoscheduling functions"""

import logging
from datetime import datetime, timedelta
from typing import Dict, Optional

import ephem
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.db.models import F, Prefetch
from django.shortcuts import get_object_or_404
from django.utils.timezone import make_aware, now, utc

from network.base.models import (
    LatestTleSet,
    Observation,
    Satellite,
    Station,
    Transmitter,
)
from network.base.scheduling import (
    create_new_observation,
    create_station_window,
    predict_available_observation_windows,
    recalculate_window_parameters,
)
from network.base.validators import is_transmitter_in_station_range
from network.users.models import User

LOGGER = logging.getLogger("db")


def autoschedule_predictions_for_satellite(
    station_id: int,
    sat_id: str,
    transmitter_uuid: str,
    start_time: datetime,
    end_time: datetime,
    params: Optional[Dict[str, str]] = None,
    scheduled_user_name: Optional[str] = None,
) -> int:
    """
    Calculate all schedule window for one satellite and one station with choosed \\
    satellites transmitter.After it if autoschedule have parameters, func trying \\
    to constraint them to params or dropped it.

    Args:
        station_id (int) : ID of the station at which observations are planned.
        sat_id (str) : ID of the satellite for which observations will be planned.
        transmitter_uuid (str) : UUID of the transmitter of the satellite.
            with which the observation will be carried out.
        start_time (datetime) : Starting time for scheduling.
        end_time (datetime) : Ending time for scheduling.
        params (dict) : Dictionary of parameters for observations.Taking from autoschedule.
        scheduled_user_name (str) : Name of scheduled user. Default is station owner.

    Returns:
        int: Value of scheduled observations.
    """
    try:
        satellite = Satellite.objects.get(status="alive", sat_id=sat_id)
    except ObjectDoesNotExist:
        LOGGER.info("Satellite is dead. Skip scheduling")
        return 0
    station = get_object_or_404(
        Station.objects.prefetch_related(
            Prefetch(
                "observations",
                queryset=Observation.objects.filter(end__gt=now()),
                to_attr="scheduled_obs",
            ),
            "antennas",
            "antennas__frequency_ranges",
        ),
        id=station_id,
        alt__isnull=False,
        lat__isnull=False,
        lng__isnull=False,
    )
    transmitter = Transmitter.objects.get(
        uuid=transmitter_uuid,
        status__in=["active", "inactive"],
    )
    if not is_transmitter_in_station_range(transmitter, station):
        LOGGER.info("Transmitter not in range of station antennas")
        return 0
    tle = LatestTleSet.objects.filter(satellite=satellite).get().latest
    if scheduled_user_name is not None:
        user = User.objects.get(username=scheduled_user_name)
    else:
        user = station.owner

    # unpack parameters
    if params is None:
        params = {}
    params = unpack_schedule_params(params, station.horizon)
    _, station_windows = predict_available_observation_windows(
        station,
        params["min_horizon"],
        1,
        tle,
        start_time,
        end_time,
        {
            "split": params["split"],
            "break": params["break"],
        },
    )

    observation_list_count = 0
    if not station_windows:
        LOGGER.info("Station dont have windows for this satellite")
    else:
        for window in station_windows:
            window = window_validation(
                window, params, start_time, satellite.sat_id, station.id
            )
            if window is None:
                continue

            if transmitter.type == "Transponder":
                center_frequency = (
                    transmitter.downlink_high + transmitter.downlink_low
                ) // 2
            else:
                center_frequency = None
            window_start = make_aware(
                datetime.strptime(window["start"], "%Y-%m-%d %H:%M:%S.%f"), utc
            )
            window_end = make_aware(
                datetime.strptime(window["end"], "%Y-%m-%d %H:%M:%S.%f"), utc
            )
            observation = create_new_observation(
                station=station,
                transmitter=transmitter,
                start=window_start,
                end=window_end,
                author=user,
                center_frequency=center_frequency,
                is_auto=True,
            )

            observation.save()
            observation_list_count += 1

    return observation_list_count


def check_az(window_az: float, start_az: int, end_az: int):
    """
    Сhecking if window is in start-end azimuth from parameters.

    Args:
        window_az(float) : Azimuth from window.
        start_az(int) : Start azimuth from parameters.
        end_az(int) : End azimuth from parameters.

    Returns:
        bool: True if window az satisfied the parameters else False.
    """
    if start_az > end_az:
        complementary_window = True
        start_az, end_az = end_az, start_az
    else:
        complementary_window = False
    # Check if the specified azimuth is inside the window
    if start_az <= window_az <= end_az:
        # Azimuth is within the window (normal or complementary)
        return not complementary_window
    # Azimuth is outside the window (normal or complementary)
    return complementary_window


def constrain_pass_to_az_window(
    sat_id: str,
    station_id: int,
    window: dict,
    start_azimuth: int,
    stop_azimuth: int,
    horizon: int,
):
    """
    Changes the start and end times of observation so \\
    that the azimuths begin to fall within the specified.

    Args:
        sat_id (str): sat_id of satellite which window is constrained.
        station_id (int): ID of station which window is constrained.
        window (dict): Dict of window parameters
        start_azimuth (int): Start azimuth from params
        stop_azimuth (int): End azimuth from params
        horizon (int): Minimum horizon for constraining,needed if it value
                is different from station min horizon

    Returns:
        dict: new schedule window if it possible else None
    """
    sweep_step_size = timedelta(seconds=1)  # time step for calculation

    satellite = Satellite.objects.select_related(
        "latest_tle_set", "latest_tle_set__latest"
    ).get(status="alive", sat_id=sat_id)
    station = Station.objects.get(id=station_id)
    window_start = make_aware(
        datetime.strptime(window["start"], "%Y-%m-%d %H:%M:%S.%f"), utc
    )
    window_end = make_aware(
        datetime.strptime(window["end"], "%Y-%m-%d %H:%M:%S.%f"), utc
    )
    current_duration = window_end - window_start
    sat_ephem = ephem.readtle(
        str(satellite.latest_tle_set.latest.tle0),
        str(satellite.latest_tle_set.latest.tle1),
        str(satellite.latest_tle_set.latest.tle2),
    )
    observer = ephem.Observer()
    observer.lon = str(station.lng)
    observer.lat = str(station.lat)
    observer.elevation = station.alt
    observer.date = ephem.Date(window_start)
    observer.pressure = 0
    observer.horizon = horizon

    # start azimuth check
    azr_within_window = False
    while not azr_within_window:
        (
            start_az_recalculate,
            end_az_recalculate,
            max_altutide,
        ) = recalculate_window_parameters(observer, sat_ephem, window_start, window_end)
        azr_within_window = check_az(start_az_recalculate, start_azimuth, stop_azimuth)
        if not azr_within_window:
            window_start += sweep_step_size
            current_duration -= sweep_step_size
        if current_duration.total_seconds() < settings.OBSERVATION_DURATION_MIN:
            return None

    # end azimuth check
    azr_within_window = False
    while not azr_within_window:
        (
            start_az_recalculate,
            end_az_recalculate,
            max_altutide,
        ) = recalculate_window_parameters(observer, sat_ephem, window_start, window_end)
        azr_within_window = check_az(end_az_recalculate, start_azimuth, stop_azimuth)
        if not azr_within_window:
            window_end -= sweep_step_size
            current_duration -= sweep_step_size
        if current_duration.total_seconds() < settings.OBSERVATION_DURATION_MIN:
            return None

    new_window = create_station_window(
        window_start,
        window_end,
        start_az_recalculate,
        end_az_recalculate,
        max_altutide,
        satellite.latest_tle_set.latest,
        window["valid_duration"],
        window["overlapped"],
        window["split"],
        window["overlap_ratio"],
    )
    LOGGER.info("Azimuts resolved.new window:%s", new_window)
    return new_window


def unpack_schedule_params(params: dict, station_horizon: int):
    """
    Unpacking params for schedule

    Args:
        params (dict): Dict of params from Schedule
            can be None,then return dict with default
            values
        station_horizon(int): Minimal horizon of scheduled
            station
    Returns:
        dict: Dict of unpacked params for scheduling
    """
    unpacked_params = {}
    if params is None:
        params = {}
    unpacked_params["split"] = int(
        params.get("split", settings.OBSERVATION_SPLIT_DURATION)
    )
    unpacked_params["break"] = int(
        params.get("break", settings.OBSERVATION_SPLIT_BREAK_DURATION)
    )
    unpacked_params["min_az"] = int(params.get("min_az", 0))
    unpacked_params["max_az"] = int(params.get("max_az", 360))
    unpacked_params["min_horizon"] = int(params.get("min_horizon", station_horizon))
    unpacked_params["start_culmination"] = int(params.get("start_culmination", 0))
    unpacked_params["end_culmination"] = int(params.get("end_culmination", 90))
    return unpacked_params


def window_validation(
    window: dict, params: dict, start_time: datetime, sat_id: str, station_id: int
):
    """
    Check window with schedule parameters(or with default parameters)

    Args:
        window (dict): Observation window for checking
        params (dict): Autoscheduler parameters
        start_time (datetime): Start time for planning. Need for
            checking min observation start
        sat_id (str): Sat_id of satellite for scheduling
        station_id (int): ID of station for autoscheduling

    Returns:
        dict: Checked and constrained window if possible or None
    """
    observation_min_start = (
        start_time + timedelta(minutes=settings.OBSERVATION_DATE_MIN_START)
    ).strftime("%Y-%m-%d %H:%M:%S.%f")

    # valid duration check
    valid = window["start"] > observation_min_start and window["valid_duration"]
    if not valid:
        return None

    # azimith check. If can- constraint azumiths to params
    if not check_az(
        window["az_start"], params["min_az"], params["max_az"]
    ) or not check_az(window["az_end"], params["min_az"], params["max_az"]):
        window = constrain_pass_to_az_window(
            sat_id,
            station_id,
            window,
            params["min_az"],
            params["max_az"],
            params["min_horizon"],
        )
        if window is None:
            return None
    # max elevation check
    if (
        not params["start_culmination"]
        <= window["elev_max"]
        <= params["end_culmination"]
    ):
        return None

    return window
