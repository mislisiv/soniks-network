"""SONIKS Network django rest framework API url routings"""

from django.urls import path
from rest_framework import routers

from network.api import views

DEFAULT_ROUTER = routers.DefaultRouter()

DEFAULT_ROUTER.register(r"jobs", views.JobView, basename="jobs")
DEFAULT_ROUTER.register(r"observations", views.ObservationView, basename="observations")
DEFAULT_ROUTER.register(r"stations", views.StationView, basename="stations")
DEFAULT_ROUTER.register(r"satellites", views.SatelliteView, basename="satellites")
DEFAULT_ROUTER.register(r"transmitters", views.TransmitterView, basename="transmitters")
DEFAULT_ROUTER.register(r"demoddata", views.DemodDataViewSet, basename="demoddata")
DEFAULT_ROUTER.register(r"modes", views.ModeView, basename="modes")
DEFAULT_ROUTER.register(r"tles", views.TleView, basename="tles")
DEFAULT_ROUTER.register(r"latesttles", views.LatestTleView, basename="latesttles")
SIMPLE_ROUTER = routers.SimpleRouter()


API_URLPATTERNS = (
    DEFAULT_ROUTER.urls
    + SIMPLE_ROUTER.urls
    + [path("station/register", views.station_register_view)]
    + [
        path(
            "satellitestationobs",
            views.satellite_obs_on_stations,
            name="satellitestationobs",
        )
    ]
    + [path("topdata", views.top_data_view, name="topdata")]
)
