"""Define functions and settings for the django admin users interface"""

import base64
import email

import requests
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.db.models import Count

from network import settings
from network.users.models import User


@admin.action(description="Перенести этих пользователей в Keycloak")
def migrate_users_to_keycloak(modeladmin, request, queryset):
    """
    Function for creating users in keycloak based on \\
    existing django user. Work only if OIDC is setted in .env
    """
    # should be post for creating
    admin_token_client = settings.OIDC_ADMIN_SERVICE_ID
    admin_token_secret = settings.OIDC_ADMIN_SERVICE_SECRET
    token = requests.post(
        url=f"{settings.OIDC_KEYCLOAK_URL}/realms/master/protocol/openid-connect/token",
        data={
            "client_id": admin_token_client,
            "client_secret": admin_token_secret,
            "grant_type": "client_credentials",
        },
    )
    admin_access_token = token.json()["access_token"]

    user_create_url = (
        f"{settings.OIDC_KEYCLOAK_URL}/admin/realms/{settings.OIDC_REALM}/users"
    )
    for user in queryset:
        algorithm, iterations, salt, hashed_value = user.password.split("$")
        payload = {
            "email": user.email,
            "emailVerified": True,
            "username": user.username,
            "enabled": True,
            "credentials": [
                {
                    "type": "password",
                    "algorithm": algorithm.replace("_", "-"),
                    "hashedSaltedValue": hashed_value,
                    "hashIterations": iterations,
                    "salt": base64.b64encode(salt.encode()).decode("ascii").strip(),
                }
            ],
        }
        user_create_request = requests.post(
            user_create_url,
            json=payload,
            headers={"Authorization": f"Bearer {admin_access_token}"},
        )
    messages.success(request, f"Перенос пользователей в кейклок совершен!")


class HasStationListFilter(admin.SimpleListFilter):
    """Filter users by checking if the own a station or not"""

    title = "having station"
    parameter_name = "has_station"

    def lookups(self, request, model_admin):
        return (
            (1, "Yes"),
            (0, "No"),
        )

    def queryset(self, request, queryset):
        if self.value() == "1":
            return queryset.annotate(count=Count("ground_stations")).filter(count__gt=0)
        if self.value() == "0":
            return queryset.annotate(count=Count("ground_stations")).filter(count__lt=1)
        return None


class UserAdmin(AuthUserAdmin):
    """Class of AuthUserAdmin"""

    create_form_class = UserCreationForm
    update_form_class = UserChangeForm
    list_filter = AuthUserAdmin.list_filter + (HasStationListFilter,)
    actions = [migrate_users_to_keycloak]


admin.site.register(User, UserAdmin)
