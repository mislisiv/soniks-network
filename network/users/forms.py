"""Django users forms for SONIKS Network"""

from django import forms

from network.users.models import User
from django.utils.translation import gettext_lazy as _


class UserForm(forms.ModelForm):
    """Model Form class for User objects"""

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email")
        labels = {
            "first_name": _("Имя"),
            "last_name": _("Фамилия"),
            "email": _("Эл. почта"),
        }
