from mozilla_django_oidc.auth import OIDCAuthenticationBackend


class MyOIDCAB(OIDCAuthenticationBackend):
    def create_user(self, claims):
        user = super(MyOIDCAB, self).create_user(claims)
        print(claims)
        user.email = claims.get("email", "")
        user.username = claims.get("preferred_username", "")
        user.save()
        return user

    def update_user(self, user, claims):
        user.username = claims.get("preferred_username", "")
        user.save()
        return user
