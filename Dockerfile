FROM python:3.9.12
MAINTAINER SONIKS project <sonikspace@yandex.ru>

WORKDIR /workdir/

RUN groupadd -r soniks \
	&& useradd -r -g soniks soniks \
	&& install -d -m 755 -o soniks -g soniks /var/run/celery

RUN apt-get update \
	&& apt-get -y install ruby-sass \
	&& rm -rf /var/lib/apt/lists/*

COPY requirements.txt /usr/local/src/soniks-network/
RUN pip install \
	--no-cache-dir \
	--no-deps \
	--force-reinstall \
	-r /usr/local/src/soniks-network/requirements.txt

COPY . /usr/local/src/soniks-network/
RUN pip install \
	--no-cache-dir \
	--no-deps \
	--force-reinstall \
	/usr/local/src/soniks-network
RUN install -m 755 /usr/local/src/soniks-network/bin/djangoctl.sh /usr/local/bin/

RUN rm -rf /usr/local/src/soniks-network

ENV DJANGO_SETTINGS_MODULE network.settings

COPY .env /workdir/

EXPOSE 8000
